package com.example.pikpoktest;

//import android.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import com.example.pikpoktest.ProductsFragment.OnProductSelectedListener;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

public class FeaturedFragment extends Fragment {

	ArrayList<String> imagelist;
	ArrayList<String> featuredstores, storenames;
	String city;
	ListView gv1;
	protected ProgressDialog proDialog;

	public interface OnFeaturedSelected {
		public void featuredSelected(ArrayList<String> list, String StoreID);
	}

	private static ArrayList<String> list = new ArrayList<String>();

	public static ArrayList<String> getList() {
		return list;
	}

	public static void setList(ArrayList<String> list) {
		FeaturedFragment.list = list;
	}

	OnFeaturedSelected mCallback;

	public static Fragment newInstance(Context context) {
		FeaturedFragment f = new FeaturedFragment();
		return f;
	}

	protected void startLoading() {
		proDialog = new ProgressDialog(getActivity());
		proDialog.setCancelable(false);
		proDialog.show();
		proDialog.setContentView(R.layout.progressdialog);
		//proDialog.setMessage("loading...");
		//proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		
	}

	protected void stopLoading() {
		proDialog.dismiss();
		proDialog = null;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (OnFeaturedSelected) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnProductSelectedListener");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		ViewGroup root = (ViewGroup) inflater.inflate(
				R.layout.fragment_find_people, null);
		gv1 = (ListView) root.findViewById(R.id.listView1);
		return root;

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

		super.onResume();

		imagelist = new ArrayList<String>();
		storenames = new ArrayList<String>();
		featuredstores = new ArrayList<String>();
		if (getArguments() != null) {
			city = getArguments().getString("selectedcity");
		}
		populateListView();
		/*
		 * if(getActivity().getSupportFragmentManager().getBackStackEntryCount()>
		 * 1){ FragmentManager fm = getActivity().getSupportFragmentManager();
		 * 
		 * fm.popBackStack(fm.getBackStackEntryAt(1).getId(),fm.
		 * POP_BACK_STACK_INCLUSIVE); }
		 */

	}

	private void populateListView() {
		// TODO Auto-generated method stub
		startLoading();

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Store");
		query.whereEqualTo("Featured", 1);
		query.whereEqualTo("Status", 1);
		query.whereEqualTo("City", city);
		// Log.d("loggedincity", city);
		Log.d("checkone", "1" + city);
		query.orderByAscending("Rank");
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {

					// ob = objects;
					for (int i = 0; i < objects.size(); i++) {
						Log.d("checktwo", "2");

						imagelist.add(objects.get(i).getString("Banner"));
						storenames.add(objects.get(i).getString("Name"));
						featuredstores.add(objects.get(i).getObjectId());
						// Log.d("subdasda",""+objects.get(i).getString("Store"));

					}
					 try {
						stopLoading();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} 
				
				else {
					Log.d("score", "Error: " + e.getMessage());
					 stopLoading();
				}
				// objects.get(0).getInt("StoreID");

				// Log.d("abc", ""+list.size());
				// mCallback.featuredSelected(list, position);
				gv1.setAdapter(new ImageAdapter(getActivity(), imagelist,
						storenames));
				gv1.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View v,
							final int position, long id) {
						ParseQuery<ParseObject> query = ParseQuery
								.getQuery("Products");
						query.whereEqualTo("StoreID",
								featuredstores.get(position));
						query.findInBackground(new FindCallback<ParseObject>() {
							public void done(List<ParseObject> objects,
									ParseException e) {
								if (e == null) {
									// ArrayList<Integer> array = new
									// ArrayList<Integer>();
									// ob = objects;
									for (int i = 0; i < objects.size(); i++) {

										list.add(objects.get(i).getObjectId());
										// Log.d("sub",""+objects.get(i).getInt("ProductID"));

									}

								} else {
									Log.d("score", "Error: " + e.getMessage());
								}

								if (objects.size() > 0) {
									String storeID = objects.get(0).getString(
											"StoreID");
									mCallback.featuredSelected(list, storeID);
								} else {
									Toast.makeText(getActivity(),
											"No products in this store.",
											Toast.LENGTH_LONG).show();
								}
								// storeID =
								// objects.get(0).getString("StoreID");
								// Log.d("abc", ""+list.size());

							}
						});

						// Toast.makeText(getActivity(),
						// ""+position,Toast.LENGTH_SHORT).show();
					}
				});

			}

		});

	}

}