package com.example.pikpoktest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.example.pikpoktest.ProductsFragment.OnProductSelectedListener;
import com.google.android.gms.maps.model.LatLng;
import com.javapapers.android.maps.path.PathGoogleMapActivity;
import com.loopj.android.image.SmartImageView;
import com.parse.ParseObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class ToFromFragment extends Fragment {

	ToFromListener mCallback;
	AutoCompleteTextView to, from;
	Button quote,call;
	RadioGroup radiogroup;
	String toAddress, fromAddress;
	Geocoder geoCoder;
	LatLng fromadd, toadd;
	protected ProgressDialog progress;
	ArrayAdapter<String> adapter;
	
	
	protected void startLoading() {
		//ProgressDialog progress;

		progress = new ProgressDialog(getActivity());
		//progress.setTitle("Please Wait!!");
		//progress.setMessage("Wait!!");
		progress.setCancelable(true);
		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progress.show();
	}

	protected void stopLoading() {
		progress.dismiss();
		progress = null;
	}


	public interface ToFromListener {
		public void toFromSelected(LatLng to, LatLng from);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (ToFromListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement ToFromListener");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_tofrom,
				null);
		 to = (AutoCompleteTextView) root.findViewById(R.id.tVTo);
		 from = (AutoCompleteTextView) root.findViewById(R.id.tVFrom);
		 String[] countries = getResources().
				   getStringArray(R.array.suburbs);
				   adapter = new ArrayAdapter<String>
				   (getActivity(),android.R.layout.simple_list_item_1,countries);
				//   actv.setAdapter(adapter);
		quote = (Button) root.findViewById(R.id.bQuote);
		radiogroup = (RadioGroup) root.findViewById(R.id.radioGroup1);
		//To = (Spinner) root.findViewById(R.id.STo);
		//From = (Spinner) root.findViewById(R.id.SFrom);
		call = (Button) root.findViewById(R.id.bCall);
		

		call.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String url = "tel:+91 9899363678";
				Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
				startActivity(intent);
			}
		});

		return root;
	}

	private void setUpSpinners() {
//		ArrayAdapter<CharSequence> adaptersuburbs = ArrayAdapter
//				.createFromResource(getActivity(), R.array.suburbs,
//						android.R.layout.simple_spinner_item);
//		adaptersuburbs
//				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		//To.setAdapter(adaptersuburbs);
		//From.setAdapter(adaptersuburbs);
		to.setAdapter(adapter);
		from.setAdapter(adapter);
		
		
//		To.setOnItemSelectedListener(new OnItemSelectedListener() {
//
//			@Override
//			public void onItemSelected(AdapterView<?> arg0, View arg1,
//					int arg2, long arg3) {
//				// TODO Auto-generated method stub
//				toAddress = (String) arg0.getItemAtPosition(arg2);
//			}
//
//			@Override
//			public void onNothingSelected(AdapterView<?> arg0) {
//				// TODO Auto-generated method stub
//				toAddress = (String) arg0.getItemAtPosition(0);
//
//			}
//		});
//
//		From.setOnItemSelectedListener(new OnItemSelectedListener() {
//
//			@Override
//			public void onItemSelected(AdapterView<?> arg0, View arg1,
//					int arg2, long arg3) {
//				// TODO Auto-generated method stub
//				fromAddress = (String) arg0.getItemAtPosition(arg2);
//			}
//
//			@Override
//			public void onNothingSelected(AdapterView<?> arg0) {
//				// TODO Auto-generated method stub
//				fromAddress = (String) arg0.getItemAtPosition(0);
//
//			}
//		});
	}

	@Override
	public void onResume() {
		super.onResume();
		geoCoder = new Geocoder(getActivity());
		setUpSpinners();
		getOrderInfo();
		toadd = null;
		fromadd = null;
	}

	private void getOrderInfo() {

		if (radiogroup.indexOfChild(getActivity().findViewById(
				radiogroup.getCheckedRadioButtonId())) == 0) {
		//	Toast.makeText(getActivity(), "Index 0", Toast.LENGTH_SHORT).show();
		} else {
			//Toast.makeText(getActivity(), "Index 1", Toast.LENGTH_SHORT).show();
		}
		
				
		quote.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				toAddress = to.getText().toString();
				fromAddress = from.getText().toString();

//				if(toAddress.equals(fromAddress)){
//					Toast.makeText(getActivity(), "Please enter different to and from locations",
//							Toast.LENGTH_SHORT).show();
//				}else{
				
				if(to.getText().toString().equals("")||from.getText().toString().equals("")){
					Toast.makeText(getActivity(), "Please fill in the to and from locations", 1000).show();
				}else{
				
				try {
					//startLoading();
					
					
					

					toAddress = toAddress + ", Noida";
					fromAddress = fromAddress + ", Noida";
					// fromAddress = from.getText().toString();
					List<Address> addresses = geoCoder.getFromLocationName(
							toAddress, 2);
					if (addresses.size() > 0) {
//						Toast.makeText(
//								getActivity(),
//								"lat " + addresses.get(0).getLatitude()
//										+ "long "
//										+ addresses.get(0).getLongitude(),
//								Toast.LENGTH_SHORT).show();
						toadd = new LatLng(addresses.get(0).getLatitude(),
								addresses.get(0).getLongitude());

					}

					List<Address> addresses1 = geoCoder.getFromLocationName(
							fromAddress, 2);
					if (addresses1.size() > 0) {
						// Toast.makeText(
						// getActivity(),
						// "lat " + addresses1.get(0).getLatitude()
						// + "long "
						// + addresses1.get(0).getLongitude(),
						// Toast.LENGTH_SHORT).show();
						fromadd = new LatLng(addresses1.get(0).getLatitude(),
								addresses1.get(0).getLongitude());

					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Intent i = new Intent(getActivity(), PathGoogleMapActivity.class);
			Bundle args = new Bundle();
			if(fromadd!=null&&toadd!=null){
				args.putParcelable("from_position", fromadd);
				args.putParcelable("to_position", toadd);
				args.putString("to", toAddress);
				args.putString("from", fromAddress);
				i.putExtra("bundle", args);
				//stopLoading();
				startActivity(i);
			
				}else{
					//stopLoading();
					Toast.makeText(getActivity(), "Could not get co-ordinates of the specified locations." +
							"Please check your network connecion.", Toast.LENGTH_SHORT).show();
				}
			
				//mCallback.toFromSelected(toadd,fromadd);
			}
			}
//			}

		});

	}

}


