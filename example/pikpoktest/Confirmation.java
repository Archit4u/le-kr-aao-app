package com.example.pikpoktest;

import in.varunbhalla.jsontest.ServiceHandler;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.internal.cu;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.RefreshCallback;
import com.parse.SaveCallback;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Confirmation extends Activity implements OnClickListener {

	String address, houseS, streetoneS, streettwoS, cityS, stateS, emailS,
			suburbS, nameS, emailid;
	Long contactN;
	Number pinN;
	EditText house, streetone, streettwo, pin, contact, name, email;
	Spinner state;
	Spinner city;
	AutoCompleteTextView suburb;
	ArrayAdapter<String> adapter;

	Button confirm, call;
	List<AddToCart> myList = new ArrayList<AddToCart>();
	TextView tVadd, price, delivery, amount;
	CheckBox defaultaddress;
	int j = 0;
	int ordercharge, deliverycharge =25, tproducts,tprice;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.confirmation);

		Bundle extras = getIntent().getExtras();

		myList = (List<AddToCart>) extras.getSerializable("cart");
		ordercharge = extras.getInt("amount");

		for (int i = 0; i < myList.size(); i++) {
			Log.d("intent", "" + myList.get(i).getProductId() + ""
					+ myList.get(i).getQty());
		}

		//defaultaddress = (CheckBox) findViewById(R.id.cBSave);

		
		name = (EditText) findViewById(R.id.eTname);
		house = (EditText) findViewById(R.id.editText1);
		streetone = (EditText) findViewById(R.id.editText2);
		suburb = (AutoCompleteTextView) findViewById(R.id.Ssuburb);
		city = (Spinner) findViewById(R.id.Scity);
		state = (Spinner) findViewById(R.id.Sstate);
		pin = (EditText) findViewById(R.id.editText6);
		email = (EditText) findViewById(R.id.ETemail);

		// email = (EditText) findViewById(R.id.EditText02);
		contact = (EditText) findViewById(R.id.EditText01);
		// checkbox = (CheckBox) findViewById(R.id.checkBox1);
		// checkboxphone = (CheckBox) findViewById(R.id.checkBox2);
		 String[] countries = getResources().
				   getStringArray(R.array.suburbs);
				   adapter = new ArrayAdapter<String>
				   (this,android.R.layout.simple_list_item_1,countries);

		ArrayAdapter<CharSequence> adaptercities = ArrayAdapter
				.createFromResource(this, R.array.cities,
						android.R.layout.simple_spinner_item);

		ArrayAdapter<CharSequence> adapterstate = ArrayAdapter
				.createFromResource(this, R.array.state,
						android.R.layout.simple_spinner_item);

		ArrayAdapter<CharSequence> adaptersuburbs = ArrayAdapter
				.createFromResource(this, R.array.suburbs,
						android.R.layout.simple_spinner_item);
		
		email.setText(""+MainActivity.USER_EMAIL);

		adaptercities
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adapterstate
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adaptersuburbs
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		city.setAdapter(adaptercities);

		state.setAdapter(adapterstate);

		suburb.setAdapter(adapter);

		city.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				cityS = (String) arg0.getItemAtPosition(arg2);

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				cityS = (String) arg0.getItemAtPosition(0);

			}
		});

		state.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				stateS = (String) arg0.getItemAtPosition(arg2);

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				stateS = (String) arg0.getItemAtPosition(0);
			}
		});

//		suburb.setOnItemSelectedListener(new OnItemSelectedListener() {
//
//			@Override
//			public void onItemSelected(AdapterView<?> arg0, View arg1,
//					int arg2, long arg3) {
//				// TODO Auto-generated method stub
//				suburbS = (String) arg0.getItemAtPosition(arg2);
//			}
//
//			@Override
//			public void onNothingSelected(AdapterView<?> arg0) {
//				// TODO Auto-generated method stub
//				suburbS = (String) arg0.getItemAtPosition(0);
//
//			}
//		});

//		name.setFocusable(false);
//		house.setFocusable(false);
//		streetone.setFocusable(false);
//		// streettwo.setFocusable(false);
//		// city.setFocusable(false);
//		// state.setFocusable(false);
//		city.setEnabled(false);
//		state.setEnabled(false);
//		suburb.setEnabled(false);
//		pin.setFocusable(false);
//		contact.setFocusable(true);
		// checkbox.setClickable(false);
		// checkboxphone.setClickable(false);

	
/*
		if (ordercharge >= 500) {
			deliverycharge = 0;
		} else {
			deliverycharge = 50;
		}*/

		price = (TextView) findViewById(R.id.textView2);
		delivery = (TextView) findViewById(R.id.textView3);
		amount = (TextView) findViewById(R.id.textView4);

		price.setText("The price of items ordered is Rs. " + ordercharge + " *");
		delivery.setText("The delivery charge is Rs. " + deliverycharge);
		amount.setText("The total bill amount is Rs. "
				+ (ordercharge + deliverycharge)+ " *");

		call = (Button) findViewById(R.id.bCall);
		confirm = (Button) findViewById(R.id.bConfirm);

		call.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String url = "tel:+91 9899363678";
				Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
				startActivity(intent);
			}
		});

		
		confirm.setOnClickListener(this);
		//Log.d("username", ParseUser.getCurrentUser().getUsername());
//		ParseQuery<ParseObject> query = ParseQuery.getQuery("Order");
//
//		query.whereEqualTo("Email", ParseUser.getCurrentUser().getUsername());
//		query.addDescendingOrder("OrderID");
//		query.setLimit(1);
//		query.findInBackground(new FindCallback<ParseObject>() {
//		
//			@Override
//			public void done(List<ParseObject> objects, ParseException e) {
//				// TODO Auto-generated method stub
//				if (objects == null) {
//
//					Log.d("score", "The getFirst request failed.");
//				} else {
//					for (int i = 0; i < objects.size(); i++) {
//						// Log.d("username","" +
//						// objects.get(i).getString("Address"));
//						Log.d("add", "ress");
//						address = objects.get(i).getString("Address");
//								
//								
//						//contactN = objects.get(i).getLong("Contact");
//						Rbadd.setText(address);
//						
//						// Log.d("score", "Retrieved the object.");
//					}
//					
//					if(objects.size()==0){
//						Rbadd.setText("No stored address. Please enter address below.");
//					}
//				}
//			}
//
//		});

	}

//	public void onRadioButtonClicked(View view) {
//		// Is the button now checked?
//		boolean checked = ((RadioButton) view).isChecked();
//
//		// Check which radio button was clicked
//		switch (view.getId()) {
//		case R.id.other:
//			if (checked)
//				// add.setFocusable(true);
//				// add.setFocusableInTouchMode(true);
//
//				name.setFocusableInTouchMode(true);
//			house.setFocusableInTouchMode(true);
//			streetone.setFocusableInTouchMode(true);
//			// streettwo.setFocusableInTouchMode(true);
//			// city.setFocusableInTouchMode(true);
//			// state.setFocusableInTouchMode(true);
//			city.setEnabled(true);
//			state.setEnabled(true);
//			suburb.setEnabled(true);
//			pin.setFocusableInTouchMode(true);
//		//	defaultaddress.setEnabled(true);
//			contact.setFocusableInTouchMode(true);
//			// checkbox.setClickable(true);
//			// checkboxphone.setClickable(true);
//			// Toast.makeText(this, "other", Toast.LENGTH_SHORT).show();
//			break;
//		case R.id.address:
//			if (checked)
//				// add.setFocusable(false);
//				//defaultaddress.setEnabled(false);
//				name.setFocusable(false);
//			house.setFocusable(false);
//			streetone.setFocusable(false);
//			city.setEnabled(false);
//			state.setEnabled(false);
//			suburb.setEnabled(false);
//			// streettwo.setFocusable(false);
//			// city.setFocusable(false);
//			// state.setFocusable(false);
//			pin.setFocusable(false);
//			contact.setFocusableInTouchMode(true);
//			// checkbox.setClickable(false);
//			// checkboxphone.setClickable(false);
//
//			// Toast.makeText(this, "addre", Toast.LENGTH_SHORT).show();
//
//			break;
//		}
//	}

	/*
	 * public void itemClicked(View v) { //code to check if this checkbox is
	 * checked! checkbox = (CheckBox)v;
	 * 
	 * switch(v.getId()){
	 * 
	 * case R.id.checkBox1:
	 * 
	 * if(checkbox.isChecked()){ Log.d("Checkbox1","is checked"); houseS =
	 * house.getText().toString(); streetoneS = streetone.getText().toString();
	 * streettwoS = streettwo.getText().toString(); //cityS =
	 * city.getText().toString(); //stateS = state.getText().toString();
	 * //Log.d("State",""+stateS); pinN =
	 * Integer.parseInt(pin.getText().toString());
	 * 
	 * ParseUser currentUser = ParseUser.getCurrentUser();
	 * currentUser.put("HouseNumber", houseS);
	 * currentUser.put("Address1",streetoneS); currentUser.put("Address2",
	 * suburbS); currentUser.put("City",cityS); currentUser.put("State",
	 * stateS); currentUser.put("Pincode",pinN);
	 * Log.d("UsernameOfCurrentuser",""+currentUser.getUsername());
	 * currentUser.saveInBackground();
	 * 
	 * } else { Log.d("Checkbox1","is notchecked"); }
	 * 
	 * case R.id.checkBox2:
	 * 
	 * if(checkboxphone.isChecked()){ Log.d("Checkbox2","is checked"); contactN
	 * = Long.parseLong(contact.getText().toString());
	 * 
	 * ParseUser currentUser = ParseUser.getCurrentUser();
	 * currentUser.put("Contact", contactN);
	 * Log.d("UsernameOfCurrentuser",""+currentUser.getUsername());
	 * currentUser.saveInBackground();
	 * 
	 * } else { Log.d("Checkbox2","is notchecked"); }
	 * 
	 * } }
	 */
	
/*	 public void itemClicked(View v) { 
		 defaultaddress = (CheckBox)v;
		 
		 
		 switch(v.getId()){
		 case R.id.cBSave:
			 Toast.makeText(getBaseContext(), "dadas", Toast.LENGTH_SHORT).show();
		 }
	 }  */
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// Toast.makeText(this,
		// ""+radiogroup.indexOfChild(findViewById(radiogroup.getCheckedRadioButtonId())),
		// Toast.LENGTH_SHORT).show();
		nameS = name.getText().toString();
		suburbS = suburb.getText().toString();
		final ArrayList<String> pid = new ArrayList<String>();
		final ArrayList<Integer> qty = new ArrayList<Integer>();
		for (int i = 0; i < myList.size(); i++) {
			// Log.d("orderfinal",
			// "id"+myList.get(i).getProductId()+"  q"+myList.get(i).getQty()+" q"
			// +list.get(i).getQty()
			// +" name"+list.get(i).getName());
			pid.add(myList.get(i).getProductId());
			qty.add(myList.get(i).getQty());

		}

	//	if (radiogroup.indexOfChild(findViewById(radiogroup
		//		.getCheckedRadioButtonId())) == 1) { 
			if (house.getText().toString().equals("")
					|| streetone.getText().toString().equals("")
					|| contact.getText().toString().equals("")
				|| suburbS.equals("")||nameS.equals("")
					|| pin.getText().toString().equals("")) {

				Toast toast = Toast.makeText(getBaseContext(),
						"Please fill in all the fields.", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
				toast.show();
			} else if(contact.getText().toString().length()!=10 || pin.getText().toString().length()!=6){
				Toast toast = Toast.makeText(getBaseContext(), "Please enter a valid contact number and pincode.",
						Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
				toast.show();} 
			else  {
				address = house.getText().toString() + ", "
						+ streetone.getText().toString() + ", " + suburbS
						+ ", " + cityS + ", " + stateS + ", "
						+ pin.getText().toString();
				confirm.setEnabled(false);
				ParseQuery<ParseObject> query = ParseQuery.getQuery("Order");
				query.setLimit(1000);

				// query.whereEqualTo("Category", "Food");
				query.findInBackground(new FindCallback<ParseObject>() {
					public void done(List<ParseObject> objects, ParseException e) {
						if (e == null) {
							Log.i("orderid", "" + objects.size());
							for (int i = 0; i < objects.size(); i++) {
								// Log.d("adapter", "" +
								// objects.get(i).getParseFile("image").getUrl());
								int order = objects.get(i).getInt("OrderID");
								
								if (j < order) {
									j = order;
								}

							}

						} else {
							Log.d("score", "Error: " + e.getMessage());
						}

						nameS = name.getText().toString();

						pinN = Integer.parseInt(pin.getText().toString());
						tproducts = pid.size();
						tprice = ordercharge+deliverycharge;
						
					/*	if(defaultaddress.isChecked()){
							Toast.makeText(getBaseContext(), "aaaa", Toast.LENGTH_SHORT).show();
							//pinN = Integer.parseInt(pin.getText().toString());
							 
			                    
							 ParseUser currentUser = ParseUser.getCurrentUser();
							 
							 ParseACL acl = new ParseACL();
			                    acl.setReadAccess(currentUser, true);
			                    acl.setWriteAccess(currentUser, true);
			                   
							 
							final ParseObject addressclass = new ParseObject("Address");
							ParseQuery<ParseObject> query = ParseQuery.getQuery("Address");
							query.whereEqualTo("username", ParseUser.getCurrentUser().getUsername());
							query.getFirstInBackground(new GetCallback<ParseObject>() {
								
								@Override
								public void done(ParseObject object, ParseException e) {
									// TODO Auto-generated method stub
									object.put("Address", address);
									object.put("Contact", contactN);
								}
							});
						
							
									
									  Log.d("SERIN", house.getText().toString()+"    "+ streetone.getText().toString()+"              "
											  +cityS+"    "+ stateS);
									 
									 currentUser.put("HouseNumber", house.getText().toString());
									 currentUser.put("Address1",streetone.getText().toString()); currentUser.put("Address2",
									  suburbS); currentUser.put("City",cityS); currentUser.put("State",
									  stateS); currentUser.put("Pincode",pin.getText().toString());
									  Log.d("UsernameOfCurrentuser",""+currentUser.getUsername());
									  currentUser.saveInBackground();
						} */
					    String url = "http://api.androidhive.info/contacts/";
				        new GetContacts().execute();


						ParseObject order2 = new ParseObject("Order");
						List<ParseObject> objects2 = new ArrayList<ParseObject>();
						
						
						
						order2.put("OrderID", j + 1);
						order2.put("username", email.getText().toString());
						order2.put("Email",  email.getText().toString());
						order2.put("Name", nameS);
						order2.put("ProductID", pid);
						order2.put("Quantity", qty);
						order2.put("Address", address);
						order2.put("Status", 0);
						order2.put("Pincode", pinN);
						order2.put("OrderType", "Delivery");
						order2.put("DeliveryCharge", deliverycharge);
						order2.put("TotalPrice", ordercharge + deliverycharge);
						order2.put("Contact", Long.parseLong(contact.getText().toString()));
						order2.saveInBackground();
						Toast.makeText(
								getBaseContext(),
								"Order placed! View the current order status by navigating to My orders.",
								Toast.LENGTH_LONG).show();
						
						MainActivity.getMyList().clear();
						Intent in = new Intent(getBaseContext(),
								MainActivity.class);
						in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
								| Intent.FLAG_ACTIVITY_CLEAR_TASK);
						startActivity(in);

					}
				});

			}
		
		//else {
//			if(Rbadd.getText().equals("No stored address. Please enter address below.")){
//				Toast toast = Toast.makeText(getBaseContext(), "Please enter alternate address below.",
//						Toast.LENGTH_SHORT);
//				toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
//				toast.show();
//			}
//			else{
//			address = Rbadd.getText().toString();
//			if(contact.getText().toString().length()!=10){
//				Toast toast = Toast.makeText(getBaseContext(), "Please enter a valid contact number.",
//						Toast.LENGTH_SHORT);
//				toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
//				toast.show();
//				}else{
//					confirm.setEnabled(false);
//			ParseQuery<ParseObject> query = ParseQuery.getQuery("Order");
//			query.setLimit(1000);
//			// query.whereEqualTo("Category", "Food");
//			query.findInBackground(new FindCallback<ParseObject>() {
//				public void done(List<ParseObject> objects, ParseException e) {
//					if (e == null) {
//						Log.i("orderid", "" + objects.size());
//						for (int i = 0; i < objects.size(); i++) {
//							// Log.d("adapter", "" +
//							// objects.get(i).getParseFile("image").getUrl());
//							int order = objects.get(i).getInt("OrderID");
//							//Log.i("orderid", "" + order);
//							if (j < order) {
//								j = order;
//							}
//
//						}
//
//					} else {
//						Log.d("score", "Error: " + e.getMessage());
//					}
//					// pinN = Integer.parseInt(pin.getText().toString());
//					tproducts = pid.size();
//					tprice = ordercharge+deliverycharge;
//					new GetContacts().execute();
//					
//					ParseObject order2 = new ParseObject("Order");
//					List<ParseObject> objects2 = new ArrayList<ParseObject>();
//					order2.put("OrderID", j + 1);
//					order2.put("username", ParseUser.getCurrentUser()
//							.getUsername());
//					order2.put("Email",  email.getText().toString());
//					order2.put("ProductID", pid);
//					order2.put("Quantity", qty);
//					order2.put("Address", address);
//					order2.put("Status", 0);
//					order2.put("DeliveryCharge", deliverycharge);
//					order2.put("TotalPrice", ordercharge + deliverycharge);
//					order2.put("Contact", Long.parseLong(contact.getText().toString()));
//					order2.saveInBackground();
//					Toast.makeText(
//							getBaseContext(),
//							"Order placed! View the current order status by navigating to My orders.",
//							Toast.LENGTH_SHORT).show();
//					MainActivity.getMyList().clear();
//					Intent in = new Intent(getBaseContext(), MainActivity.class);
//					in.putExtra("Confirmation", 1);
//					startActivity(in);
//
//					
//				}
//			});
//				}
//			}
//		}

	


	}
	 private class GetContacts extends AsyncTask<Void, Void, Void> {
		 
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	            // Showing progress dialog
	           
	 
	        }
	 
	        @Override
	        protected Void doInBackground(Void... arg0) {
	            // Creating service handler class instance
	            ServiceHandler sh = new ServiceHandler();
				String url = "http://107.170.68.128:8080/mail.jsp?";
	            emailid =  email.getText().toString();
	            Calendar c = Calendar.getInstance();
				Date d = c.getTime();
				int orderNumber = j+1;
				
				String add = URLEncoder.encode(address);
				
			//	String date = df.format(d);
			//	http://107.170.68.128:8080/mail.jsp?t
				//	oken=v2lekaraao&uname=Vivek&delivery_addr=
					//Delhi&pick_addr=Bangalore&contact=8867686066&order_no=101&order_time=10AM&exp_time=60min&email=vivek@foofys.com
				url = url + "token=v2lekaraao" +"&uname="+nameS+"&delivery_addr="+add+"&contact="
					+ Long.parseLong(contact.getText().toString())+"&order_no="+orderNumber+
					"&order_time="+System.currentTimeMillis()+
						"&exp_time=60min"+
					"&email=" + emailid;
	            //url = url+"&token=v2lekaraao"+"&email="+emailid+"&tproducts="+tproducts+"&tprice="+tprice;
				//url = URLEncoder.encode(url);
				
	            Log.d("URL", url);
	            // Making a request to url and getting response
	            String jsonStr = sh.makeServiceCall(url,
	            		ServiceHandler.GET);
	            //Toast.makeText(getApplicationContext(), "dsa" +jsonStr, Toast.LENGTH_SHORT).show();
	            Log.d("Response: ", "> " + jsonStr);
	 
	 
	            return null;
	        }
	 
	        @Override
	        protected void onPostExecute(Void result) {
	            super.onPostExecute(result);
	          
	        }
	 
	    }
}



