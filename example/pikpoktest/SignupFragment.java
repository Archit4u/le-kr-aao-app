package com.example.pikpoktest;

import java.util.List;
import java.util.regex.Pattern;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
//import android.app.Fragment;
//import android.app.FragmentManager;
//import android.app.FragmentTransaction;

public class SignupFragment extends Fragment {

	Button signup;
	String usernametxt;
	String passwordtxt, housenoS, street1S, street2S, cityS, stateS,conpasswordtxt,pincodeS,numberS, selectedcity;
	Number pincodeN;
	//s=Long phone;
	EditText password, number;
	EditText username,conpassword;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_signup, container,
				false);
		username = (EditText) rootView.findViewById(R.id.username);
		password = (EditText) rootView.findViewById(R.id.password);
		conpassword = (EditText) rootView.findViewById(R.id.conpassword);
		number = (EditText) rootView.findViewById(R.id.number);
		
		signup = (Button) rootView.findViewById(R.id.signup);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		if(getArguments()!=null){
			selectedcity = getArguments().getString("selectedcity");
		} 	
		 password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
		        @Override
		        public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
		        	passwordtxt = password.getText().toString();
		        	if (passwordtxt.length()<6){
						
						Toast.makeText(getActivity(),
								"Password should be of minimum 6 characters",
								Toast.LENGTH_LONG).show();
						return true;
					}
		        	else{
		        		return false;
		        	}
		        }}); 
		 
		 conpassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
		        @Override
		        public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
		        	passwordtxt = password.getText().toString();
		        	Log.d("pass",""+passwordtxt);
		        	conpasswordtxt = conpassword.getText().toString();
		        	Log.d("conpass",""+conpasswordtxt);
		        	if(!conpassword.getText().toString().equals(password.getText().toString()) ){
						
						Toast.makeText(getActivity(),
								"Password doesnt match!!!",
								Toast.LENGTH_LONG).show();
						return true;
					}
		        	else{
		        		return false;
		        	}
		        }}); 
		
		 
		 number.setOnEditorActionListener(new TextView.OnEditorActionListener() {
		        @Override
		        public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
		        	numberS = number.getText().toString();
		        	
		        	if(numberS.length() != 10 ){
						
						Toast.makeText(getActivity(),
								"Enter a 10 digit phone number!!!",
								Toast.LENGTH_LONG).show();
						return true;
					}
		        	else{
		        		return false;
		        	}
		        }}); 
		 
		 username.setOnEditorActionListener(new TextView.OnEditorActionListener() {
		        @Override
		        public boolean onEditorAction(TextView view, int actionId, KeyEvent event){
		        	
		        	usernametxt = username.getText().toString();
		        	
		        	final Pattern Email = Pattern.compile(
		        	          "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
		        	          "\\@" +
		        	          "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
		        	          "(" +
		        	          "\\." +
		        	          "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
		        	          ")+"
		        	      );
		        	
		        	if(!Email.matcher(usernametxt).matches()){
						
						Toast.makeText(getActivity(),
								"Enter a valid email id!!!",
								Toast.LENGTH_LONG).show();
						return true;
					}
		        	else{
		        		return false;
		        	}
		        	
		        }}); 
		 
		signup.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// Retrieve the text entered from the EditText
				usernametxt = username.getText().toString();
				passwordtxt = password.getText().toString();
				
				

				ParseQuery<ParseUser> query = ParseUser.getQuery();
				query.whereEqualTo("username", usernametxt);
				query.findInBackground(new FindCallback<ParseUser>() {
					public void done(List<ParseUser> objects, ParseException e) {
						if (e == null) {
							// The query was successful.
							
							if (objects.size() == 0) {
								
								if(usernametxt.equals("")
											|| passwordtxt.equals("")
											|| number.getText().toString().equals(""))
											 {
										Log.i("pass", passwordtxt);
										Toast toast = Toast.makeText(getActivity(),
												"Please complete the sign up form",
												Toast.LENGTH_SHORT);
										toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
										toast.show();
								}
								else if (passwordtxt.length()<6){
									
									Toast toast = Toast.makeText(getActivity(),
											"Password should be of minimum 6 characters",
											Toast.LENGTH_LONG);
									toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
									toast.show();
									
								}else if (number.getText().toString().length()!=10){
									
									Toast toast = Toast.makeText(getActivity(),
											"Please enter a valid number.",
											Toast.LENGTH_LONG);
											toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
									toast.show();
									
								}else {
									// Save new user data into Parse.com Data
									// Storage
									Log.i("pass", passwordtxt);
									ParseUser user = new ParseUser();
									user.setUsername(usernametxt);
									user.setPassword(passwordtxt);
									
									//user.put("Address", houseS);
									
									user.put("Contact", Long.parseLong(number.getText().toString()));
									user.put("PasswordEnabled", true);
									// user.put("Number", phone);
									// user.put("Address", addresstxt);
									Log.i("pass1", passwordtxt);

									// Update information in Customer table on
									// Parse
									/*
									 * ParseObject customer = new
									 * ParseObject("Customer");
									 * customer.put("Username", usernametxt);
									 * customer.put("Number", phone);
									 * customer.put("Address", addresstxt);
									 * customer.saveInBackground();
									 * ParseInstallation
									 * .getCurrentInstallation()
									 * .saveInBackground();
									 */

									user.signUpInBackground(new SignUpCallback() {
										public void done(ParseException e) {
											if (e == null) {
												// Show a simple Toast message
												// upon successful registration
												Toast toast =Toast.makeText(
														getActivity(),
														"Successfully Signed up, please log in.",
														Toast.LENGTH_SHORT);
												toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
												toast.show();
												Fragment fragment = new LoginFragment();
												Bundle args = new Bundle();
												args.putString("selectedcity", selectedcity);
												//Log.d("error", selectedCity);
												fragment.setArguments(args);
												FragmentManager fragmentManager = getFragmentManager();
												FragmentTransaction transaction = getFragmentManager()
														.beginTransaction();
												// transaction.addToBackStack(null);
												InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			                        			imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

												transaction.replace(
														R.id.frame_container,
														fragment).commit();
											} else {
												Toast toast =Toast.makeText(getActivity(),
														"Sign up Error",
														Toast.LENGTH_SHORT);
												toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
												toast.show();
											}
										}
									});
								}
							} else {
								Toast toast = Toast.makeText(
										getActivity(),
										"That username already exists, please choose another one.",
										Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
								toast.show();

							}

						} else {
							// Something went wrong.

							// Force user to fill up the form

						}
					}
				});

			}

		});

	}

}
