package com.example.pikpoktest;

import java.util.ArrayList;

import com.example.pikpoktest.FeaturedFragment.OnFeaturedSelected;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

public class PickAndDropFragment extends Fragment {
	ImageView shop, pickAndDrop;
	
	public interface OnModeSelected {
		public void modeSelected(String mode);
	}

	

	OnModeSelected mCallback;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (OnModeSelected) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnModeSelectedListener");
		}
	}

	
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		ViewGroup root = (ViewGroup) inflater.inflate(R.layout.home_activity,
				null);
		shop = (ImageView) root.findViewById(R.id.iVShop);
		pickAndDrop = (ImageView) root.findViewById(R.id.iVPickAndDrop);
		return root;

	}

	@Override
	public void onResume() {
		super.onResume();
		initClickListeners();
	}

	private void initClickListeners() {
		shop.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				mCallback.modeSelected("shop");
			}
		});

		pickAndDrop.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mCallback.modeSelected("pick");
				
			}
		});
	}
}
