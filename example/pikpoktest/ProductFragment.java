package com.example.pikpoktest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.loopj.android.image.SmartImageView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
//import android.app.Fragment;
//import android.app.ListFragment;

public class ProductFragment extends Fragment{
	
	OnProductSelectedListener mCallback;
	
	private ArrayList<SubcategorySinglerow> categoryList = new ArrayList<SubcategorySinglerow>(); 

	
	//ListView l;
	ArrayList<SingleRow> list; 
	ArrayList<String> array;
	List<ParseObject> ob;
	ViewGroup root;
	LinearLayout layout, banner;
	ProductsAdapter adapter;   
	SmartImageView iv;
	String StoreID;
	String bannerurl;
	View header;
	TextView storename;
	ExpandableListAdapter listAdapter;
	ExpandableListView expListView;
	List<String> listDataHeader;
	HashMap<String, List<String>> listDataChild; 
	
	
	public interface OnProductSelectedListener {
        public void productSelected(int id);
    }
	
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnProductSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnProductSelectedListener");
        }
    }
	
	
	   @Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		//array = getArguments().getIntegerArrayList("list");
		
	}

	   
	   @Override
	public void onResume() {
		// TODO Auto-generated method stub
		    list = new ArrayList<SingleRow>();  
			array = new ArrayList<String>();
			array = getArguments().getStringArrayList("list");
			StoreID = getArguments().getString("StoreID");
			super.onResume();
			getBanner();
	//		populateListView();
			prepareListData();
	}
	   
	   private void prepareListData() {
		// TODO Auto-generated method stub
		   listDataHeader = new ArrayList<String>();
			listDataChild = new HashMap<String, List<String>>();
			
			final HashMap<String, String> test = new HashMap<String, String>();
			 final Multimap<String, String> multiMap = ArrayListMultimap.create();
			 
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Products");
			// query.whereEqualTo("Category", c);
			
				query.whereEqualTo("StoreID", StoreID);
				query.setLimit(1000);
			
			query.findInBackground(new FindCallback<ParseObject>() {
				public void done(List<ParseObject> objects, ParseException e) {
					if (e == null) {
						ob = objects; 
						for (int i = 0; i < objects.size(); i++) {
							ArrayList<String> cat; 
							Log.d("sizeee", ""+objects.size());
							//ArrayList<String> subcat; 
							// Log.d("abcde",
							// ""+objects.get(i).getInt("Price"));
							// Log.d("abcde",
							// ""+objects.get(i).getString("Description"));
							// Log.d("abcdefg",
							// ""+objects.get(i).getInt("ProductID"));
							
							cat = (ArrayList<String>) objects.get(i).get("SubCategory");
							for(int j = 0 ; j<cat.size(); j++){
								multiMap.put(cat.get(j),objects.get(i).getString("Name"));
							//test.put(cat.get(j),objects.get(i).getString("Name") );
							}
							//listDataHeader.add(objects.get(i).getString("Description"));
							// array.add(objects.get(i).getString("Subcategory"));
						}
						storename.setText(objects.get(0).getString("Store"));
						 Set<String> keys = multiMap.keySet();
						for(String key : keys){
							List<String> child = new ArrayList<String>();
							child.addAll(multiMap.get(key));
							listDataHeader.add(key);
							listDataChild.put(key, child);
							Log.d("testing", (key+""+multiMap.get(key)));
						}
					//	listAdapter = new ExpandableAdapter(getActivity(), listDataHeader, listDataChild);
						
						expListView.setAdapter(listAdapter);
					} else {
						Log.d("score", "Error: " + e.getMessage());
					}
				}});
	}


	@Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		  
	        root = (ViewGroup) inflater.inflate(R.layout.productsexpandable, null);
	     //   myList=(ExpandableListView)root.findViewById(R.id.listView1);
	      //  layout = (LinearLayout) root.findViewById(R.id.progressbar_view);
	      //  banner = (LinearLayout) root.findViewById(R.id.banner);
	      
	        expListView = (ExpandableListView) root.findViewById(R.id.lvExp);
			// l=(ListView)root.findViewById(R.id.listView1);
			// layout = (LinearLayout) root.findViewById(R.id.progressbar_view);
			 //banner = (LinearLayout) root.findViewById(R.id.banner);
			header = inflater.inflate(R.layout.storebanner, null);
			iv = (SmartImageView) header.findViewById(R.id.storeBanner);
			storename = (TextView) header.findViewById(R.id.storeName);
	        //listener for child row click
	      //  myList.setOnChildClickListener(myListItemClicked);
	        //listener for group heading click
	       // myList.setOnGroupClickListener(myListGroupClicked);
	               
	        
	        return root;
	    }
	   
	   private void getBanner(){
		   
		 
		   ParseQuery<ParseObject> query = ParseQuery.getQuery("Store");
	    	query.whereEqualTo("objectId", StoreID);
	    	
	    	query.findInBackground(new FindCallback<ParseObject>() {
	    	    public void done(List<ParseObject> objects, ParseException e) {
	    	        if (e == null) {
	    	        	
	    	            for(int i=0;i<objects.size();i++){
	    	            	bannerurl = objects.get(0).getString("Banner");
	    	            }
	    	          
	    	        } else {
	    	            Log.d("score", "Error: " + e.getMessage());
	    	        }
	    	        try {
	    	        	if(bannerurl==null){
	    	        		iv.setVisibility(View.GONE);
	    	        	}
	    	        	else{
						iv.setImageUrl(bannerurl);
						WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
						Display display = wm.getDefaultDisplay();
						DisplayMetrics metrics = new DisplayMetrics();
						wm.getDefaultDisplay().getMetrics(metrics);
           //  metrics.heightPixels;
						//metrics.widthPixels;
						LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(metrics.widthPixels,metrics.heightPixels/4);
						iv.setLayoutParams(layoutParams);
						iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
						iv.setPadding(5,25, 5, 10);
						}
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
	    	    }
	    	});
	    	
	   }
	 private void populateListView() {
	    	// TODO Auto-generated method stub	    	         
	    	        new Task().execute();
	    	      
	   }
	 
	 class Task extends AsyncTask<String, Integer, Boolean> {
		    @Override
		    protected void onPreExecute() {
		        layout.setVisibility(View.VISIBLE);
		   //     myList.setVisibility(View.GONE);
		        super.onPreExecute();
		    }

		    @Override
		    protected void onPostExecute(Boolean result) {
		    	ParseQuery<ParseObject> query = ParseQuery.getQuery("Products");
		    	//query.whereEqualTo("Category", c);
			    query.whereEqualTo("StoreID", StoreID);
			    		
		    	query.findInBackground(new FindCallback<ParseObject>() {
		    	    public void done(List<ParseObject> objects, ParseException e) {
		    	        if (e == null) {
		    	        	ob = objects;
		    	            for(int i=0;i<objects.size();i++){
		    	            //	Log.d("abcde", ""+objects.get(i).getInt("Price"));
		    	            	//Log.d("abcde", ""+objects.get(i).getString("Description"));
		    	            	//Log.d("abcdefg", ""+objects.get(i).getobjectId());
		    	            	
		    	         //   	SingleRow temp = new SingleRow(objects.get(i).getString("Name"),objects.get(i).getString("Description"),objects.get(i).getInt("Price"),objects.get(i).getString("Store"),objects.get(i).getObjectId(),1);
		    	           // 	list.add(temp);
		    	            //	Log.d("abcd",""+temp.getDescription());
		    	       //     array.add(objects.get(i).getString("Subcategory")); 
		    	            }
		    	           storename.setText(objects.get(0).getString("Store"));
		    	        } else {
		    	            Log.d("score", "Error: " + e.getMessage());
		    	        }
		    	            }
		    	});
		   //     myList.addHeaderView(header, null, false);
    	        adapter = new ProductsAdapter(getActivity(),list);
    	     //   myList.setAdapter(new ProductsAdapter(getActivity(),list)); 
		        layout.setVisibility(View.GONE);
		       // myList.setVisibility(View.VISIBLE);
		       // adapter.notifyDataSetChanged();
		        expListView.addHeaderView(header,null,false);
		        super.onPostExecute(result);
		        
		    }

			@Override
			protected Boolean doInBackground(String... params) {
				// TODO Auto-generated method stub
			
				return null;
			}

		  
		}
	 
	 
	
		  
		 //our group listener
		
		 
		 //here we maintain our products in various departments
	 
}
	   
	    

