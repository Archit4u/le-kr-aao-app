package com.example.pikpoktest;

public class CategoryRow {

		String category;
		String pic;
		
		public CategoryRow(String category, String pic){
			this.category = category;
			this.pic = pic;
		}
		
		public String getCategory() {
			return category;
		}

		public void setCategory(String category) {
			this.category = category;
		}

		public String getPic() {
			return pic;
		}
		

		public void setPic(String pic) {
			this.pic = pic;
		}
		
		
}
