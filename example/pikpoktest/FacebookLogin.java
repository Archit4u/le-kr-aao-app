package com.example.pikpoktest;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.People.LoadPeopleResult;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class FacebookLogin extends Activity implements ConnectionCallbacks,
		OnConnectionFailedListener, ResultCallback<People.LoadPeopleResult> {
	private Button loginButton;
	SignInButton btnSignIn;
	private Dialog progressDialog;
	int counter = 0;
	public static GoogleApiClient mGoogleApiClient;
	private static final int RC_SIGN_IN = 0;
	public static boolean facebookUser = false;
	public static boolean googleUser = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		
		
		
		setContentView(R.layout.facebooklogin);
		btnSignIn = (SignInButton) findViewById(R.id.btn_sign_in);
		ActionBar actionBar = getActionBar();
		actionBar.hide();
		loginButton = (Button) findViewById(R.id.iVloginButton);
		// loginButton.setReadPermissions(Arrays.asList("public_profile"));
		// loginButton.setReadPermissions(Arrays.asList("public_profile"));
		
		loginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d("LOGIN", "button");
				facebookUser = true;
				onLoginButtonClicked();
			}
		});

		btnSignIn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				signInWithGplus();
			}

			private void signInWithGplus() {
				// TODO Auto-generated method stub
				if (!mGoogleApiClient.isConnecting()) {
					mSignInClicked = true;
					resolveSignInError();
				}
			}
		});

		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this).addApi(Plus.API, null)
				.addScope(Plus.SCOPE_PLUS_LOGIN).build();

		// Check if there is a currently logged in user
		// and they are linked to a Facebook account.
		ParseUser currentUser = ParseUser.getCurrentUser();
		if ((currentUser != null) && ParseFacebookUtils.isLinked(currentUser)) {
			// Go to the user info activity
			facebookUser = true;
			showUserDetailsActivity();
		}

	}

	private boolean mIntentInProgress;
	private boolean mSignInClicked;

	private ConnectionResult mConnectionResult;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.d("LOGIN", "ar");
		if (facebookUser) {
			ParseFacebookUtils.finishAuthentication(requestCode, resultCode,
					data);
		}
		if (requestCode == RC_SIGN_IN) {
			if (resultCode != RESULT_OK) {
				mSignInClicked = false;
			}

			mIntentInProgress = false;

			if (!mGoogleApiClient.isConnecting()) {
				mGoogleApiClient.connect();
			}
		}
	}

	private void resolveSignInError() {
		if (mConnectionResult.hasResolution()) {
			try {
				mIntentInProgress = true;
				mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
			} catch (SendIntentException e) {
				mIntentInProgress = false;
				mGoogleApiClient.connect();
			}
		}
	}

	private void onLoginButtonClicked() {
		counter = 1;
		// FacebookLogin.this.progressDialog = ProgressDialog.show(
		// FacebookLogin.this, "", "Logging in...", true);
		List<String> permissions = Arrays.asList("public_profile", "email",
				"user_friends", "user_about_me", "user_relationships",
				"user_birthday", "user_location");
		ParseFacebookUtils.logIn(permissions, this, new LogInCallback() {
			@Override
			public void done(ParseUser user, ParseException err) {

				if (user == null) {
					Log.d("fb", "Uh oh. The user cancelled the Facebook login.");
					// FacebookLogin.this.progressDialog.dismiss();

				} else if (user.isNew()) {
					Log.d("fb",
							"User signed up and logged in through Facebook!");
					showUserDetailsActivity();
				} else {
					Log.d("fb", "User logged in through Facebook!");
					showUserDetailsActivity();
				}
			}
		});
	}

	private void showUserDetailsActivity() {
		// if(counter == 1)
		// FacebookLogin.this.progressDialog.dismiss();
		Intent intent = new Intent(this, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		if(googleUser){
		intent.putExtra("email", email);
		intent.putExtra("name", name);
		}
		startActivity(intent);
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
		if (!result.hasResolution()) {
			GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
					0).show();
			return;
		}

		if (!mIntentInProgress) {
			// Store the ConnectionResult for later usage
			mConnectionResult = result;

			if (mSignInClicked) {
				// The user has already clicked 'sign-in' so we attempt to
				// resolve all
				// errors until the user is signed in, or they cancel.
				resolveSignInError();
			}
		}
	}

	protected void onStart() {
		super.onStart();
		mGoogleApiClient.connect();
	}

	protected void onStop() {
		super.onStop();
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}

	String email,name;

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		Log.d("COMING", "ooncencenoecon");
		mSignInClicked = false;
		googleUser = true;
		Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(
				this);
		email = Plus.AccountApi.getAccountName(mGoogleApiClient);
		 if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
		Person currentPerson = Plus.PeopleApi
                .getCurrentPerson(mGoogleApiClient);
		name = currentPerson.getDisplayName();
		Log.d("GOOOOOGLE NAME", name);
		 }
		userSignup();
		// Toast.makeText(getApplicationContext(), email, 1000).show();
		// getProfileInformation();
		showUserDetailsActivity();
	}

	private void userSignup() {
		// TODO Auto-generated method stub
		
		ParseUser user = new ParseUser();
		user.setUsername(email);
		user.setPassword("null");
		

		user.signUpInBackground(new SignUpCallback() {
			public void done(ParseException e) {
				if (e == null) {
					// Hooray! Let them use the app now.
					Log.d("Signup", "was successful");

				} else {
					// Sign up didn't succeed. Look at the
					// ParseException
					// to figure out what went wrong
					Log.d("Signup", "Error:" + e.getMessage());
				}
			}
		});

	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		mGoogleApiClient.connect();
	}

	private void getProfileInformation() {
		try {
			if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {

				String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
				Log.e("COMING", "email: " + email);

			} else {
				Toast.makeText(getApplicationContext(),
						"Person information is null", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onResult(LoadPeopleResult arg0) {
		// TODO Auto-generated method stub

	}

}
