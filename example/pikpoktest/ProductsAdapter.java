package com.example.pikpoktest;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ProductsAdapter extends BaseAdapter {

	Context c;
	ArrayList<SingleRow> list;
	int pos;
	int i;

	public ProductsAdapter(Context baseContext, ArrayList<SingleRow> list) {
		this.c = baseContext;
		this.list = list;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View row = LayoutInflater.from(c).inflate(R.layout.singlerow, null);

		TextView name = (TextView) row.findViewById(R.id.textView1);
		TextView qty = (TextView) row.findViewById(R.id.qty);
		//final TextView incart = (TextView) row.findViewById(R.id.incart);
	//	incart.setVisibility(View.GONE);
		// TextView desc = (TextView) row.findViewById(R.id.textView2);
		TextView price = (TextView) row.findViewById(R.id.textView3);
		// TextView store = (TextView) row.findViewById(R.id.textView4);
		final ImageView add = (ImageView) row.findViewById(R.id.bAdd);
		final ImageView plus = (ImageView) row.findViewById(R.id.bPlus);
		final ImageView minus = (ImageView) row.findViewById(R.id.bMinus);
		// SmartImageView myImage = (SmartImageView)
		// row.findViewById(R.id.my_image);
		final SingleRow temp = list.get(position);

		qty.setText("" + temp.getQty());
		Log.d("abcimage", "" + temp.getProductId());
		// pos = temp.getProductId();
		// myImage.setImageUrl(temp.getUrl());
		name.setText(temp.getName());
		// desc.setText(temp.getDescription());
		price.setText(temp.getPrice());
		// store.setText(temp.getStore());

		for (int i = 0; i < MainActivity.getMyList().size(); i++) {
			Log.d("pricelist", ""
					+ MainActivity.getMyList().get(i).getProductId());
			if (temp.getProductId() == MainActivity.getMyList().get(i)
					.getProductId()) {
				add.setEnabled(false);
				plus.setEnabled(false);
				minus.setEnabled(false);
				minus.setVisibility(View.GONE);
				plus.setVisibility(View.GONE);
				qty.setVisibility(View.GONE);
				/*
				 * if(incart.getTag()!=null)
				 * incart.setText(""+incart.getTag().toString
				 * ()+" nos. in cart"); incart.setVisibility(View.VISIBLE);
				 */

			}
		}

		if (add.isEnabled()) {
			add.setBackgroundResource(R.drawable.cart);
		} else {
			add.setBackgroundResource(R.drawable.cartdisabled);
		}
		add.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(c, "Item Added to Cart!", Toast.LENGTH_SHORT)
						.show();
				// String str=(String) v.getTag();
				// Log.d("pricesadas", str);

				AddToCart obj = new AddToCart(temp.getProductId(), temp
						.getQty());
				MainActivity.getMyList().add(obj);
				//incart.setText("" + temp.getQty() + "nos. in cart");
				add.setEnabled(false);
				add.setBackgroundResource(R.drawable.cartdisabled);
				add.setTag(temp.getProductId());
			///	incart.setTag(temp.getQty());
				Log.d("price", add.getTag().toString());
				Activity m = (Activity) c;
				m.invalidateOptionsMenu();
				// ActionBarRefresh.refreshActionBarMenu(c);

				notifyDataSetChanged();
			}
		});
		plus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				temp.setQty(temp.getQty() + 1);
				notifyDataSetChanged();
			}
		});

		minus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (temp.getQty() > 1) {
					temp.setQty(temp.getQty() - 1);
					notifyDataSetChanged();
				}
			}
		});
		if (add.getTag() != null)
			Log.d("pricedsaa", add.getTag().toString());
		return row;
	}

}
