package com.example.pikpoktest;

import java.io.Serializable;

public class AddToCart implements Serializable,Comparable<AddToCart>{
	String productId;
	int qty;
	
	public AddToCart(String productId, int qty){
		this.productId = productId;
		this.qty = qty;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	@Override
	public int compareTo(AddToCart another) {
		// TODO Auto-generated method stub
		String anotherID = another.getProductId();
		return 0;
	}

	
}
