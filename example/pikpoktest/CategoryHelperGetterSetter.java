package com.example.pikpoktest;

public class CategoryHelperGetterSetter {
	
	private String name;
	private String desc;
	
	public CategoryHelperGetterSetter(String name,String desc ){
		this.name = name;
		this.desc = desc;
	}
	
	public CategoryHelperGetterSetter(){
	}
	
	//======================Getter setter=================================
	
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	public void setDesc(String desc){
		this.desc = desc;
	}
	public String getDesc(){
		return this.desc;
	}

}
