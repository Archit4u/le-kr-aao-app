package com.example.pikpoktest;

import in.varunbhalla.jsontest.ServiceHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class ConfirmationWithoutLogin extends Activity implements
		OnClickListener {

	String address;
	Long contactN;
	Number pinN;
	String houseS, suburbS, streetoneS, cityS, stateS, emailS, numberS,
			pincodeS, nameS;
	EditText house, streetone, streettwo, pin, email, contact, name;
	Spinner city, state, suburb;
	Button confirm, call;
	// RadioGroup radiogroup;
	List<AddToCart> myList = new ArrayList<AddToCart>();
	TextView tVadd, price, delivery, amount;
	// RadioButton Rbadd;
	int j = 0, tprice, tproducts;
	int ordercharge, deliverycharge = 25;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.confirmationwithoutlogin);

		Bundle extras = getIntent().getExtras();

		myList = (List<AddToCart>) extras.getSerializable("cart");
		ordercharge = extras.getInt("amount");

		Log.d("show", "" + extras.getInt("amount"));

		for (int i = 0; i < myList.size(); i++) {
			Log.d("intent", "" + myList.get(i).getProductId() + ""
					+ myList.get(i).getQty());
		}

		// radiogroup = (RadioGroup) findViewById(R.id.radioGroup1);
		name = (EditText) findViewById(R.id.eTname);
		house = (EditText) findViewById(R.id.editText1);
		streetone = (EditText) findViewById(R.id.editText2);
		suburb = (Spinner) findViewById(R.id.Ssuburb);
		city = (Spinner) findViewById(R.id.Scity);
		state = (Spinner) findViewById(R.id.Sstate);
		pin = (EditText) findViewById(R.id.editText6);
		email = (EditText) findViewById(R.id.EditText02);
		contact = (EditText) findViewById(R.id.EditText01);

		ArrayAdapter<CharSequence> adaptercities = ArrayAdapter
				.createFromResource(this, R.array.cities,
						android.R.layout.simple_spinner_item);

		ArrayAdapter<CharSequence> adapterstate = ArrayAdapter
				.createFromResource(this, R.array.state,
						android.R.layout.simple_spinner_item);
		
		ArrayAdapter<CharSequence> adaptersuburbs = ArrayAdapter
				.createFromResource(this, R.array.suburbs,
						android.R.layout.simple_spinner_item);
		
		adaptercities
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adapterstate
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adaptersuburbs
		.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		

		city.setAdapter(adaptercities);

		state.setAdapter(adapterstate);
		
		suburb.setAdapter(adaptersuburbs);
		
		city.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				cityS = (String) arg0.getItemAtPosition(arg2);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				cityS = (String) arg0.getItemAtPosition(0);

			}
		});
		
		state.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				stateS = (String) arg0.getItemAtPosition(arg2);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				stateS = (String) arg0.getItemAtPosition(0);
			}
		});
		
		suburb.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				suburbS = (String) arg0.getItemAtPosition(arg2);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				suburbS = (String) arg0.getItemAtPosition(0);

			}
		});
		
		name.setFocusable(true);
		house.setFocusable(true);
		streetone.setFocusable(true);
		//streettwo.setFocusable(true);
		// city.setFocusable(true);
		// state.setFocusable(true);
		pin.setFocusable(true);
		email.setFocusable(true);
		contact.setFocusable(true);

	/*	if (ordercharge >= 500) {
			deliverycharge = 0;
		} else {
			deliverycharge = 50;
		}*/

		price = (TextView) findViewById(R.id.textView2);
		delivery = (TextView) findViewById(R.id.textView3);
		amount = (TextView) findViewById(R.id.textView4);

		price.setText("The price of items ordered is Rs. " + ordercharge + " *");
		delivery.setText("The delivery charge is Rs. " + deliverycharge);
		amount.setText("The total bill amount is Rs. "
				+ (ordercharge + deliverycharge)+ " *");

		// Rbadd = (RadioButton) findViewById(R.id.address);
		call = (Button) findViewById(R.id.bCall);
		confirm = (Button) findViewById(R.id.bConfirm);

		call.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String url = "tel:+91 9899363678";
				Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
				startActivity(intent);
			}
		});

		confirm.setOnClickListener(this);

		pin.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView view, int actionId,
					KeyEvent event) {
				pincodeS = pin.getText().toString();

				if (pincodeS.length() != 6) {

					Toast toast = Toast.makeText(getBaseContext(),
							"Enter a proper 6 digit pincode!!!",
							Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
					toast.show();
					return true;
				} else {
					return false;
				}
			}
		});

		contact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView view, int actionId,
					KeyEvent event) {
				numberS = contact.getText().toString();

				if (numberS.length() != 10) {

					Toast toast = Toast.makeText(getBaseContext(),
							"Enter a 10 digit phone number!!!",
							Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
					toast.show();
					return true;
				} else {
					return false;
				}
			}
		});

		email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView view, int actionId,
					KeyEvent event) {

				emailS = email.getText().toString();

				final Pattern Email = Pattern
						.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
								+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "("
								+ "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}"
								+ ")+");

				if (!Email.matcher(emailS).matches()) {

					Toast toast = Toast.makeText(getBaseContext(),
							"Enter a valid email id", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
					toast.show();
					return true;
				} else {
					return false;
				}
			}
		});

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// Toast.makeText(this,
		// ""+radiogroup.indexOfChild(findViewById(radiogroup.getCheckedRadioButtonId())),
		// Toast.LENGTH_SHORT).show();

		final ArrayList<String> pid = new ArrayList<String>();
		final ArrayList<Integer> qty = new ArrayList<Integer>();
		for (int i = 0; i < myList.size(); i++) {
			// Log.d("orderfinal",
			// "id"+myList.get(i).getProductId()+"  q"+myList.get(i).getQty()+" q"
			// +list.get(i).getQty()
			// +" name"+list.get(i).getName());
			pid.add(myList.get(i).getProductId());
			qty.add(myList.get(i).getQty());
		}

		// if (radiogroup.indexOfChild(findViewById(radiogroup
		// .getCheckedRadioButtonId())) == 2) {
		if (name.getText().toString().equals("")
				|| house.getText().toString().equals("")
				|| streetone.getText().toString().equals("")
				|| contact.getText().toString().equals("")
				||email.getText().toString().equals("")
				|| pin.getText().toString().equals("")) {

			Toast toast = Toast.makeText(getBaseContext(), "Please fill in all the fields.",
					Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			toast.show();
			
		}else if(contact.getText().toString().length()!=10 || pin.getText().toString().length()!=6){
			Toast toast = Toast.makeText(getBaseContext(), "Please enter a valid contact number and pincode.",
					Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			toast.show();} 
		else {

			emailS = email.getText().toString();
			ParseQuery<ParseUser> query = ParseUser.getQuery();
			query.whereEqualTo("username", emailS);
			query.findInBackground(new FindCallback<ParseUser>() {
				@SuppressWarnings("static-access")
				@Override
				public void done(List<ParseUser> objects, ParseException e) {
					// TODO Auto-generated method stub
					if (objects.size() == 0) {

						Log.d("KJ", "User doesnot exist!!");
						nameS = name.getText().toString();
						Log.d("name", nameS);
						houseS = house.getText().toString();
						streetoneS = streetone.getText().toString();
						//streettwoS = streettwo.getText().toString();
						
						pinN = Integer.parseInt(pin.getText().toString());
						contactN = Long.parseLong(contact.getText().toString());

						Log.i("name", emailS);
						ParseUser user = new ParseUser();
						user.setUsername(emailS);
						user.setPassword("null");
						user.put("HouseNumber", houseS);
						// user.put("Address", houseS);
						user.put("Name", nameS);
						user.put("Address1", streetoneS);
						user.put("Address2", suburbS);
						user.put("City", cityS);
						user.put("State", stateS);
						user.put("Pincode", pinN);
						user.put("Contact", contactN);
						user.put("PasswordEnabled", false);
						Log.i("name1", emailS);

						user.signUpInBackground(new SignUpCallback() {
							public void done(ParseException e) {
								if (e == null) {
									// Hooray! Let them use the app now.
									Log.d("Signup", "was successful");

								} else {
									// Sign up didn't succeed. Look at the
									// ParseException
									// to figure out what went wrong
									Log.d("Signup", "Error:" + e.getMessage());
								}
							}
						});

					} else {
						Log.d("object exist", "" + objects.size());
						// ParseUser c = ParseUser.getCurrentUser();
						// String usernametxt = c.getUsername();
						for (int i = 0; i < objects.size(); i++) {

							Log.d("User", "" + objects.get(i).getUsername());
							ParseUser.logInInBackground(objects.get(i)
									.getUsername(), "null",
									new LogInCallback() {
										@Override
										public void done(ParseUser user,
												ParseException e) {
											// TODO Auto-generated method stub

											if (e == null) {
												// Hooray! Let them use the app
												// now.
												Log.d("login", "was successful");

											} else {
												// Sign up didn't succeed. Look
												// at the ParseException
												// to figure out what went wrong
												Log.d("login",
														"Error:"
																+ e.getMessage());
											}

										}
									});
						}
					}
				}
			});

			address = house.getText().toString() + ", "
					+ streetone.getText().toString() + ", "
					+ suburbS + ", "
					+ cityS + ", "
					+ stateS + ", "
					+ pin.getText().toString();
			contactN = Long.parseLong(contact.getText().toString());
			confirm.setEnabled(false);
			tproducts = pid.size();
			tprice = ordercharge+deliverycharge;

			ParseQuery<ParseObject> query1 = ParseQuery.getQuery("Order");
			query1.setLimit(1000);

			// query.whereEqualTo("Category", "Food");
			query1.findInBackground(new FindCallback<ParseObject>() {
				public void done(List<ParseObject> objects, ParseException e) {
					if (e == null) {

						for (int i = 0; i < objects.size(); i++) {
							// Log.d("adapter", "" +
							// objects.get(i).getParseFile("image").getUrl());
							int order = objects.get(i).getInt("OrderID");
							//Log.i("orderid", "" + order);
							if (j < order) {
								j = order;
							}

						}

					} else {
						Log.d("score", "Error: " + e.getMessage());
					}
					nameS = name.getText().toString();

					pinN = Integer.parseInt(pin.getText().toString());
					new GetContacts().execute();

					
					ParseObject order2 = new ParseObject("Order");
					List<ParseObject> objects2 = new ArrayList<ParseObject>();
					order2.put("OrderID", j + 1);
					order2.put("username", emailS);
					Log.d("name", nameS);
					order2.put("Name", nameS);
					order2.put("Contact", contactN);
					order2.put("ProductID", pid);
					order2.put("Quantity", qty);
					order2.put("Email", emailS);
					order2.put("Address", address);
					order2.put("Status", 0);
					order2.put("Pincode", pinN);
					order2.put("DeliveryCharge", deliverycharge);
					order2.put("TotalPrice", ordercharge + deliverycharge);
					order2.saveInBackground();
					Toast toast = Toast.makeText(
							getBaseContext(),
							"Order placed! View the current order status by navigating to My orders.",
							Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
					toast.show();
					MainActivity.getMyList().clear();
					
					Intent in = new Intent(getBaseContext(), MainActivity.class);
					in.putExtra("confirmation", true);
					startActivity(in);

				}
			});

		}

	}
	 private class GetContacts extends AsyncTask<Void, Void, Void> {
		 
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	            // Showing progress dialog
	           
	 
	        }
	 
	        @Override
	        protected Void doInBackground(Void... arg0) {
	            // Creating service handler class instance
	            ServiceHandler sh = new ServiceHandler();
	            String url = "http://107.170.68.128:8080/lekaraao/mail.jsp?";
	            url = url+"&token=v2lekaraao"+"&email="+emailS+"&tproducts="+tproducts+"&tprice="+tprice;
	            Log.d("URL", url);
	            // Making a request to url and getting response
	            String jsonStr = sh.makeServiceCall(url,
	            		ServiceHandler.GET);
	            //Toast.makeText(getApplicationContext(), "dsa" +jsonStr, Toast.LENGTH_SHORT).show();
	            Log.d("Response: ", "> " + jsonStr);
	 
	 
	            return null;
	        }
	 
	        @Override
	        protected void onPostExecute(Void result) {
	            super.onPostExecute(result);
	          
	        }
	 
	    }
}
