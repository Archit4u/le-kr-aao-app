package com.example.pikpoktest;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAdapter  extends BaseAdapter{
	Context context;
    List<CategoryHelperGetterSetter> list;
    LayoutInflater lif;
    
	public MyAdapter(Context context,List<CategoryHelperGetterSetter> list) {
		this.context = context;
        lif = LayoutInflater.from(this.context);
        this.list = list;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return this.list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		 return this.list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		 return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		 LafoutHolder holder;
	        if(convertView==null){
	            convertView = lif.inflate(R.layout.rowitem, null);
	            holder = new LafoutHolder();
	            holder.tv = (TextView) convertView.findViewById(R.id.productHeader);
	            holder.name = (TextView) convertView.findViewById(R.id.productDescription);
	           //holder.ivFood = (ImageView) convertView.findViewById(R.id.productImage);
	            convertView.setTag(holder);
	        }else{
	            holder = (LafoutHolder) convertView.getTag();
	        }
	        //holder.tv.setText(this.list.get(position).getCategory()+" "+this.list.get(position).getItem()+" Q="+this.list.get(position).getQuantity()+" C="+this.list.get(position).getComment()+" T="+this.list.get(position).getTableNo());
	        holder.tv.setText(list.get(position).getName());
	        holder.name.setText(list.get(position).getDesc());
	        /*holder.defaultComment.setVisibility(View.VISIBLE);
	        holder.defaultComment.setText("deffalut comment");*/
	      /*  holder.ivFood.setImageResource(R.drawable.food);*/
	        
	        return convertView;
	    }
	    private class LafoutHolder{
	        TextView tv,name,defaultComment;
	        Button btStart;
	        ImageView ivFood;
	    }


	}


