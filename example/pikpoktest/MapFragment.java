package com.example.pikpoktest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

public class MapFragment extends Fragment{
	
	GoogleMap map;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {

	    return inflater.inflate(R.layout.mapfragment, container, false);
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	    getGoogleMap();

	}
	private GoogleMap getGoogleMap() {
         
		if (map == null && getActivity() != null && getActivity().getSupportFragmentManager()!= null) {
  //          SupportMapFragment smf = (SupportMapFragment)getActivity().getSupportFragmentManager().findFragmentById(R.id.mapview);
      //      if (smf != null) {
        //        map = smf.getMap();
            }
       // }
        return map;
    }
}

