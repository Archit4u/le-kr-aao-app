package com.example.pikpoktest;

public class CartRow {
	String name,price, store, unit,image;
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	int qty;
	String id;
	public CartRow(String name, int qty,int price, String string, String store, String unit, String image) {
		
		// TODO Auto-generated constructor stub
		this.name = name;
		this.qty = qty;
		//this.url = url;
		this.price = ""+price;
		this.id = string;
		this.store = store;
		this.unit = unit;
		this.image=image;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}

}
