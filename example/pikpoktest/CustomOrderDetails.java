package com.example.pikpoktest;

import in.varunbhalla.jsontest.ServiceHandler;

import java.io.ByteArrayOutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.parse.codec.binary.StringUtils;

public class CustomOrderDetails extends Activity implements OnClickListener {

	EditText toAddress, fromAddress, order, email, contact;
	ImageView ivThumbnailPhoto;
	Bitmap bitMap;
	static int TAKE_PICTURE = 1;
	ImageView btnTackPic;
	Button bOrder, call;
	String toAdd, fromAdd, orderDetails, emailId, contactNumber, to, from;
	boolean picTaken = false;
	int j, deliverCost, tproducts;
	TextView tVTo, tVFrom;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.customorderconfirmation);

		toAddress = (EditText) findViewById(R.id.eTTo);
		fromAddress = (EditText) findViewById(R.id.eTFrom);
		btnTackPic = (ImageView) findViewById(R.id.btnTakePic);
		ivThumbnailPhoto = (ImageView) findViewById(R.id.ivThumbnailPhoto);
		ivThumbnailPhoto.setVisibility(View.GONE);
		order = (EditText) findViewById(R.id.eTOrder);
		email = (EditText) findViewById(R.id.eTEmail);
		contact = (EditText) findViewById(R.id.eTPhone);
		tVTo = (TextView) findViewById(R.id.tVToSector);
		tVFrom = (TextView) findViewById(R.id.tVFromSector);
		Bundle bundle = getIntent().getParcelableExtra("bundle");
		email.setText("" + MainActivity.USER_EMAIL);
		deliverCost = bundle.getInt("cost");
		to = bundle.getString("to");
		from = bundle.getString("from");
		
		
		InputFilter filter = new InputFilter() {
			@Override
			public CharSequence filter(CharSequence source, int start, int end,
					Spanned dest, int dstart, int dend) {
				
				for (int i = start; i < end; i++) {
					if ((!Character.isLetterOrDigit(source.charAt(i)))&&(!(source.charAt(i)==' '))
							&&(!(source.charAt(i)==','))) {
						return "";
					}
				}
				return null;
			}

		};
		toAddress.setFilters(new InputFilter[]{filter}); 
		fromAddress.setFilters(new InputFilter[]{filter});
		order.setFilters(new InputFilter[]{filter});
		Log.d("SDS", to + from);
		// deliverCost = getIntent().getIntExtra("cost", 0);
		call = (Button) findViewById(R.id.bCall);

		call.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String url = "tel:+91 9899363678";
				Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
				startActivity(intent);
			}
		});
		// tVTo.setText(to);
		// tVFrom.setText(from);
		btnTackPic.setOnClickListener(this);
		bOrder = (Button) findViewById(R.id.bOrder);

		bOrder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fromAdd = fromAddress.getText().toString();
				toAdd = toAddress.getText().toString();
				emailId = email.getText().toString();
				contactNumber = contact.getText().toString();

				if (TextUtils.isEmpty(fromAdd) || TextUtils.isEmpty(toAdd)
						|| TextUtils.isEmpty(contactNumber)
						|| TextUtils.isEmpty(emailId)) {
					Toast.makeText(getBaseContext(),
							"Please fill in all the details.",
							Toast.LENGTH_SHORT).show();
				} else if (contactNumber.length() != 10) {
					Toast.makeText(getBaseContext(),
							"Please enter a valid mobile number.",
							Toast.LENGTH_SHORT).show();
				} else {

					placeOrder();

				}
			}

			private void placeOrder() {
				// TODO Auto-generated method stub
				// userSignup();
				bOrder.setEnabled(false);
				if (picTaken) {
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					// Compress image to lower quality scale 1 - 100
					bitMap.compress(Bitmap.CompressFormat.PNG, 100, stream);
					byte[] image = stream.toByteArray();

					// Create the ParseFile
					final ParseFile file = new ParseFile("order.png", image);
					// Upload the image into Parse Cloud
					file.saveInBackground();

					// Create a New Class called "ImageUpload" in Parse
					// ParseObject imgupload = new ParseObject("Order");

					// Create a column named "ImageName" and set the string
					// imgupload.put("Image", "AndroidBegin Logo");

					// Create a column named "ImageFile" and insert the image
					// imgupload.put("Image", file);

					// Create the class and the columns
					// imgupload.saveInBackground();

					ParseQuery<ParseObject> query1 = ParseQuery
							.getQuery("Order");
					query1.setLimit(1000);

					// query.whereEqualTo("Category", "Food");
					query1.findInBackground(new FindCallback<ParseObject>() {
						public void done(List<ParseObject> objects,
								ParseException e) {
							if (e == null) {

								for (int i = 0; i < objects.size(); i++) {
									// Log.d("adapter", "" +
									// objects.get(i).getParseFile("image").getUrl());
									int order = objects.get(i)
											.getInt("OrderID");
									// Log.i("orderid", "" + order);
									if (j < order) {
										j = order;
									}

								}

							} else {
								Log.d("score", "Error: " + e.getMessage());
							}

							new GetContacts().execute();

							ParseObject order2 = new ParseObject("Order");
							List<ParseObject> objects2 = new ArrayList<ParseObject>();
							order2.put("OrderID", j + 1);
							order2.put("username", MainActivity.USER_EMAIL);
							order2.put("Contact", Long.parseLong(contactNumber));
							order2.put("Email", emailId);
							order2.put("PickupAddress", fromAdd);
							order2.put("OrderDetails", "");
							order2.put("Address", toAdd);
							order2.put("OrderType", "PickandDrop");
							order2.put("Status", 0);
							order2.put("DeliveryCharge", deliverCost);
							order2.put("Image", file);
							order2.saveInBackground();
							Toast toast = Toast
									.makeText(
											getBaseContext(),
											"Order placed! View the current order status by navigating to My orders.",
											Toast.LENGTH_LONG);
							toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
							toast.show();
							MainActivity.getMyList().clear();

							Intent in = new Intent(getBaseContext(),
									MainActivity.class);
							in.putExtra("confirmation", true);
							startActivity(in);

						}
					});
				} else {

					orderDetails = order.getText().toString();
					if (TextUtils.isEmpty(orderDetails)) {
						Toast.makeText(
								getBaseContext(),
								"Please fill in the detials of items to be shipped.",
								Toast.LENGTH_SHORT).show();
					} else {
						// Place order with text
						ParseQuery<ParseObject> query1 = ParseQuery
								.getQuery("Order");
						query1.setLimit(1000);

						// query.whereEqualTo("Category", "Food");
						query1.findInBackground(new FindCallback<ParseObject>() {
							public void done(List<ParseObject> objects,
									ParseException e) {
								if (e == null) {

									for (int i = 0; i < objects.size(); i++) {
										// Log.d("adapter", "" +
										// objects.get(i).getParseFile("image").getUrl());
										int order = objects.get(i).getInt(
												"OrderID");
										// Log.i("orderid", "" + order);
										if (j < order) {
											j = order;
										}

									}

								} else {
									Log.d("score", "Error: " + e.getMessage());
								}

								new GetContacts().execute();

								ParseObject order2 = new ParseObject("Order");
								List<ParseObject> objects2 = new ArrayList<ParseObject>();
								order2.put("OrderID", j + 1);
								order2.put("username", MainActivity.USER_EMAIL);
								order2.put("Contact",
										Long.parseLong(contactNumber));
								order2.put("Email", emailId);
								order2.put("PickupAddress", fromAdd);
								order2.put("Address", toAdd);
								order2.put("OrderType", "PickandDrop");
								order2.put("OrderDetails", orderDetails);
								order2.put("Status", 0);
								order2.put("DeliveryCharge", deliverCost);
								order2.saveInBackground();
								Toast toast = Toast
										.makeText(
												getBaseContext(),
												"Order placed! View the current order status by navigating to My orders.",
												Toast.LENGTH_LONG);
								toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
								toast.show();
								MainActivity.getMyList().clear();

								Intent in = new Intent(getBaseContext(),
										MainActivity.class);
								in.putExtra("confirmation", true);
								startActivity(in);

							}
						});
					}
				}
			}

			private void userSignup() {
				// TODO Auto-generated method stub

				ParseQuery<ParseUser> query = ParseUser.getQuery();
				query.whereEqualTo("username", emailId);
				query.findInBackground(new FindCallback<ParseUser>() {
					@SuppressWarnings("static-access")
					@Override
					public void done(List<ParseUser> objects, ParseException e) {
						// TODO Auto-generated method stub
						if (objects.size() == 0) {

							Log.d("KJ", "User doesnot exist!!");

							ParseUser user = new ParseUser();
							user.setUsername(emailId);
							user.setPassword("null");
							// user.put("HouseNumber", houseS);
							// // user.put("Address", houseS);
							// user.put("Name", nameS);
							// user.put("Address1", streetoneS);
							// user.put("Address2", suburbS);
							// user.put("City", cityS);
							// user.put("State", stateS);
							// user.put("Pincode", pinN);
							user.put("Contact", Long.parseLong(contactNumber));
							user.put("PasswordEnabled", false);

							user.signUpInBackground(new SignUpCallback() {
								public void done(ParseException e) {
									if (e == null) {
										// Hooray! Let them use the app now.
										Log.d("Signup", "was successful");

									} else {
										// Sign up didn't succeed. Look at the
										// ParseException
										// to figure out what went wrong
										Log.d("Signup",
												"Error:" + e.getMessage());
									}
								}
							});

						} else {
							Log.d("object exist", "" + objects.size());
							// ParseUser c = ParseUser.getCurrentUser();
							// String usernametxt = c.getUsername();
							for (int i = 0; i < objects.size(); i++) {

								Log.d("User", "" + objects.get(i).getUsername());
								ParseUser.logInInBackground(objects.get(i)
										.getUsername(), "null",
										new LogInCallback() {
											@Override
											public void done(ParseUser user,
													ParseException e) {
												// TODO Auto-generated method
												// stub

												if (e == null) {
													// Hooray! Let them use the
													// app
													// now.
													Log.d("login",
															"was successful");

												} else {
													// Sign up didn't succeed.
													// Look
													// at the ParseException
													// to figure out what went
													// wrong
													Log.d("login",
															"Error:"
																	+ e.getMessage());
												}

											}
										});
							}
						}
					}
				});
			}
		});
	}

	@Override
	public void onClick(View view) {

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		startActivityForResult(intent, TAKE_PICTURE);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		tVTo.setText(to);
		tVFrom.setText(from);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {

		if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK
				&& intent != null) {

			Bundle extras = intent.getExtras();
			picTaken = true;
			// get bitmap
			bitMap = (Bitmap) extras.get("data");
			bitMap = Bitmap.createScaledBitmap(bitMap, 500, 500, false);
			order.setVisibility(View.GONE);
			btnTackPic.setVisibility(View.GONE);
			ivThumbnailPhoto.setVisibility(View.VISIBLE);
			ivThumbnailPhoto.setImageBitmap(bitMap);

		}
	}

	private class GetContacts extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog

		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// Creating service handler class instance
			ServiceHandler sh = new ServiceHandler();
			String url = "http://107.170.68.128:8080/mail.jsp?";
			//SimpleDateFormat df = new SimpleDateFormat("h:mm a");
			Calendar c = Calendar.getInstance();
			Date d = c.getTime();
			int orderNumber = j+1;
			
			String toFormatted = URLEncoder.encode(toAdd);
			String fromFormatted = URLEncoder.encode(fromAdd);
			
		//	String date = df.format(d);
		//	http://107.170.68.128:8080/mail.jsp?t
			//	oken=v2lekaraao&uname=Vivek&delivery_addr=
				//Delhi&pick_addr=Bangalore&contact=8867686066&order_no=101&order_time=10AM&exp_time=60min&email=vivek@foofys.com
			url = url + "token=v2lekaraao" +"&uname="+MainActivity.USER_EMAIL+"&delivery_addr="+toFormatted+"&pick_addr="
				+fromFormatted+"&contact="+Long.parseLong(contactNumber)+"&order_no="+orderNumber+"&order_time="+System.currentTimeMillis()+
					"&exp_time=60min"+
				"&email=" + emailId;
			Log.d("URL", url);
				//	+ "&tproducts=" + 1 + "&tprice=" + deliverCost;
			
			// Making a request to url and getting response
			String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);
			// Toast.makeText(getApplicationContext(), "dsa" +jsonStr,
			// Toast.LENGTH_SHORT).show();
			Log.d("Response: ", "> " + jsonStr);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

		}

	}

	// method to check if you have a Camera
	private boolean hasCamera() {
		return getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA);
	}

	// method to check you have Camera Apps
	private boolean hasDefualtCameraApp(String action) {
		final PackageManager packageManager = getPackageManager();
		final Intent intent = new Intent(action);
		List<ResolveInfo> list = packageManager.queryIntentActivities(intent,
				PackageManager.MATCH_DEFAULT_ONLY);

		return list.size() > 0;

	}
}
