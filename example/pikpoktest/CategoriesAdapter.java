package com.example.pikpoktest;

import java.util.ArrayList;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.image.SmartImageView;

public class CategoriesAdapter extends BaseAdapter {

	Context c;
	ArrayList<CategoryRow> list;

	public CategoriesAdapter(Context baseContext, ArrayList<CategoryRow> list) {
		this.c = baseContext;
		this.list = list;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Log.d("HELLO", "WORLD!!!!7");
		View row = LayoutInflater.from(c).inflate(R.layout.categoryrow, null);
		TextView category = (TextView) row.findViewById(R.id.title);
		SmartImageView iv = (SmartImageView) row.findViewById(R.id.list_image);
		String imageurl = list.get(position).getPic();
		iv.setImageUrl(imageurl);
		
		WindowManager wm = (WindowManager) c
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(metrics);
		// LinearLayout.LayoutParams layoutParams = new
		// LinearLayout.LayoutParams(metrics.widthPixels,metrics.heightPixels);
		// iv.setLayoutParams(layoutParams);
		iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
		// iv.setPadding(5,5,5,5);

		category.setText("" + list.get(position).getCategory());

		// icon.setBackgroundResource(list.get(position).getPic());
		return row;
	}

}
