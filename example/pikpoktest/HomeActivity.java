package com.example.pikpoktest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class HomeActivity extends Activity{
	
	ImageView shop, pickAndDrop;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_activity);
		shop = (ImageView) findViewById(R.id.iVShop);
		pickAndDrop = (ImageView) findViewById(R.id.iVPickAndDrop);
		initClickListeners();
	}

	private void initClickListeners() {
		shop.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(i);
			}
		});
		
		pickAndDrop.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(getBaseContext(), "Pick", Toast.LENGTH_SHORT).show();
			}
		});
	}
}
