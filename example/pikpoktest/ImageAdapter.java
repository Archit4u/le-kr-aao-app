package com.example.pikpoktest;

import java.util.ArrayList;

import com.loopj.android.image.SmartImageView;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ImageAdapter extends BaseAdapter {

	private Context ctx;
	ArrayList<String> list,storenames;

	public ImageAdapter(Context c, ArrayList<String> list, ArrayList<String> storenames) {
		ctx = c;
		this.list = list;
		this.storenames=storenames;
	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		// View row = LayoutInflater.from(ctx).inflate(R.layout.featuredrow,
		// null);
		// SmartImageView iv = (SmartImageView) row.findViewById(R.id.my_image);

		if (convertView == null) {
			convertView = LayoutInflater.from(ctx).inflate(
					R.layout.featurerow, null);
			holder = new ViewHolder();
			holder.thumbnail = (SmartImageView) convertView
					.findViewById(R.id.my_image);
			holder.storename = (TextView) convertView.findViewById(R.id.tVStore);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();

		}
		
		holder.thumbnail.setImageUrl(list.get(position));
		holder.storename.setText(storenames.get(position));
		WindowManager wm = (WindowManager) ctx
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(metrics);
		// metrics.heightPixels;
		// metrics.widthPixels;
		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
				metrics.widthPixels, metrics.heightPixels / 4);
		holder.thumbnail.setLayoutParams(layoutParams);
		holder.thumbnail.setScaleType(ImageView.ScaleType.CENTER_CROP);
	//	holder.thumbnail.setPadding(10, 0, 10, 0);
		return convertView;

		
		
		
		
		/*
		 * ImageView iv; if (convertView == null) { // if it's not recycled,
		 * initialize some attributes iv = new ImageView(ctx); WindowManager wm
		 * = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
		 * Display display = wm.getDefaultDisplay(); DisplayMetrics metrics =
		 * new DisplayMetrics(); wm.getDefaultDisplay().getMetrics(metrics); //
		 * metrics.heightPixels; //metrics.widthPixels; iv.setLayoutParams(new
		 * GridView.LayoutParams(metrics.widthPixels,metrics.heightPixels/5));
		 * iv.setScaleType(ImageView.ScaleType.CENTER_CROP); iv.setPadding(5,
		 * 10, 5, 10); } else { iv = (ImageView) convertView; }
		 * 
		 * iv.setImageResource(pics[position]); return iv;
		 */

	}

	/*
	 * private Integer[] pics={ R.drawable.kfc,R.drawable.subway,
	 * R.drawable.mcdonalds,R.drawable.reliancefresh,
	 * R.drawable.donutbaker,R.drawable.starbucks
	 * 
	 * };
	 */
	private static class ViewHolder {
		SmartImageView thumbnail;
		TextView storename;
	}
}