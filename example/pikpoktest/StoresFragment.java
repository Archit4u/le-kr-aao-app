package com.example.pikpoktest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.example.pikpoktest.SubcategoriesFragment.OnSubCategorySelectedListener;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class StoresFragment extends Fragment implements OnItemClickListener{

	OnStoreSelectedListener mCallback;
	String querys;
	String city = new String();
	ViewGroup root;
	ListView l;
	StoresAdapter adapter;
	protected ProgressDialog proDialog;

	
	public interface OnStoreSelectedListener {
		public void storeSelected(ArrayList<String> list, String StoreID);
	}

	ArrayList<String> array;
	ArrayList<Double> distarr;
	View header;
	List<ParseObject> ob;
	TextView locationoff, nostores;
	private static ArrayList<String> list = new ArrayList<String>();

	public static ArrayList<String> getList() {
	return list;
	}

	public static void setList(ArrayList<String> list) {


		StoresFragment.list = list;
	}

	
	protected void startLoading() {
		proDialog = new ProgressDialog(getActivity());
		proDialog.setCancelable(false);
		proDialog.show();
		proDialog.setContentView(R.layout.progressdialog);
		//proDialog.setMessage("loading...");
		//proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		
	}

	protected void stopLoading() {
		proDialog.dismiss();
		proDialog = null;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (OnStoreSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnStoreSelectedListener");
		}
	}

	/**
	 * @Override public View onCreateView(LayoutInflater inflater, ViewGroup
	 *           container,Bundle savedInstanceState) { Creating an array
	 *           adapter to store the list of countries ArrayAdapter<String>
	 *           adapter = new ArrayAdapter<String>(inflater.getContext(),
	 *           android.R.layout.simple_list_item_1,categories);
	 * 
	 *           /** Setting the list adapter for the ListFragment
	 *           setListAdapter(adapter);
	 * 
	 *           return super.onCreateView(inflater, container,
	 *           savedInstanceState); }
	 */

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		root = (ViewGroup) inflater.inflate(R.layout.storeslist, null);
		l = (ListView) root.findViewById(R.id.list);
		locationoff = (TextView) root.findViewById(R.id.LocationOff);
		nostores = (TextView) root.findViewById(R.id.NoStores);
		locationoff.setVisibility(View.GONE);
		nostores.setVisibility(View.GONE);
		header = inflater.inflate(R.layout.storesheader, null);
		return root;
	}
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		array = new ArrayList<String>();
		//header = LayoutInflater.from(getActivity()).inflate(R.layout.storesheader, null);
		if (getArguments() != null) {
			if (getArguments().containsKey("query")) {
				querys = getArguments().getString("query");
				Log.d("QUERY", ""+querys + ""+ getArguments().getString("selectedcity"));
// Toast.makeText(getActivity(), querys,
				// Toast.LENGTH_SHORT).show();
				city = getArguments().getString("selectedcity");
				populatelist();
			} else {
				city = getArguments().getString("selectedcity");
				populatelistview();
			}
		}
		/*
		 * else if(getArguments().containsKey("selectedcity")){
		 * if(getArguments()!=null){ city =
		 * getArguments().getString("selectedcity"); populatelistview(); }
		 */

	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		//setEmptyText("No matches found.");

	}
	private void populatelist() {
		// TODO Auto-generated method stub
	startLoading();

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Store");
		query.whereEqualTo("City", city);
		query.whereEqualTo("Status", 1);
		query.whereEqualTo("Tags", querys);
		query.orderByAscending("Name");
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> object, ParseException e) {
				if (e == null) {
					ob = object;
					for (int i = 0; i < object.size(); i++) {
						array.add(object.get(i).getString("Name"));


					}
					 stopLoading();

					Log.d("arraya", ""+array.size());
					adapter = new StoresAdapter(getActivity(), array);
				/*	ArrayAdapter<String> adapter = new ArrayAdapter<String>(
							getActivity(), android.R.layout.simple_list_item_1,
							array);*/
					//getListView().addHeaderView(header); 
					l.setAdapter(adapter);
					if (array.size() == 0) {
						l.setVisibility(View.GONE);
						nostores.setVisibility(View.VISIBLE);
					}
					
				} else {
					Log.d("score", "Error: " + e.getMessage());
				 stopLoading();


				}
				
				
				
			}
		});
		/*
		 * ParseQuery<ParseObject> lotsOfWins = ParseQuery.getQuery("Products");
		 * 
		 * lotsOfWins.whereStartsWith("Name", querys);
		 * 
		 * 
		 * ParseQuery<ParseObject> fewWins = ParseQuery.getQuery("Products");
		 * fewWins.whereStartsWith("Store", querys);
		 * 
		 * 
		 * List<ParseQuery<ParseObject>> queries = new
		 * ArrayList<ParseQuery<ParseObject>>();
		 * 
		 * queries.add(lotsOfWins); queries.add(fewWins);
		 * 
		 * 
		 * ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);
		 * mainQuery.findInBackground(new FindCallback<ParseObject>() { public
		 * void done(List<ParseObject> results, ParseException e) { // results
		 * has the list of players that win a lot or haven't won much. if (e ==


		 * null) { ob=results; for(int i=0;i<results.size();i++){
		 * array.add(results.get(i).getString("Name")); } } else {
		 * Log.d("score", "Error: " + e.getMessage()); } ArrayAdapter<String>
		 * adapter = new ArrayAdapter<String>(getActivity(),
		 * android.R.layout.simple_list_item_1,array);
		 * 
		 * setListAdapter(adapter); setEmptyText("No matching stores found.");
		 * 

		 * 


		 * 

		 * }
		 * 
		 * });
		 */
		l.setOnItemClickListener(this);

	}

	private void populatelistview() {
		// TODO Auto-generated method stub
		final Map<Float, String> hmap = new HashMap<Float, String>();
		startLoading();

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Store");
		query.whereEqualTo("Status", 1);
		query.orderByAscending("Name");
		query.whereEqualTo("City", city);
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> object, ParseException e) {
				if (e == null) {
					ob = object;
					for (int i = 0; i < object.size(); i++) {
						array.add(object.get(i).getString("Name"));
					}
					
					Log.d("arraya", ""+array.size());
					 stopLoading();

					adapter = new StoresAdapter(getActivity(), array);
					/*	ArrayAdapter<String> adapter = new ArrayAdapter<String>(
								getActivity(), android.R.layout.simple_list_item_1,
								array);*/
						//getListView().addHeaderView(header); 
						l.setAdapter(adapter);

				} else {
					Log.d("score", "Error: " + e.getMessage());
					stopLoading();

				}
				try {
				
						
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		});
		l.setOnItemClickListener(this);
	}

	
/*	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);

		// Toast.makeText(getActivity(), "" +ob.get(position).getString("Name")
		// .toString(),
		// Toast.LENGTH_LONG).show();
		// Log.d("sub",""+ob.get(position).getInt("StoreID"));
		final String storeID = ob.get(position).getObjectId();
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Products");
		query.whereEqualTo("StoreID", storeID);
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
					// ArrayList<Integer> array = new ArrayList<Integer>();
					// ob = objects;
					for (int i = 0; i < objects.size(); i++) {

						list.add(objects.get(i).getObjectId());
						// Log.d("sub",""+objects.get(i).getInt("ProductID"));

					}

				} else {
					Log.d("score", "Error: " + e.getMessage());


				}
				if(objects.size()>0){
				Log.d("abc", "" + list.size());
				mCallback.storeSelected(list, storeID);
				}else{
					
					Toast.makeText(getActivity(), "No products in this store.", Toast.LENGTH_LONG).show();
					
					
				}

			}
		});

	}*/

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		// TODO Auto-generated method stub
		final String storeID = ob.get(position).getObjectId();
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Products");
		query.whereEqualTo("StoreID", storeID);
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
					// ArrayList<Integer> array = new ArrayList<Integer>();
					// ob = objects;
					for (int i = 0; i < objects.size(); i++) {

						list.add(objects.get(i).getObjectId());
						// Log.d("sub",""+objects.get(i).getInt("ProductID"));

					}

				} else {
					Log.d("score", "Error: " + e.getMessage());


				}
				if(objects.size()>0){
				Log.d("abc", "" + list.size());
				mCallback.storeSelected(list, storeID);
				}else{
					
					Toast.makeText(getActivity(), "No products in this store.", Toast.LENGTH_LONG).show();
					
					
				}

			}
		});
	}



}