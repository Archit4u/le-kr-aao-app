package com.foofys.lekaraao.android;

import java.util.ArrayList;


import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class OrdersAdapter extends BaseAdapter {

	Context c;
	ArrayList<OrderStatus> list;

	public OrdersAdapter(Context baseContext, ArrayList<OrderStatus> list) {
		this.c = baseContext;
		this.list = list;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = LayoutInflater.from(c).inflate(R.layout.ordersrow, null);

		TextView status = (TextView) row.findViewById(R.id.Status);
		TextView orderID = (TextView) row.findViewById(R.id.OrderID);
		TextView statuschange = (TextView) row.findViewById(R.id.StatusChange);
		TextView number = (TextView) row.findViewById(R.id.Number);
		
		int items = list.get(position).getNumber();
		
		if(list.get(position).isPickup()){
		number.setText("Pick up and Drop order received at "+list.get(position).getTime());	
		}else{
		
		number.setText(items + " Item(s) ordered on "+ list.get(position).getTime());
		
		}
		int OID = list.get(position).getOrderID();
		orderID.setText("Order ID - " + OID);

		final OrderStatus temp = list.get(position);
		int stat = list.get(position).getStatus();
		if (stat == 0) {
			status.setText("Current Status - Order received.");
		} else if(stat == 1) {
			status.setText("Current Status - Order confirmed.");
		} else if(stat == 2){
			status.setText("Current Status - Order is ready to be dispatched.");
		} else if(stat ==3){
			status.setText("Current Status - Order in transit.");
			statuschange.setVisibility(View.GONE);
		}else if(stat ==4){
			status.setText("Current Status - Order delivered.");
			statuschange.setVisibility(View.GONE);
		} else if(stat == 10){
			status.setText("Current Status - Order cancelled.");
			statuschange.setVisibility(View.GONE);
		}
		
		//orderTime.setText("Placed on "+list.get(position).getTime());
		
		return row;
	}

}
