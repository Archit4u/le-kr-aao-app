package com.foofys.lekaraao.android;

import java.util.List;

import com.foofys.lekaraao.android.ProductsFragment.OnProductSelectedListener;
import com.loopj.android.image.SmartImageView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ProductDetailsFragment extends Fragment implements OnClickListener {

	OnProductAddedListener mCallback;
	String id;
	SmartImageView myImage;
	TextView desc, name, qty, store;
	// EditText qty;
	Button addtocart;

	public interface OnProductAddedListener {
		public void productAdded(String id);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (OnProductAddedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnProductAddedListener");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		id = getArguments().getString("id");
		//Log.d("abcdef", "" + id);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup root = (ViewGroup) inflater.inflate(
				R.layout.fragment_productdetails, null);
		myImage = (SmartImageView) root.findViewById(R.id.my_image);
		desc = (TextView) root.findViewById(R.id.tVdesc);
		name = (TextView) root.findViewById(R.id.tVname);
		qty = (TextView) root.findViewById(R.id.tVqty);
		store = (TextView) root.findViewById(R.id.tVstore);
		addtocart = (Button) root.findViewById(R.id.bCart);
		addtocart.setOnClickListener(this);
		return root;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Products");
		// query.whereEqualTo("Category", c);
		query.whereEqualTo("objectId", id);
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
					// ob = objects;
					for (int i = 0; i < objects.size(); i++) {
						
						myImage.setImageUrl(objects.get(i).getString("Image"));
						name.setText(objects.get(i).getString("Name"));
						// String ss = objects.get(i).getString("Description");
						// ss.substring(1, ss.length()-1);
						// Log.d("abcdef",ss.substring(1, ss.length()-1));
						desc.setText(objects.get(i).getString("Description"));
						qty.setText("Rs " + objects.get(i).getInt("Price"));
						store.setText(objects.get(i).getString("Store"));
					}
				} else {
					//Log.d("score", "Error: " + e.getMessage());
				}

			}
		});

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		// Toast.makeText(getActivity(),""+qty.getText() ,Toast.LENGTH_SHORT
		// ).show();
		
		mCallback.productAdded(id);
		getActivity().invalidateOptionsMenu();
	}

}
