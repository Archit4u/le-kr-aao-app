package com.foofys.lekaraao.android;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.json.JSONException;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;
import at.theengine.android.bestlocation.BestLocationListener;
import at.theengine.android.bestlocation.BestLocationProvider;
import at.theengine.android.bestlocation.BestLocationProvider.LocationType;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.foofys.lekaraao.android.ToFromFragment.ToFromListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.plus.Plus;
import com.parse.ParseObject;
import com.parse.ParseUser;

public class MainActivity extends FragmentActivity implements
		CategoriesFragment.OnCategorySelectedListener,
		SubcategoriesFragment.OnSubCategorySelectedListener,
		StoresFragment.OnStoreSelectedListener,
		ProductsFragment.OnProductSelectedListener,
		ProductDetailsFragment.OnProductAddedListener,
		CartFragment.OnOrderPlacedListener,
		FeaturedFragment.OnFeaturedSelected,
		MapsFragment.OnMarkerSelectedListener,ConnectionCallbacks,
		OnConnectionFailedListener,
		PickAndDropFragment.OnModeSelected, ToFromFragment.ToFromListener
		 {
	GoogleApiClient mGoogleApiClient ;
	private DrawerLayout mDrawerLayout;
	ParseObject addtocart;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	ArrayList<String> cart = new ArrayList<String>();
	private static List<AddToCart> myList = new ArrayList<AddToCart>();

	public static List<AddToCart> getMyList() {

		return myList;
	}

	public static void setMyList(List<AddToCart> myList) {
		MainActivity.myList = myList;

	}

	private static String TAG = "LekaraaoLocation";

	private static double latitude = 0;
	private static double longitude = 0;
	// private TextView mTvLog;
	private BestLocationProvider mBestLocationProvider;
	private BestLocationListener mBestLocationListener;

	private void initLocation() {
		if (mBestLocationListener == null) {
			mBestLocationListener = new BestLocationListener() {

				@Override
				public void onStatusChanged(String provider, int status,
						Bundle extras) {
					//Log.i(TAG, "onStatusChanged PROVIDER:" + provider
						//	+ " STATUS:" + String.valueOf(status));
				}

				@Override
				public void onProviderEnabled(String provider) {
					//Log.i(TAG, "onProviderEnabled PROVIDER:" + provider);
				}

				@Override
				public void onProviderDisabled(String provider) {
					//Log.i(TAG, "onProviderDisabled PROVIDER:" + provider);
				}

				@Override
				public void onLocationUpdateTimeoutExceeded(LocationType type) {
					//Log.w(TAG, "onLocationUpdateTimeoutExceeded PROVIDER:"
						//	+ type);
				}

				@Override
				public void onLocationUpdate(Location location,
						LocationType type, boolean isFresh) {
					//Log.i(TAG, "onLocationUpdate TYPE:" + type + " Location:"
					//		+ mBestLocationProvider.locationToString(location));
					// Toast.makeText(getBaseContext(),""+location.getLatitude(),
					// Toast.LENGTH_SHORT).show();
					latitude = location.getLatitude();
					longitude = location.getLongitude();
					// mTvLog.setText("\n\n" + new Date().toLocaleString() +
					// "\nLOCATION UPDATE: isFresh:" + String.valueOf(isFresh) +
					// "\n" + mBestLocationProvider.locationToString(location) +
					// mTvLog.getText());
				}
			};

			if (mBestLocationProvider == null) {
				mBestLocationProvider = new BestLocationProvider(this, true,
						true, 10000, 1000, 2, 0);
			}
		}
	}

	public static double getLatitude() {
		return latitude;
	}

	public static void setLatitude(double latitude) {
		MainActivity.latitude = latitude;
	}

	public static double getLongitude() {
		return longitude;
	}

	public static void setLongitude(double longitude) {
		MainActivity.longitude = longitude;
	}

	@Override
	protected void onResume() {
		initLocation();
		mBestLocationProvider
				.startLocationUpdatesWithListener(mBestLocationListener);
		// List<AddToCart> add = new ArrayList<AddToCart>();
		// add.add(new AddToCart("1",1));
		// setMyList(add);

		super.onResume();
	}

	@Override
	protected void onPause() {
		initLocation();
		mBestLocationProvider.stopLocationUpdates();

		super.onPause();
	}

	public static String USER_EMAIL = "";
	public static String USER_NAME = "";
	public static String USER_ID = "";

	// nav drawer title
	// private CharSequence mDrawerTitle;

	// used to store app title
	// private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private String selectedCity;
	private String[] cities2;
	SharedPreferences sharedPref;
	String saved_city = "savedcity", searchcity;
	int city;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	private SearchView searchView;

	// private ShareActionProvider mShareActionProvider;

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		 PackageInfo packageInfo = null;
		    try {
		        packageInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
		    } catch (PackageManager.NameNotFoundException e) {
		        e.printStackTrace();
		    }
		    Signature[] signatures = packageInfo.signatures;
		    byte[] cert = signatures[0].toByteArray();
		    InputStream input = new ByteArrayInputStream(cert);

		    CertificateFactory cf = null;
		    try {
		        cf = CertificateFactory.getInstance("X509");
		    } catch (CertificateException e) {
		        e.printStackTrace();
		    }
		    X509Certificate c = null;
		    try {
		        c = (X509Certificate) cf.generateCertificate(input);
		    } catch (CertificateException e) {
		        e.printStackTrace();
		    }
		    try {
		        MessageDigest md = MessageDigest.getInstance("SHA1");
		        byte[] publicKey = md.digest(c.getPublicKey().getEncoded());

		        StringBuffer hexString = new StringBuffer();
		        for (int i=0;i<publicKey.length;i++) {
		            String appendString = Integer.toHexString(0xFF & publicKey[i]);
		            if(appendString.length()==1)hexString.append("0");
		            hexString.append(appendString);
		        }
		        //Log.d("SHA1", "Cer: " + hexString.toString());

		    } catch (NoSuchAlgorithmException e1) {
		        e1.printStackTrace();
		    }
		
		
		
		
		cities2 = getResources().getStringArray(R.array.cities);
		setContentView(R.layout.activity_main);
		setupUI(findViewById(R.id.drawer_layout));
		if (FacebookLogin.facebookUser) {
			final Session session = Session.getActiveSession();
			if (session != null && session.isOpened()) {
				Request request = Request.newMeRequest(session,
						new Request.GraphUserCallback() {

							@Override
							public void onCompleted(GraphUser user,
									Response response) {
								// If the response is successful
								if (session == Session.getActiveSession()) {
									if (user != null) {
										USER_ID = user.getId();// user id
										USER_NAME = user.getName();//
										// user's
										// profile
										// name
										USER_EMAIL = (String) user
												.getProperty("email");

									}
								}

							}
						});

				Request.executeBatchAsync(request);
			}
		} else if (FacebookLogin.googleUser) {
			mGoogleApiClient = new GoogleApiClient.Builder(this)
			.addConnectionCallbacks(this)
			.addOnConnectionFailedListener(this).addApi(Plus.API)
			.addScope(Plus.SCOPE_PLUS_LOGIN).build();

			if(getIntent().getStringExtra("email")!=null){
		String email = 	getIntent().getStringExtra("email");
		USER_EMAIL = email;
			}
			if(getIntent().getStringExtra("name")!=null){
				String name = 	getIntent().getStringExtra("name");
				USER_NAME = name;
					}
			
			// String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
			// Toast.makeText(getApplicationContext(), email, 1000).show();
			//mGoogleApiClient.connect();
			
		}

		// finding the current city by current location

		sharedPref = getApplicationContext().getSharedPreferences(
				getString(R.string.preference_file_key), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		/*
		 * Criteria criteria = new Criteria(); LocationManager locationManager =
		 * (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		 * Location location = locationManager
		 * .getLastKnownLocation(locationManager.getBestProvider(criteria,
		 * true));
		 */

		city = sharedPref.getInt(saved_city, 100);
		//Log.d("def", "" + city);

		//Log.d("savedcity", "" + city);
		if (city == 100) {
			//Log.d("def1", "" + city);
			final Dialog dialog = new Dialog(this);
			dialog.getWindow();
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.citydialog);
			dialog.setCancelable(false);
			TextView title = (TextView) dialog.findViewById(R.id.storename);
			final ListView citylist = (ListView) dialog
					.findViewById(R.id.citylist);
			citylist.setAdapter(new ArrayAdapter<String>(this,
					android.R.layout.simple_list_item_single_choice,
					android.R.id.text1, cities2));
			citylist.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			citylist.setItemChecked(0, true);

			title.setText("Select City (More cities coming soon)");
			dialog.show();
			Button cancel = (Button) dialog.findViewById(R.id.bCancel);
			cancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}
			});

			Button OK = (Button) dialog.findViewById(R.id.bOK);
			OK.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					int selectedPosition = citylist.getCheckedItemPosition();
					//Log.d("SELECTEDpOSITION", "" + selectedPosition);
					String[] cities = getResources().getStringArray(
							R.array.cities);
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(
							getActionBar().getThemedContext(),
							android.R.layout.simple_spinner_item,
							android.R.id.text1, cities);

					adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

					// getActionBar().setListNavigationCallbacks(adapter,
					// null);
					getActionBar().setListNavigationCallbacks(adapter,
							new OnNavigationListener() {
								@Override
								public boolean onNavigationItemSelected(
										int itemPosition, long itemId) {
									// TODO Auto-generated
									// method stub
									selectedCity = cities2[itemPosition];
									searchcity = selectedCity;
									//Log.d("CITY01", "" + itemPosition + " "
										//	+ selectedCity);
									displayView(0);
									SharedPreferences.Editor editor = sharedPref
											.edit();

									editor.putInt(saved_city, itemPosition);
									editor.commit();
									city = sharedPref.getInt(saved_city, 100);
									//Log.d("savedcity",
										//	""+ sharedPref.getInt(saved_city, 100));
									return true;
								}
							});

					selectedCity = cities[selectedPosition];
					// Do something useful with the position of
					// the selected radio button
					//Log.d("SELECTEDcITY", "" + selectedCity);
					int position = Arrays.asList(cities).indexOf(selectedCity);
					//Log.d("positon4", "" + position);
					getActionBar().setSelectedNavigationItem(position);
					displayView(0);
					dialog.dismiss();
				}
			});
			/*
			 * new AlertDialog.Builder(this)
			 * .setTitle("Select City ").setCancelable(false)
			 * .setSingleChoiceItems(R.array.cities, 0, null)
			 * .setPositiveButton("OK", new DialogInterface.OnClickListener() {
			 * public void onClick(DialogInterface dialog, int whichButton) {
			 * dialog.dismiss(); int selectedPosition = ((AlertDialog) dialog)
			 * .getListView() .getCheckedItemPosition();
			 * Log.d("SELECTEDpOSITION", "" + selectedPosition); String[] cities
			 * = getResources() .getStringArray(R.array.cities);
			 * ArrayAdapter<String> adapter = new ArrayAdapter<String>(
			 * getActionBar().getThemedContext(),
			 * android.R.layout.simple_spinner_item, android.R.id.text1,
			 * cities);
			 * 
			 * adapter.setDropDownViewResource(android.R.layout.
			 * simple_spinner_dropdown_item);
			 * 
			 * // getActionBar().setListNavigationCallbacks(adapter, // null);
			 * getActionBar().setListNavigationCallbacks( adapter, new
			 * OnNavigationListener() {
			 * 
			 * @Override public boolean onNavigationItemSelected( int
			 * itemPosition, long itemId) { // TODO Auto-generated // method
			 * stub selectedCity = cities2[itemPosition]; searchcity =
			 * selectedCity; Log.d("CITY01", "" + itemPosition + " " +
			 * selectedCity); displayView(0); SharedPreferences.Editor editor =
			 * sharedPref.edit();
			 * 
			 * editor.putInt(saved_city, itemPosition); editor.commit(); city =
			 * sharedPref.getInt(saved_city, 100); Log.d("savedcity",
			 * ""+sharedPref.getInt(saved_city, 100)); return true; } });
			 * 
			 * selectedCity = cities[selectedPosition]; // Do something useful
			 * with the position of // the selected radio button
			 * Log.d("SELECTEDcITY", "" + selectedCity); int position =
			 * Arrays.asList(cities) .indexOf(selectedCity); Log.d("positon4",
			 * "" + position); getActionBar().setSelectedNavigationItem(
			 * position); displayView(0); } }) .setNegativeButton("Quit", new
			 * DialogInterface.OnClickListener() {
			 * 
			 * @Override public void onClick(DialogInterface dialog, int which)
			 * { // TODO Auto-generated method stub finish(); } }).show();
			 */
		} else {
			// getActionBar().setSelectedNavigationItem(city);
			getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			final String[] cities = getResources().getStringArray(
					R.array.cities);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
					getActionBar().getThemedContext(),
					android.R.layout.simple_spinner_item, android.R.id.text1,
					cities);

			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

			// getActionBar().setListNavigationCallbacks(adapter,
			// null);
			getActionBar().setListNavigationCallbacks(adapter,
					new OnNavigationListener() {
						@Override
						public boolean onNavigationItemSelected(
								int itemPosition, long itemId) {
							// TODO Auto-generated
							// method stub
							// itemPosition = city;
							selectedCity = cities2[city];
							searchcity = selectedCity;
							//Log.d("CITY01", "" + itemPosition + " "
								//	+ selectedCity);
							// displayView(city);
							SharedPreferences.Editor editor = sharedPref.edit();

							editor.putInt(saved_city, itemPosition);
							editor.commit();
							//Log.d("savedcity",
									//"" + sharedPref.getInt(saved_city, 100));
							int selectedPosition = sharedPref.getInt(
									saved_city, 100);

							selectedCity = cities[selectedPosition];
							// Do something useful with the position of
							// the selected radio button
							//Log.d("SELECTEDcITY", "" + selectedCity);
							int position = Arrays.asList(cities).indexOf(
									selectedCity);
							//Log.d("positon4", "" + position);
							city = sharedPref.getInt(saved_city, 100);
							getActionBar().setSelectedNavigationItem(position);
							displayView(0);
							return true;
						}
					});
			getActionBar().setSelectedNavigationItem(
					sharedPref.getInt(saved_city, 100));

		}

		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		// nav drawer icons from resources
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		navDrawerItems = new ArrayList<NavDrawerItem>();

		// adding nav drawer items to array
		// Home
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
				.getResourceId(0, -1)));
		// Find People
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
				.getResourceId(1, -1)));
		// Photos
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
				.getResourceId(2, -1)));
		// Communities, Will add a counter here
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
				.getResourceId(3, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
				.getResourceId(4, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
				.getResourceId(5, -1)));
		//navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons
			//	.getResourceId(6, -1)));
		// Pages
		// navDrawerItems.add(new NavDrawerItem(navMenuTitles[4],
		// navMenuIcons.getResourceId(4, -1)));
		// What's hot, We will add a counter here
		// navDrawerItems.add(new NavDrawerItem(navMenuTitles[5],
		// navMenuIcons.getResourceId(5, -1), true, "50+"));

		// Recycle the typed array
		navMenuIcons.recycle();

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		// getActionBar().setTitle("");
		getActionBar().setIcon(R.drawable.logo_hdpi);
		getActionBar().setDisplayShowTitleEnabled(false);

		getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

		final String[] dropdownValues = getResources().getStringArray(
				R.array.cities);

		/*
		 * if (location != null) { double lat, lng; lat =
		 * location.getLatitude(); lng = location.getLongitude(); Geocoder
		 * geocoder = new Geocoder(this, Locale.getDefault()); try { // Specify
		 * a SpinnerAdapter to populate the dropdown list. ArrayAdapter<String>
		 * adapter = new ArrayAdapter<String>(
		 * getActionBar().getThemedContext(),
		 * android.R.layout.simple_spinner_item, android.R.id.text1,
		 * dropdownValues);
		 * 
		 * adapter.setDropDownViewResource(android.R.layout.
		 * simple_spinner_dropdown_item);
		 * 
		 * getActionBar().setListNavigationCallbacks(adapter, new
		 * OnNavigationListener() {
		 * 
		 * @Override public boolean onNavigationItemSelected( int itemPosition,
		 * long itemId) { // TODO Auto-generated method stub selectedCity =
		 * dropdownValues[itemPosition]; Log.d("CITY0", "" + itemPosition + " "
		 * + selectedCity); displayView(0); return true; } });
		 * 
		 * List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
		 * Address address = addresses.get(0); String addresstext =
		 * address.getLocality(); // getActionBar().setTitle(addresstext);
		 * selectedCity = addresstext; String[] cities =
		 * getResources().getStringArray(R.array.cities); Log.d("CHECK", "" +
		 * Arrays.asList(cities).contains(addresstext) + addresstext);
		 * 
		 * if (Arrays.asList(cities).contains(addresstext)) { int position =
		 * Arrays.asList(cities).indexOf(addresstext); Log.d("positon1", "" +
		 * position); getActionBar().setSelectedNavigationItem(position); } else
		 * { // getActionBar().setTitle("notfound"); int position =
		 * Arrays.asList(cities).indexOf("Select city"); Log.d("positon2", "" +
		 * position); getActionBar().setSelectedNavigationItem(position); }
		 * 
		 * // Log.d("Location", ""+address.getLocality()); } catch (IOException
		 * e) { // TODO Auto-generated catch block e.printStackTrace(); } }
		 * 
		 * else { int position =
		 * Arrays.asList(R.array.cities).indexOf(selectedCity);
		 * Log.d("positon3", "" + position);
		 * getActionBar().setSelectedNavigationItem(position); }
		 */
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, // nav menu toggle icon
				R.string.drawer_open, // nav drawer open - description for
				// accessibility
				R.string.drawer_close // nav drawer close - description for
		// accessibility
		) {
			public void onDrawerClosed(View view) {
				// getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				// getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			/*
			 * (if (location != null) { displayView(0); }
			 */
		}
		handleIntent(getIntent());
	}

	@Override
	protected void onNewIntent(Intent intent) {

		handleIntent(intent);
	}

	private void handleIntent(Intent intent) {

		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			getActionBar()
					.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
			String query = intent.getStringExtra(SearchManager.QUERY);
			//Log.d("QUERY", "" + query);
			//Log.d("QUERY", "" + cities2[city]);
			Fragment fragment = new StoresFragment();
			Bundle args = new Bundle();
			args.putString("selectedcity", cities2[city]);
			args.putString("query", query.toUpperCase());
			fragment.setArguments(args);

			// getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();
			transaction.addToBackStack(null);

			transaction.replace(R.id.frame_container, fragment).commit();
			// use the query to search your data somehow
		}
	}
	protected void onStart() {
		super.onStart();
		if(FacebookLogin.googleUser){
			if(mGoogleApiClient!=null){
		mGoogleApiClient.connect();
			}
		
		}
	}

	protected void onStop() {
		super.onStop();
		if(FacebookLogin.googleUser){
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
		}
	}
	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
	}

	TextView tv;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		/**
		 * Getting the actionprovider associated with the menu item whose id is
		 * share
		 */
		// mShareActionProvider = (ShareActionProvider)
		// menu.findItem(R.id.menu_item_share).getActionProvider();

		/** Setting a share intent */
		// mShareActionProvider.setShareIntent(getDefaultShareIntent());
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		searchView = (SearchView) menu.findItem(R.id.search).getActionView();
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));

		int id = searchView.getContext().getResources()
				.getIdentifier("android:id/search_src_text", null, null);

		TextView textView = (TextView) searchView.findViewById(id);
		//textView.setTextColor(Color.WHITE);
		textView.setHintTextColor(Color.BLACK);

		searchView.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextSubmit(String query) {
				// TODO Auto-generated method stub
				searchView.setQuery("", false);
				searchView.setIconified(true);
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				// TODO Auto-generated method stub
				return false;
			}
		});

		RelativeLayout badgeLayout = (RelativeLayout) menu.findItem(
				R.id.action_cart).getActionView();
		tv = (TextView) badgeLayout.findViewById(R.id.textOne);
		if (myList.size() == 0) {
			tv.setVisibility(View.GONE);
		}

		tv.setText("" + myList.size());
		ImageView b = (ImageView) badgeLayout.findViewById(R.id.myButton);
		b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new CartFragment();

				Bundle args = new Bundle();
				args.putSerializable("cart", (Serializable) myList);
				fragment.setArguments(args);

				FragmentManager fragmentManager = getSupportFragmentManager();
				FragmentTransaction transaction = getSupportFragmentManager()
						.beginTransaction();
				transaction.addToBackStack(null);

				transaction.replace(R.id.frame_container, fragment).commit();
			}
		});

	//	menu.findItem(R.id.action_logout).setVisible(false);
		return true;
	}

	/**
	 * Returns a share intent private Intent getDefaultShareIntent(){ Intent
	 * intent = new Intent(Intent.ACTION_SEND); intent.setType("text/plain");
	 * intent.putExtra(Intent.EXTRA_SUBJECT, "SUBJECT");
	 * intent.putExtra(Intent.EXTRA_TEXT,"Extra Text"); return intent; }
	 */

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action bar actions click
		switch (item.getItemId()) {

		case R.id.action_logout: {

			if (FacebookLogin.facebookUser) {
				ParseUser.logOut();
				com.facebook.Session fbs = com.facebook.Session
						.getActiveSession();
				if (fbs == null) {
					fbs = new com.facebook.Session(this);
					com.facebook.Session.setActiveSession(fbs);
				}
				FacebookLogin.facebookUser = false;
				fbs.closeAndClearTokenInformation();
			}
			if (FacebookLogin.googleUser) {
				if (mGoogleApiClient.isConnected()) {
					Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
					mGoogleApiClient.disconnect();
					FacebookLogin.googleUser = false;
					mGoogleApiClient.connect();
				}else{
					Toast.makeText(getApplicationContext(), "no", 1000).show();
				}
			}

			Intent i = new Intent(this, FacebookLogin.class);
			startActivity(i);

			// Toast.makeText(getBaseContext(), "Succesfully logged out.",
			// Toast.LENGTH_SHORT).show();
			// MainActivity.getMyList().clear();
			// invalidateOptionsMenu();
			// getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			// Fragment fragment = new FeaturedFragment();
			// Bundle args = new Bundle();
			// args.putString("selectedcity", selectedCity);
			// // //Log.d("error", selectedCity);
			// fragment.setArguments(args);
			//
			// FragmentManager fragmentManager = getSupportFragmentManager();
			// FragmentTransaction transaction = getSupportFragmentManager()
			// .beginTransaction();
			// transaction.addToBackStack(null);
			//
			// transaction.replace(R.id.frame_container, fragment).commit();

		}
			return true;

		case R.id.action_orders: {
			// ParseUser.logOut();

			Fragment fragment = new MyOrdersFragment();

			FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();
			transaction.addToBackStack(null);

			transaction.replace(R.id.frame_container, fragment).commit();

		}
			return true;

		case R.id.action_login: {
			Fragment fragment = new LoginFragment();
			Bundle args = new Bundle();
			args.putString("selectedcity", selectedCity);
			// Log.d("error", selectedCity);
			fragment.setArguments(args);
			FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();
			transaction.addToBackStack(null);

			transaction.replace(R.id.frame_container, fragment).commit();

		}
			return true;

		case R.id.action_contact: {
			Fragment fragment = new ContactFragment();

			FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();
			transaction.addToBackStack(null);

			transaction.replace(R.id.frame_container, fragment).commit();

		}
			return true;

		case R.id.action_cart: {
			Fragment fragment = new CartFragment();
			Bundle args = new Bundle();
			args.putSerializable("cart", (Serializable) myList);
			fragment.setArguments(args);
			FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();
			transaction.addToBackStack(null);

			transaction.replace(R.id.frame_container, fragment).commit();
			;
			// setTitle("Cart");
		}
		default:
			return super.onOptionsItemSelected(item);
		}
	}


	public void setupUI(View view) {

		// Set up touch listener for non-text box views to hide keyboard.
		if (!(view instanceof EditText)) {

			view.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					hideSoftKeyboard(MainActivity.this);

					return false;
				}

			});
		}

		// If a layout container, iterate over children and seed recursion.
		if (view instanceof ViewGroup) {

			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

				View innerView = ((ViewGroup) view).getChildAt(i);

				setupUI(innerView);
			}
		}
	}

	public static void hideSoftKeyboard(Activity activity) {
		InputMethodManager inputMethodManager = (InputMethodManager) activity
				.getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus()
				.getWindowToken(), 0);
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		ParseUser currentUser = ParseUser.getCurrentUser();
		//if (currentUser != null) {
			// do stuff with the user

		//	Log.d("Currentuser", "" + currentUser.getBoolean("PasswordEnabled"));

			menu.findItem(R.id.action_login).setVisible(false);
			menu.findItem(R.id.action_logout).setVisible(true);
			menu.findItem(R.id.action_orders).setVisible(true);

			// Boolean bool = currentUser.getBoolean("PasswordEnabled");
			// if (bool) {
			// menu.findItem(R.id.action_login).setVisible(false);
			// menu.findItem(R.id.action_logout).setVisible(true);
			// menu.findItem(R.id.action_orders).setVisible(true);
			// } else {
			// menu.findItem(R.id.action_login).setVisible(true);
			// menu.findItem(R.id.action_logout).setVisible(false);
			// menu.findItem(R.id.action_orders).setVisible(true);
			// }

//		} else {
//			Toast.makeText(getApplicationContext(), "in", 1000).show();
//			//Log.d("Currentuser", "current user is null");
//			menu.findItem(R.id.action_login).setVisible(true);
//			menu.findItem(R.id.action_logout).setVisible(false);
//			menu.findItem(R.id.action_orders).setVisible(false);
//			// show the signup or login screen
//		}
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		// menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	private void displayView(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		switch (position) {
		case 0:
			getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			// getActionBar().setTitle("Featured");
			// getActionBar().setDisplayShowTitleEnabled(true);

			fragment = new PickAndDropFragment();
			Bundle args = new Bundle();
			args.putString("selectedcity", selectedCity);
			//Log.d("error", selectedCity);

			fragment.setArguments(args);
			break;

		case 1:
			getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			// getActionBar().setTitle("Featured");
			// getActionBar().setDisplayShowTitleEnabled(true);
			fragment = new FeaturedFragment();
			Bundle args4 = new Bundle();
			args4.putString("selectedcity", selectedCity);
			//Log.d("error", selectedCity);
			fragment.setArguments(args4);
			break;
		case 2:
			// if (getSupportFragmentManager().getBackStackEntryCount() != 0)
			getActionBar()
					.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
			// getActionBar().setTitle("Categories");
			// getActionBar().setDisplayShowTitleEnabled(true);
			fragment = new CategoriesFragment();
			Bundle args1 = new Bundle();
			args1.putString("selectedcity", selectedCity);
			fragment.setArguments(args1);
			getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			// }
			break;
		case 3:
			getActionBar()
					.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
			// getActionBar().setTitle("Stores");
			// getActionBar().setDisplayShowTitleEnabled(true);
			fragment = new StoresFragment();
			Bundle args2 = new Bundle();
			args2.putString("selectedcity", selectedCity);
			fragment.setArguments(args2);
			getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			break;

		case 4:
			getActionBar()
					.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
			// getActionBar().setTitle("Map");
			// getActionBar().setDisplayShowTitleEnabled(true);
			fragment = new MapsFragment();
			Bundle args3 = new Bundle();
			args3.putString("selectedcity", selectedCity);
			fragment.setArguments(args3);
			getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			break;

		case 5:
			getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			if (MainActivity.getMyList().size() > 0) {
				final Dialog dialog = new Dialog(this);
				dialog.getWindow();
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

				dialog.setContentView(R.layout.customdialog);

				TextView title = (TextView) dialog.findViewById(R.id.storename);
				title.setText("Discard the items in your cart?");
				dialog.show();

				Button cancel = (Button) dialog.findViewById(R.id.bCancel);
				cancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						mDrawerLayout.closeDrawer(mDrawerList);
						dialog.dismiss();
					}
				});

				Button OK = (Button) dialog.findViewById(R.id.bOK);
				OK.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						MainActivity.getMyList().clear();
						modeSelected("pick");
						mDrawerLayout.closeDrawer(mDrawerList);
						dialog.dismiss();
					}
				});
			} else {
				fragment = new ToFromFragment();
				Bundle args6 = new Bundle();
				args6.putString("selectedcity", selectedCity);
				//Log.d("error", selectedCity);

				fragment.setArguments(args6);
			}
			break;

		case 6:
			getActionBar()
					.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
			// getActionBar().setTitle("Map");
			// getActionBar().setDisplayShowTitleEnabled(true);
			fragment = new CustomOrderFragment();
			Bundle args5 = new Bundle();
			args5.putString("selectedcity", selectedCity);
			fragment.setArguments(args5);
			getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			break;

		default:
			break;
		}

		if (fragment != null) {

			FragmentManager fragmentManager = getSupportFragmentManager();

			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();
			if (position != 0) {
				transaction.addToBackStack(null);
			}
			transaction.replace(R.id.frame_container, fragment).commit();

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			// setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			// error in creating fragment
			//Log.e("MainActivity", "Error in creating fragment");
		}
	}

	/*
	 * @Override public void setTitle(CharSequence title) { mTitle = title;
	 * getActionBar().setTitle(mTitle); }
	 */

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	// public void onBackPressed() {
	// FragmentManager manager =getSupportFragmentManager();
	// if (manager.getBackStackEntryCount() > 1 ) {
	// // If there are back-stack entries, leave the FragmentActivity
	// // implementation take care of them.
	// manager.popBackStack();
	// }
	// } else {
	// // Otherwise, ask user if he wants to leave :)
	// new AlertDialog.Builder(this)
	// .setTitle("Really Exit?")
	// .setMessage("Are you sure you want to exit?")
	// .setNegativeButton(android.R.string.no, null)
	// .setPositiveButton(android.R.string.yes, new
	// DialogInterface.OnClickListener() {
	//
	// public void onClick(DialogInterface arg0, int arg1) {
	// MainActivity.super.onBackPressed();
	// }
	// }).create().show();
	// }
	// }

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	boolean check(List<AddToCart> myList, String id) {
		for (int j = 0; j < myList.size(); j++)
			if (id == myList.get(j).getProductId())
				return true;
		return false;
	};

	// Interface for categories fragment
	@Override
	public void categorySelected(String category, String scity) {
		// TODO Auto-generated method stub
		// Log.d("interface", ""+category);
		Fragment fragment = new SubcategoriesFragment();
		Bundle args = new Bundle();
		args.putString("category", category);
		args.putString("scity", scity);
		fragment.setArguments(args);
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.addToBackStack(null);

		transaction.replace(R.id.frame_container, fragment).commit();
		// setTitle(category);
	}

	// Interface for subcategories fragment
	@Override
	public void subCategorySelected(ArrayList<String> list, String StoreID) {
		// TODO Auto-generated method stub

		//Log.d("interface", "" + list.size());
		ArrayList<String> temp = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++)
			temp.add(list.get(i));
		//Log.d("interface", "" + temp.size());
		Fragment fragment = new ProductsFragment();
		Bundle args = new Bundle();
		args.putStringArrayList("list", temp);
		args.putString("StoreID", StoreID);
		// args.putString("category", category);
		SubcategoriesFragment.getList().clear();
		// Log.d("interface temp", ""+temp.size());
		fragment.setArguments(args);
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.addToBackStack(null);

		transaction.replace(R.id.frame_container, fragment, "Product List")
				.commit();
		// setTitle(category);

	}

	// Interface for stores fragment
	@Override
	public void storeSelected(ArrayList<String> list, String StoreID) {
		// TODO Auto-generated method stub
		ArrayList<String> temp = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++)
			temp.add(list.get(i));
		Fragment fragment = new ProductsFragment();
		Bundle args = new Bundle();
		args.putStringArrayList("list", temp);
		args.putString("StoreID", StoreID);
		StoresFragment.getList().clear();
		// args.putString("category", category);
		fragment.setArguments(args);
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.addToBackStack(null);

		transaction.replace(R.id.frame_container, fragment, "Product List")
				.commit();
		// setTitle(category);

	}

	// Interface for Maps Fragment
	@Override
	public void markerSelected(String StoreID) {
		// TODO Auto-generated method stub
		Fragment fragment = new ProductsFragment();
		Bundle args = new Bundle();

		args.putString("StoreID", StoreID);
		StoresFragment.getList().clear();
		// args.putString("category", category);
		fragment.setArguments(args);
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.addToBackStack(null);

		transaction.replace(R.id.frame_container, fragment, "Product List")
				.commit();
		// setTitle(category);

	}

	// Featured store selected
	public void featuredSelected(ArrayList<String> list, String StoreID) {

		ArrayList<String> temp = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++)
			temp.add(list.get(i));

		//Log.d("MainActivityList", "" + temp);
		Fragment fragment = new ProductsFragment();
		Bundle args = new Bundle();
		args.putStringArrayList("list", temp);
		args.putString("StoreID", StoreID);
		FeaturedFragment.getList().clear();
		// args.putString("category", category);
		fragment.setArguments(args);
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.addToBackStack(null);

		transaction.replace(R.id.frame_container, fragment, "Product List")
				.commit();
		// TODO Auto-generated method stub
		/*
		 * Fragment fragment = new StoresFragment(); Bundle args = new Bundle();
		 * args.putInt("id", id); fragment.setArguments(args); FragmentManager
		 * fragmentManager = getSupportFragmentManager(); FragmentTransaction
		 * transaction = getSupportFragmentManager().beginTransaction();
		 * transaction.addToBackStack(null);
		 * transaction.replace(R.id.frame_container, fragment).commit();
		 */

	}

	// Interface for products fragment
	@Override
	public void productSelected(int id) {
		// TODO Auto-generated method stub
		Fragment fragment = new ProductDetailsFragment();
		Bundle args = new Bundle();
		args.putInt("id", id);
		fragment.setArguments(args);
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.addToBackStack(null);
		transaction.replace(R.id.frame_container, fragment).commit();

	}

	// Interface for product details fragment(add to cart)
	@Override
	public void productAdded(String id) {

		// Log.d("abcdef", "MainActi");
		if (myList.size() == 0) {
			AddToCart objAddToCart = new AddToCart(id, 1);
			myList.add(objAddToCart);
			// for(int i=0;i<myList.size();i++)
			// Log.d("whatte", ""+myList.get(i).getProductId()+"0");

			Fragment fragment = new CartFragment();
			Bundle args = new Bundle();
			args.putSerializable("cart", (Serializable) myList);
			fragment.setArguments(args);
			FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();
			transaction.addToBackStack(null);

			transaction.replace(R.id.frame_container, fragment).commit();
			;
			// setTitle("Cart");
		}

		else if (check(myList, id)) {
			{
				Toast.makeText(getBaseContext(), "Item already in cart",
						Toast.LENGTH_SHORT).show();
				Fragment fragment = new CartFragment();
				Bundle args = new Bundle();
				args.putSerializable("cart", (Serializable) myList);
				// for(int k = 0;k<myList.size();k++)
				// Log.d("whatte",
				// ""+myList.get(k).getProductId()+"    "+myList.get(k).getQty());
				fragment.setArguments(args);
				FragmentManager fragmentManager = getSupportFragmentManager();
				FragmentTransaction transaction = getSupportFragmentManager()
						.beginTransaction();
				transaction.addToBackStack(null);

				transaction.replace(R.id.frame_container, fragment).commit();
				;
				// setTitle("Cart");

			}
		} else {
			// Log.d("fail", "fails");
			Collections.sort(myList);
			AddToCart objAddToCart = new AddToCart(id, 1);
			myList.add(objAddToCart);

			Fragment fragment = new CartFragment();
			Bundle args = new Bundle();
			args.putSerializable("cart", (Serializable) myList);
			fragment.setArguments(args);
			FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();
			transaction.addToBackStack(null);

			transaction.replace(R.id.frame_container, fragment).commit();
			;
		}

	}

	@Override
	public void orderPlaced() {
		// TODO Auto-generated method stub
		myList.clear();
		getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		Fragment fragment = new FeaturedFragment();
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.addToBackStack(null);

		transaction.replace(R.id.frame_container, fragment).commit();
		;
	}

	@Override
	public void modeSelected(String mode) {
		if (mode.equals("shop")) {
			Fragment fragment = new CategoriesFragment();
			Bundle args4 = new Bundle();
			args4.putString("selectedcity", selectedCity);
			fragment.setArguments(args4);
			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();
			transaction.addToBackStack(null);

			transaction.replace(R.id.frame_container, fragment).commit();

		} else {
			Fragment fragment = new ToFromFragment();
			Bundle args4 = new Bundle();
			args4.putString("selectedcity", selectedCity);
			fragment.setArguments(args4);
			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();
			transaction.setCustomAnimations(android.R.anim.slide_in_left,
					android.R.anim.slide_out_right);
			transaction.addToBackStack(null);

			transaction.replace(R.id.frame_container, fragment).commit();
		}
	}

	@Override
	public void toFromSelected(LatLng to, LatLng from) {
		// TODO Auto-generated method stub
		Fragment fragment = new RouteFragment();
		Bundle args = new Bundle();
		args.putParcelable("to", to);
		args.putParcelable("from", from);
		fragment.setArguments(args);
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.addToBackStack(null);

		transaction.replace(R.id.frame_container, fragment).commit();
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		
	}

	
}
