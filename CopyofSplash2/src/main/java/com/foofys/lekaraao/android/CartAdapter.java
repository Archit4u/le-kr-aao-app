package com.foofys.lekaraao.android;

import java.util.ArrayList;
import java.util.List;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class CartAdapter extends BaseAdapter {
	Context c;
	ArrayList<CartRow> list;
	TextView qty;
	List<AddToCart> myList = new ArrayList<AddToCart>();
	int totalprice;
	ImageView plus, minus, delete;
	 ListView l;
	 static int amount=0;

	public CartAdapter(Context baseContext, ArrayList<CartRow> list, ListView l) {
		this.c = baseContext;
		this.list = list;
		this.l = l;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub

		final int quantity = list.get(position).getQty();
		View row = LayoutInflater.from(c).inflate(R.layout.cartrow, null);
		//View footer = LayoutInflater.from(c).inflate(R.layout.cartfooter, null);
		//TextView test = (TextView) footer.findViewById(R.id.textView1);
		TextView name = (TextView) row.findViewById(R.id.tVName);
		TextView store = (TextView) row.findViewById(R.id.tVStore);
		TextView price = (TextView) row.findViewById(R.id.tVPrice);
		final TextView total = (TextView) row.findViewById(R.id.tVTotal);
		qty = (TextView) row.findViewById(R.id.tVQty);
		// SmartImageView myImage = (SmartImageView)
		// row.findViewById(R.id.my_image);
		plus = (ImageView) row.findViewById(R.id.bPlus);
		minus = (ImageView) row.findViewById(R.id.bMinus);
		delete = (ImageView) row.findViewById(R.id.bDelete);
		Typeface face = Typeface.createFromAsset(c.getAssets(), "Rupee_Foradian.ttf");
		total.setTypeface(face);
		final CartRow temp = list.get(position);
		 //Log.d("imagetesting", ""+temp.getName()+""+temp.getQty());
		// myImage.setImageUrl(temp.getUrl());
		store.setText(temp.getStore());
		name.setText(temp.getName());
		price.setText(temp.getPrice() + " X");
		qty.setText("" + temp.getQty() +" "+temp.getUnit());
		totalprice = Integer.parseInt(temp.getPrice()) * temp.getQty();
		total.setText("= " +c.getResources().getString(R.string.rs)+ ""+totalprice);
		amount = amount +totalprice;
		plus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				myList = MainActivity.getMyList();
				temp.setQty(temp.getQty() + 1);
				for (int i = 0; i < myList.size(); i++) {
					if (myList.get(i).getProductId() == list.get(position)
							.getId())
						myList.get(i).setQty(list.get(position).getQty());
									}
				notifyDataSetChanged();
			}
		});

		minus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				myList = MainActivity.getMyList();
				if (temp.getQty() > 1) {
					temp.setQty(temp.getQty() - 1);
					for (int i = 0; i < myList.size(); i++) {
						if (myList.get(i).getProductId() == list.get(position)
								.getId())
							myList.get(i).setQty(list.get(position).getQty());
					}
					notifyDataSetChanged();
				}

			}
		});

		delete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				  
				myList = MainActivity.getMyList();
				for (int i = 0; i < myList.size(); i++) {
				//	Log.d("cartadapter", " "+i+"   list"+list.size());
					if (myList.get(i).getProductId().equals(list.get(position)
							.getId()) ){
				//		Log.d("12222222222321", "dsadasdsa");

						myList.remove(i);
						list.remove(position);
						break;
					}
					// Log.d("pricelsada",
					// ""+list.get(position).getId()+"pos"+position);

				}
				Activity m = (Activity) c;
				m.invalidateOptionsMenu();
				notifyDataSetChanged();
				

			}

		});
		// qty.addTextChangedListener(watcher);

		/*
		 * temp.setQty(Integer.parseInt(qty1.getText().toString()));
		 * Log.d("plsra", ""+temp.getQty()); ParseObject addtocart = new
		 * ParseObject("Cart"); addtocart.put("Quantity", temp.getQty());
		 * addtocart.saveInBackground();
		 */
		//test.setText(""+amount);
	//	l.addFooterView(footer);
		return row;
	}

}
