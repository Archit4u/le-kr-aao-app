package com.foofys.lekaraao.android;

import java.util.ArrayList;

public class SubcategorySinglerow {
  
 private String name;
 private ArrayList<SingleRow> productList = new ArrayList<SingleRow>();;
  
 public String getName() {
  return name;
 }
 public void setName(String name) {
  this.name = name;
 }
 public ArrayList<SingleRow> getProductList() {
	 
  return productList;
 }
 public void setProductList(ArrayList<SingleRow> productList) {
  this.productList = productList;
 }
 
}