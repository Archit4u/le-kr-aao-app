package com.foofys.lekaraao.android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.foofys.lekaraao.android.StoresFragment.OnStoreSelectedListener;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.image.SmartImageView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class MapsFragment extends SupportMapFragment {

	ViewGroup root;
	// Google Map
	private GoogleMap googleMap;
	ParseGeoPoint storeLocation;
	String category, city;
	HashMap <Marker, String> hashMap = new HashMap <Marker, String>();
	MapView mapView;
    private Bundle bundle;
    ArrayList<String> categories = new ArrayList<String>();

	OnMarkerSelectedListener mCallback;
	
	
	
	public interface OnMarkerSelectedListener {
        public void markerSelected( String string);
    }
	
	 @Override
	    public void onAttach(Activity activity) {
	        super.onAttach(activity);
	        
	        // This makes sure that the container activity has implemented
	        // the callback interface. If not, it throws an exception
	        try {
	            mCallback = (OnMarkerSelectedListener) activity;
	        } catch (ClassCastException e) {
	            throw new ClassCastException(activity.toString()
	                    + " must implement OnStoreSelectedListener");
	        }
	    }
	
	  @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		  
	        //root = (ViewGroup) inflater.inflate(R.layout.mapfragment, null);
		  super.onCreateView(inflater, container, savedInstanceState);
	        root = (ViewGroup) inflater.inflate(R.layout.mapfragment, container, false);
	       MapsInitializer.initialize(getActivity());
	        
			try {
				// Loading map
				initializeMap();

			} catch (Exception e) {
				e.printStackTrace();
			}
			
			mapView = (MapView) root.findViewById(R.id.map);
		       mapView.onCreate(bundle);
		  //     if (googleMap == null) {
		    //	   googleMap = ((MapView) root.findViewById(R.id.map)).getMap();
		      // }
		     //  googleMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Here!"));

	        return root;
	    }
	   

	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        bundle = savedInstanceState;
	    }
	    
	    
	    
	    @Override
	    public void onResume() {
	       super.onResume();
	       mapView.onResume();
	       city = getArguments().getString("selectedcity");
	       initializeMap();
	    }
	    
	    private void initializeMap() {
	        if (googleMap == null) {
	            googleMap = ((MapView) root.findViewById(R.id.map)).getMap();
	            
	    		
	    		ParseQuery<ParseObject> query = ParseQuery.getQuery("Store");
	    		query.whereEqualTo("Status", 1);
	    		query.whereEqualTo("City", city);
	    		query.findInBackground(new FindCallback<ParseObject>() {

	    			@Override
	    			public void done(List<ParseObject> loc, ParseException e) {
	    				// TODO Auto-generated method stub
	    				if (e == null) {
	    					for(int i =0; i<loc.size();i++){
	    						storeLocation = loc.get(i).getParseGeoPoint("LatLong");
	    						categories = (ArrayList<String>) loc.get(i).get("Category");
	    						String cat = categories.get(0);
	    						
	    						//Log.d("category", cat);
	    						if(cat.equals("Food")){
	    							cat = "food";
	    						}else if(cat.equals("Office Supplies")){
	    							cat = "stationery";
	    						}else if(cat.equals("Groceries")){
	    							cat = "groceries"; 
	    						}else if(cat.equals("Medical Supplies")){
	    							cat = "medicines";
	    						}else if(cat.equals("Florists")){
	    							cat = "florist";
	    						}
	    						
	    						//Log.d("category", cat);
	    				//		Log.d("loc", ""+userLocation.getLatitude()+ "---"+ userLocation.getLongitude());
	    						double latitude = storeLocation.getLatitude() ;
	    			            double longitude = storeLocation.getLongitude();
	    			             String pid = loc.get(i).getObjectId();
	    			            // create marker
	    			             try {    MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title(""+loc.get(i).getString("Name"));
	    			         // Changing marker icon
	    			            int resID = getResources().getIdentifier(cat , "drawable", getActivity().getPackageName());
	    			            if(resID==0){
	    			            	resID = getResources().getIdentifier("mapdefault" , "drawable", getActivity().getPackageName());
	    			            }
	    			            //Log.d("categoryresid", ""+resID);
	    			            marker.icon(BitmapDescriptorFactory.fromResource(resID));
	    			            //marker.put
	    			            // adding marker
	    			            
	    			           
								Marker m =   googleMap.addMarker(marker);
								   hashMap.put(m, pid);
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
	    					}
	    		            
	    		        } else {
	    		            //Log.d("score", "Error: " + e.getMessage());
	    		        }
	    			} 
	    		});
	            
	            googleMap.setMyLocationEnabled(true);
	        /////----------------------------------Zooming camera to position user-----------------

	            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
	            
	                        Criteria criteria = new Criteria();

	                        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
	                        if (location != null)
	                        {
	                        	googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
	                                    new LatLng(location.getLatitude(), location.getLongitude()), 13));

	                            CameraPosition cameraPosition = new CameraPosition.Builder()
	                            .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
	                            .zoom(12)                   // Sets the zoom
	                            .bearing(90)                // Sets the orientation of the camera to east
	                            .tilt(40)                   // Sets the tilt of the camera to 30 degrees
	                            .build();                   // Creates a CameraPosition from the builder
	                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

	                        }else{
	                        	CameraPosition cameraPosition = new CameraPosition.Builder()
	                            .target(new LatLng(28.5700, 77.3200))      // Sets the center of the map to location user
	                            .zoom(12)                   // Sets the zoom
	                            .bearing(90)                // Sets the orientation of the camera to east
	                            .tilt(40)                   // Sets the tilt of the camera to 30 degrees
	                            .build();                   // Creates a CameraPosition from the builder
	                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
	                        }

	    /////----------------------------------Zooming camera to position user-----------------
	            // check if map is created successfully or not
	            if (googleMap == null) {
	                Toast.makeText(getActivity(),
	                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
	                        .show();
	            }
	        }
	        googleMap.setOnMarkerClickListener(new OnMarkerClickListener() {
				
				@Override
				public boolean onMarkerClick(final Marker marker) {
					// TODO Auto-generated method stub
			
					
					return false;
				}
			});
	        
	        googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
				
				@Override
				public void onInfoWindowClick(final Marker arg0) {
					// TODO Auto-generated method stub
					final Dialog dialog = new Dialog(getActivity());
					dialog.getWindow();
				    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);  
					
					dialog.setContentView(R.layout.customdialog);
					
					TextView title = (TextView) dialog.findViewById(R.id.storename);
					title.setText("Go to "+arg0.getTitle()+"?");
					dialog.show();
				/*	
					   AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
			            builder1.setMessage("Go to "+arg0.getTitle()+"?")
			                   .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			                       public void onClick(DialogInterface dialog, int id) {           
			                    	   mCallback.markerSelected(hashMap.get(arg0));
			                       } })
			                       .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			                           public void onClick(DialogInterface dialog, int id) {
			                               // User cancelled the dialog
			                           }
			                   });                         
			           builder1.create();
			           builder1.show();
			           */
					Button cancel = (Button) dialog.findViewById(R.id.bCancel);
					cancel.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dialog.dismiss();
						}
					});
					
					Button OK = (Button) dialog.findViewById(R.id.bOK);
					OK.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							 mCallback.markerSelected(hashMap.get(arg0));
							 dialog.dismiss();
						}
					});
				}
			});
	    }


	    @Override
	    public void onPause() {
	       super.onPause();
	       mapView.onPause();
	       if(googleMap != null) {   
	    	    googleMap = null;
	       }
	    }

	    @Override
	    public void onDestroy() {
	       mapView.onDestroy();
	       super.onDestroy();
	    }

	    @Override  
	    public void onLowMemory() {  
	        super.onLowMemory();  
	        mapView.onLowMemory();  
	    }  
}
	/**
	 * function to load map. If map is not created it will create it for you
	 * *
	private void initilizeMap() {
        if (mapView == null) {
           // = ((SupportMapFragment) getFragmentManager().findFragmentById(
            //        R.id.map)).getMap();
            
        	googleMap = mapView.getMap(); 
    		ParseQuery<ParseObject> query = ParseQuery.getQuery("Store");
    		
    		query.findInBackground(new FindCallback<ParseObject>() {

    			@Override
    			public void done(List<ParseObject> loc, ParseException e) {
    				// TODO Auto-generated method stub
    				if (e == null) {
    					for(int i =0; i<loc.size();i++){
    						storeLocation = loc.get(i).getParseGeoPoint("Location");	
    				//		Log.d("loc", ""+userLocation.getLatitude()+ "---"+ userLocation.getLongitude());
    						double latitude = storeLocation.getLatitude() ;
    			            double longitude = storeLocation.getLongitude();
    			             Integer pid = loc.get(i).getInt("StoreID");
    			            // create marker
    			            MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title(""+loc.get(i).getString("Username"));
    			         // Changing marker icon
    			            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.logo_hdpi));
    			            
    			            // adding marker
    			            
    			           Marker m =   googleMap.addMarker(marker);
    			            hashMap.put(m, pid);
    					}
    		            
    		        } else {
    		            Log.d("score", "Error: " + e.getMessage());
    		        }
    			} 
    		});
            
            googleMap.setMyLocationEnabled(true);
        /////----------------------------------Zooming camera to position user-----------------

            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            
                        Criteria criteria = new Criteria();

                        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
                        if (location != null)
                        {
                        	googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(location.getLatitude(), location.getLongitude()), 13));

                            CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                            .zoom(17)                   // Sets the zoom
                            .bearing(90)                // Sets the orientation of the camera to east
                            .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                            .build();                   // Creates a CameraPosition from the builder
                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                        }else{
                        	CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(13, 77))      // Sets the center of the map to location user
                            .zoom(17)                   // Sets the zoom
                            .bearing(90)                // Sets the orientation of the camera to east
                            .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                            .build();                   // Creates a CameraPosition from the builder
                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }

    /////----------------------------------Zooming camera to position user-----------------
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getActivity(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
        googleMap.setOnMarkerClickListener(new OnMarkerClickListener() {
			
			@Override
			public boolean onMarkerClick(Marker marker) {
				// TODO Auto-generated method stub
				   AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
		            builder1.setMessage(""+hashMap.get(marker))
		                   .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		                       public void onClick(DialogInterface dialog, int id) {                                   
		                       }
		                   });                         
		           builder1.create();
		           builder1.show();
				//Toast.makeText(getBaseContext(),marker.getTitle() , Toast.LENGTH_SHORT).show();
				return false;
			}
		});
    }
	
	@Override
	public void onResume() {
	mapView.onResume();
	super.onResume();
	initilizeMap();
	}
	@Override
	public void onDestroy() {
	super.onDestroy();
	mapView.onDestroy();
	}
	@Override
	public void onLowMemory() {
	super.onLowMemory();
	mapView.onLowMemory();
	}




}*/
