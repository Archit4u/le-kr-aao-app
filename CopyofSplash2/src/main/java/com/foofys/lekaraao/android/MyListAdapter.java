package com.foofys.lekaraao.android;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
 
public class MyListAdapter extends BaseExpandableListAdapter {
 
 private Context context;
 private ArrayList<SubcategorySinglerow> categoryList;
  
 public MyListAdapter(Context context, ArrayList<SubcategorySinglerow> deptList) {
  this.context = context;
  this.categoryList = deptList;
 }
  
 @Override
 public Object getChild(int groupPosition, int childPosition) {
  ArrayList<SingleRow> productList = categoryList.get(groupPosition).getProductList();
  return productList.get(childPosition);
 }
 
 @Override
 public long getChildId(int groupPosition, int childPosition) {
  return childPosition;
 }
 
 @Override
 public View getChildView(int groupPosition, int childPosition, boolean isLastChild, 
   View view, ViewGroup parent) {
   
  final SingleRow temp = (SingleRow) getChild(groupPosition, childPosition);
  if (view == null) {
   LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
   view = infalInflater.inflate(R.layout.fragment_productdetails, null);
  }
   
  /*TextView sequence = (TextView) view.findViewById(R.id.sequence);
  sequence.setText(singleRow.getSequence().trim() + ") ");
  TextView childItem = (TextView) view.findViewById(R.id.childItem);
  childItem.setText(singleRow.getName().trim());
   */
	TextView name = (TextView) view.findViewById(R.id.textView1);
	TextView qty = (TextView) view.findViewById(R.id.qty);
//	final TextView incart = (TextView) view.findViewById(R.id.incart);
	//incart.setVisibility(View.GONE);
	TextView price = (TextView) view.findViewById(R.id.textView3);
	final ImageView add = (ImageView) view.findViewById(R.id.bAdd);
	final ImageView plus = (ImageView) view.findViewById(R.id.bPlus);
	final ImageView minus = (ImageView) view.findViewById(R.id.bMinus);
	//final SingleRow temp = list.get(childPosition);

	qty.setText("" + temp.getQty());
	//Log.d("abcimage", "" + temp.getProductId());
	name.setText(temp.getName());
	price.setText(temp.getPrice());

	for (int i = 0; i < MainActivity.getMyList().size(); i++) {
		//Log.d("pricelist", ""
			//	+ MainActivity.getMyList().get(i).getProductId());
		if (temp.getProductId() == MainActivity.getMyList().get(i)
				.getProductId()) {
			add.setEnabled(false);
			plus.setEnabled(false);
			minus.setEnabled(false);
			minus.setVisibility(View.GONE);
			plus.setVisibility(View.GONE);
			qty.setVisibility(View.GONE);
			
		}
	}

	if (add.isEnabled()) {
		add.setBackgroundResource(R.drawable.cart);
	} else {
		add.setBackgroundResource(R.drawable.cartdisabled);
	}
	add.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Toast.makeText(context, "Item Added to Cart!", Toast.LENGTH_SHORT)
					.show();
			AddToCart obj = new AddToCart(temp.getProductId(), temp
					.getQty());
			MainActivity.getMyList().add(obj);
		//	incart.setText("" + temp.getQty() + "nos. in cart");
			add.setEnabled(false);
			add.setBackgroundResource(R.drawable.cartdisabled);
			add.setTag(temp.getProductId());
			//incart.setTag(temp.getQty());
			//Log.d("price", add.getTag().toString());
			Activity m = (Activity) context;
			m.invalidateOptionsMenu();
			// ActionBarRefresh.refreshActionBarMenu(c);

			notifyDataSetChanged();
		}
	});
	plus.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			temp.setQty(temp.getQty() + 1);
			notifyDataSetChanged();
		}
	});

	minus.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (temp.getQty() > 1) {
				temp.setQty(temp.getQty() - 1);
				notifyDataSetChanged();
			}
		}
	});
	//if (add.getTag() != null)
		//Log.d("pricedsaa", add.getTag().toString());
	return view;
}
  
 
 @Override
 public int getChildrenCount(int groupPosition) {
   
  ArrayList<SingleRow> productList = categoryList.get(groupPosition).getProductList();
  return productList.size();
 
 }
 
 @Override
 public Object getGroup(int groupPosition) {
  return categoryList.get(groupPosition);
 }
 
 @Override
 public int getGroupCount() {
  return categoryList.size();
 }
 
 @Override
 public long getGroupId(int groupPosition) {
  return groupPosition;
 }
 
 @Override
 public View getGroupView(int groupPosition, boolean isLastChild, View view,
   ViewGroup parent) {
   
	 SubcategorySinglerow headerInfo = (SubcategorySinglerow) getGroup(groupPosition);
  if (view == null) {
   LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
   view = inf.inflate(R.layout.grouprow, null);
  }
   
  TextView heading = (TextView) view.findViewById(R.id.heading);
  heading.setText(headerInfo.getName().trim());
   
  return view;
 }
 
 @Override
 public boolean hasStableIds() {
  return true;
 }
 
 @Override
 public boolean isChildSelectable(int groupPosition, int childPosition) {
  return true;
 }
  
  
 
}
