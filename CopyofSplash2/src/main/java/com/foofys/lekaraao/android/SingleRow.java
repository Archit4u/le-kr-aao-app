package com.foofys.lekaraao.android;

public class SingleRow {

	String name, description,price, store, image, unit;
	String ProductId;
	int qty;
	public SingleRow(String name, String description, String image, int price, String store, String string, int qty, String unit) {
		
		// TODO Auto-generated constructor stub
		this.name = name;
		this.description = description;
		this.image = image;
		this.price = ""+price;
		this.store = store;
		this.ProductId = string;
		this.qty=qty;
		this.unit = unit;
		
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public String getProductId() {
		return ProductId;
	}
	public void setProductId(String productId) {
		ProductId = productId;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
