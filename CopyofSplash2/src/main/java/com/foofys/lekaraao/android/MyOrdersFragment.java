package com.foofys.lekaraao.android;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;

import com.foofys.lekaraao.android.SubcategoriesFragment.OnSubCategorySelectedListener;
import com.loopj.android.image.SmartImageView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

public class MyOrdersFragment extends Fragment {

	ArrayList<Integer> array;

	//HashSet hs;
	ArrayList<OrderStatus> list;
	List<ParseObject> ob;
	String c;
	ViewGroup root;
	ListView l;
	OrdersAdapter adapter;
	int j ;
	boolean isPickup = false;
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		list = new ArrayList<OrderStatus>();
		array = new ArrayList<Integer>();
		c = MainActivity.USER_EMAIL;
	//	hs = new HashSet<String>();
		populateListView();

	}
	   
	   @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		  
	        root = (ViewGroup) inflater.inflate(R.layout.myorders, null);
	        l=(ListView)root.findViewById(R.id.listView1);
	       
	        return root;
	    }
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	/*
	 * public View onCreateView(LayoutInflater inflater, ViewGroup
	 * container,Bundle savedInstanceState) {
	 * 
	 * 
	 * return super.onCreateView(inflater, container, savedInstanceState); }
	 */

	private void populateListView() {
		// TODO Auto-generated method stub
		
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Order");
		query.orderByAscending("Status");
		query.whereEqualTo("username", c);
		//Log.d("USERNAME", c);
		query.orderByDescending("OrderID");
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
					if(objects.size()>0){
					ob = objects;
					DateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss");
					format.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
					
					String time = format.format(objects.get(0).getCreatedAt());
					
					//Toast.makeText(getActivity(),""+time, Toast.LENGTH_SHORT).show();
					for (int i = 0; i < objects.size(); i++) {
						ArrayList<Integer> pid = new ArrayList<Integer>();
						pid = (ArrayList<Integer>) objects.get(i).get("ProductID");
						if(pid!=null){
						j = pid.size();
						}else{
							j=1;
						}
						
						String ordertype = objects.get(i).getString("OrderType");
						if(ordertype!=null){
						if(ordertype.equalsIgnoreCase("pickanddrop")){
							isPickup = true;
						}else{
							isPickup = false;
						}
						}else{
							isPickup = false;
						}
						
						OrderStatus temp = new OrderStatus(objects.get(i).getInt("OrderID"), objects.get(i).getInt("Status"), j,
								format.format(objects.get(i).getCreatedAt()), isPickup);
						
						
						//ParseUser currentUser = ParseUser.getCurrentUser();
//						if((currentUser.getBoolean("PasswordEnabled") == false) && objects.get(i).getInt("Status") == 2)
//						{
//							Log.d("status","not logged in user plus deliverd");
//							
//								list.add(temp);
//							
//						}else{	
//							Log.d("Status","Logged in or not delivered");
//							list.add(temp);
//						}
						list.add(temp);
						//array.add(objects.get(i).getInt("OrderID"));
					//	Toast.makeText(getActivity(), ""+objects.get(i).getCreatedAt(), Toast.LENGTH_SHORT).show();
					}
				} }else {
					//Log.d("score", "Error: " + e.getMessage());
				}
				try {
					
					//array.addAll(hs);
					 adapter = new OrdersAdapter(getActivity(),list);
		    	        l.setAdapter(new OrdersAdapter(getActivity(),list));
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

	}

/*	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);

		// Toast.makeText(getActivity(), ""
		// +ob.get(position).getString("Subcategory")
		// .toString(),
		// Toast.LENGTH_LONG).show();

		String subcategory = getListView().getItemAtPosition(position)
				.toString();
		// Toast.makeText(getActivity(),
		// getListView().getItemAtPosition(position).toString(),
		// Toast.LENGTH_SHORT).show();
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Products");
		query.whereEqualTo("Store", subcategory);
		// query.whereEqualTo("Category", c);
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
					// ArrayList<Integer> array = new ArrayList<Integer>();
					// ob = objects;
					for (int i = 0; i < objects.size(); i++) {

						list.add(objects.get(i).getInt("ProductID"));
						// Log.d("sub",""+objects.get(i).getInt("ProductID"));

					}
				} else {
					// Log.d("score", "Error: " + e.getMessage());
				}
				int StoreID = objects.get(0).getInt("StoreID");
				Log.d("abc", "" + list.size());
				mCallback.subCategorySelected(list, StoreID);

			}

		});  

	}  */

}
