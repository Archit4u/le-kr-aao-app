package com.foofys.lekaraao.android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.foofys.lekaraao.android.CategoriesFragment.OnCategorySelectedListener;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.app.FragmentManager.OnBackStackChangedListener;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
//import android.app.ListFragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class SubcategoriesFragment extends Fragment implements
		OnItemClickListener {
	OnSubCategorySelectedListener mCallback;
	private LocationManager locationManager;
	private String provider;
	protected ProgressDialog proDialog;

	public interface OnSubCategorySelectedListener {
		public void subCategorySelected(ArrayList<String> list, String StoreID);
	}

	private static ArrayList<String> list = new ArrayList<String>();

	public static ArrayList<String> getList() {
		return list;
	}

	public static void setList(ArrayList<String> list) {
		SubcategoriesFragment.list = list;
	}

	ArrayList<String> array;
	ArrayList<String> stores;
	// ArrayAdapter<String> adapter;
	String c, city, storeid;
	HashSet hs;
	LayoutInflater inflater;
	List<ParseObject> ob;

	ViewGroup root;
	ListView l;
	StoresAdapter adapter;
	TextView locationoff, nostores;

	protected void startLoading() {
		proDialog = new ProgressDialog(getActivity());
		proDialog.setCancelable(false);
		proDialog.show();
		proDialog.setContentView(R.layout.progressdialog);
		// proDialog.setMessage("loading...");
		// proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

	}

	protected void stopLoading() {
		proDialog.dismiss();
		proDialog = null;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		root = (ViewGroup) inflater.inflate(R.layout.storeslist, null);
		l = (ListView) root.findViewById(R.id.list);
		locationoff = (TextView) root.findViewById(R.id.LocationOff);
		nostores = (TextView) root.findViewById(R.id.NoStores);
		locationoff.setVisibility(View.GONE);
		nostores.setVisibility(View.GONE);
		// Nostores = inflater.inflate(R.layout.bestnearbyempty, null);
		// Locationoff = inflater.inflate(R.layout.locationnotdetected, null);
		return root;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (OnSubCategorySelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnHeadlineSelectedListener");
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		// LayoutInflater li = (LayoutInflater)
		// getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		array = new ArrayList<String>();
		stores = new ArrayList<String>();
		hs = new HashSet<String>();
		if (c.equals("Best Nearby")) {

			populateList();
		} else {
			populateListView();
		}
	}

	private void populateList() {
		// TODO Auto-generated method stub
		/*
		 * locationManager = (LocationManager) getActivity().getSystemService(
		 * Context.LOCATION_SERVICE); Criteria criteria = new Criteria();
		 * provider = locationManager.getBestProvider(criteria, false); Location
		 * location =
		 * locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER
		 * );
		 * 
		 * if (location != null) { Log.d("location  sda", "Provider " +
		 * location.getLatitude() + location.getLongitude() +
		 * " has been selected."); int lat = (int) (location.getLatitude()); int
		 * lng = (int) (location.getLongitude());
		 */
		startLoading();

		if (MainActivity.getLatitude() != 0.0) {

			ParseGeoPoint point = new ParseGeoPoint(MainActivity.getLatitude(),
					MainActivity.getLongitude());
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Store");
			query.whereWithinKilometers("LatLong", point, 3);
			query.whereEqualTo("City", city);
			query.whereEqualTo("Status", 1);
			query.findInBackground(new FindCallback<ParseObject>() {

				@Override
				public void done(List<ParseObject> arg0, ParseException arg1) {
					// TODO Auto-generated method stub
					if (arg0.size() > 0) {
						for (int i = 0; i < arg0.size(); i++) {
							stores.add(arg0.get(i).getObjectId());
							array.add(arg0.get(i).getString("Name"));

						}
						try {
							// hs.addAll(array);
							// array.clear();
							// array.addAll(hs);
							/*
							 * ArrayAdapter<String> adapter = new
							 * ArrayAdapter<String>( getActivity(),
							 * android.R.layout.simple_list_item_1, array);
							 * setListAdapter(adapter);
							 */
							adapter = new StoresAdapter(getActivity(), array);
							/*
							 * ArrayAdapter<String> adapter = new
							 * ArrayAdapter<String>( getActivity(),
							 * android.R.layout.simple_list_item_1, array);
							 */
							// getListView().addHeaderView(header);
							l.setAdapter(adapter);

						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						stopLoading();

					} else {
						// Log.d("nolocation", "Provider " + provider +
						// " has been selected.");
						/*
						 * ArrayAdapter<String> adapter = new
						 * ArrayAdapter<String>( getActivity(),
						 * android.R.layout.simple_list_item_1, array);
						 * setListAdapter(adapter); ((ViewGroup)
						 * getListView().getParent()).addView(Nostores);
						 */
						stopLoading();

						adapter = new StoresAdapter(getActivity(), array);

						// l.setEmptyView(Nostores);
						l.setAdapter(adapter);
						l.setVisibility(View.GONE);
						nostores.setText("Sorry, no stores found within a 3km radius around your location.");
						nostores.setVisibility(View.VISIBLE);
					}
				}
			});
			l.setOnItemClickListener(this);

		} else {
			//Log.d("nolocation", "Provider " + provider + " has been selected.");
			/*
			 * ArrayAdapter<String> adapter = new ArrayAdapter<String>(
			 * getActivity(), android.R.layout.simple_list_item_1, array);
			 * setListAdapter(adapter); ((ViewGroup)
			 * getListView().getParent()).addView(Locationoff);
			 */
			adapter = new StoresAdapter(getActivity(), array);

			l.setAdapter(adapter);
			if (array.size() == 0) {
				// l.setVisibility(View.GONE);
				locationoff.setVisibility(View.VISIBLE);
			}

			// ((ViewGroup) l.getParent()).addView(Locationoff);
			l.setOnItemClickListener(this);
			stopLoading();

		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//Log.d("pause", "evs");
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		array = new ArrayList<String>();
		super.onCreate(savedInstanceState);
		city = getArguments().getString("scity");
		c = getArguments().getString("category");
		//Log.d("bundle", c);
	}

	/*
	 * public View onCreateView(LayoutInflater inflater, ViewGroup
	 * container,Bundle savedInstanceState) {
	 * 
	 * 
	 * return super.onCreateView(inflater, container, savedInstanceState); }
	 */

	private void populateListView() {
		// TODO Auto-generated method stub

		startLoading();

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Store");
		query.whereEqualTo("City", city);
		query.whereEqualTo("Category", c);
		query.whereEqualTo("Status", 1);
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
					ob = objects;
					for (int i = 0; i < objects.size(); i++) {
						stores.add(objects.get(i).getObjectId());
						array.add(objects.get(i).getString("Name"));
					}
					stopLoading();

				} else {
					//Log.d("score", "Error: " + e.getMessage());
					stopLoading();

				}
				try {
					// hs.addAll(array);
					// array.clear();
					// array.addAll(hs);
					/*
					 * ArrayAdapter<String> adapter = new ArrayAdapter<String>(
					 * getActivity(), android.R.layout.simple_list_item_1,
					 * array); setListAdapter(adapter); ((ViewGroup)
					 * getListView().getParent()).addView(Nostores);
					 */
					adapter = new StoresAdapter(getActivity(), array);

					l.setAdapter(adapter);
					// l.setAdapter(adapter);
					if (array.size() == 0) {
						l.setVisibility(View.GONE);
						nostores.setVisibility(View.VISIBLE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		l.setOnItemClickListener(this);

	}

	/*
	 * @Override public void onListItemClick(ListView l, View v, int position,
	 * long id) { super.onListItemClick(l, v, position, id); if(array.size()>0){
	 * String subcategory = array.get(position); } //
	 * getListView().getItemAtPosition(position).toString(); storeid =
	 * stores.get(position); Log.d("STOREid " + position, "" + storeid);
	 * 
	 * ParseQuery<ParseObject> query = ParseQuery.getQuery("Products");
	 * query.whereEqualTo("StoreID", storeid); query.findInBackground(new
	 * FindCallback<ParseObject>() {
	 * 
	 * @Override public void done(List<ParseObject> objects, ParseException e) {
	 * // TODO Auto-generated method stub if (e == null) {
	 * 
	 * for (int i = 0; i < objects.size(); i++) { Log.d("PRODUCTID", "" +
	 * objects.get(i).getObjectId()); list.add(objects.get(i).getObjectId()); }
	 * Log.d("LIST SIZE", "" + list.size()); } else { Log.d("score", "Error: " +
	 * e.getMessage()); } // String StoreID = ob.get(0).getString("StoreID");
	 * Log.d("STOREid", "" + storeid); mCallback.subCategorySelected(list,
	 * storeid);
	 * 
	 * }
	 * 
	 * }); }
	 */
	/*
	 * @Override public void onListItemClick(ListView l, View v, int position,
	 * long id) { // TODO Auto-generated method stub super.onListItemClick(l, v,
	 * position, id);
	 * 
	 * // Toast.makeText(getActivity(), ""
	 * +ob.get(position).getString("Subcategory") // .toString(), //
	 * Toast.LENGTH_LONG).show();
	 * 
	 * String subcategory =
	 * getListView().getItemAtPosition(position).toString(); //
	 * Toast.makeText(getActivity(),
	 * getListView().getItemAtPosition(position).toString(),
	 * Toast.LENGTH_SHORT).show(); ParseQuery<ParseObject> query =
	 * ParseQuery.getQuery("Products"); query.whereEqualTo("Store",
	 * subcategory); //query.whereEqualTo("Category", c);
	 * query.findInBackground(new FindCallback<ParseObject>() { public void
	 * done(List<ParseObject> objects, ParseException e) { if (e == null) { //
	 * ArrayList<Integer> array = new ArrayList<Integer>(); //ob = objects;
	 * for(int i=0;i<objects.size();i++){
	 * 
	 * list.add(objects.get(i).getObjectId()); //
	 * Log.d("sub",""+objects.get(i).getInt("ProductID"));
	 * 
	 * } } else { //Log.d("score", "Error: " + e.getMessage()); } String StoreID
	 * = objects.get(0).getString("StoreID"); Log.d("abc", ""+list.size());
	 * mCallback.subCategorySelected(list, StoreID);
	 * 
	 * }
	 * 
	 * });
	 * 
	 * }
	 */

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		// TODO Auto-generated method stub
		if (array.size() > 0) {
			String subcategory = array.get(position);
		}
		// getListView().getItemAtPosition(position).toString();
		storeid = stores.get(position);
		//Log.d("STOREid " + position, "" + storeid);

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Products");
		query.whereEqualTo("StoreID", storeid);
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub
				if (e == null) {

					for (int i = 0; i < objects.size(); i++) {
						//Log.d("PRODUCTID", "" + objects.get(i).getObjectId());
						list.add(objects.get(i).getObjectId());
					}
					//Log.d("LIST SIZE", "" + list.size());
				} else {
					//Log.d("score", "Error: " + e.getMessage());
				}
				if (objects.size() > 0) {
					//Log.d("abc", "" + list.size());
					mCallback.subCategorySelected(list, storeid);
				} else {

					Toast.makeText(getActivity(), "No products in this store.",
							Toast.LENGTH_LONG).show();

				}
				// String StoreID = ob.get(0).getString("StoreID");

			}

		});
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		//Log.d("destro", "1");

	}
}
