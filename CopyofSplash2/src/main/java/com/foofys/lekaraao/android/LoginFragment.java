package com.foofys.lekaraao.android;


import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;



//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
//import android.app.FragmentManager;
//import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginFragment extends Fragment{
	public LoginFragment(){}
	Button loginbutton;
    Button signup;
    String usernametxt;
    String passwordtxt, city;
    EditText password;
    EditText username;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        username = (EditText) rootView.findViewById(R.id.username);
        password = (EditText) rootView.findViewById(R.id.password);
        loginbutton = (Button) rootView.findViewById(R.id.login);
        signup = (Button) rootView.findViewById(R.id.signup);
        return rootView;
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		if(getArguments()!=null){
			city = getArguments().getString("selectedcity");
		} 	
		loginbutton.setOnClickListener(new OnClickListener() {
			 
            public void onClick(View arg0) {
                // Retrieve the text entered from the EditText
                usernametxt = username.getText().toString();
                passwordtxt = password.getText().toString();
 
                // Send data to Parse.com for verification
                ParseUser.logInInBackground(usernametxt, passwordtxt,
                        new LogInCallback() {
                            public void done(ParseUser user, ParseException e) {
                                if (user != null) {
                                    // If user exist and authenticated, send user to Welcome.class
                                	Fragment fragment = new FeaturedFragment();
                                	Bundle args = new Bundle();
                        			args.putString("selectedcity", city);
                        			//Log.d("error", selectedCity);
                        			fragment.setArguments(args);
                        			
                        			FragmentManager fragmentManager = getFragmentManager();
                        			FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        			transaction.addToBackStack(null);
                        			InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        			imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                        			transaction.replace(R.id.frame_container, fragment).commit();
                        			
                                } else {
                                    Toast toast = Toast.makeText(
                                            getActivity(),
                                            "No such user exist, please signup",
                                            Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                					toast.show();
                                }
                            }
                        });
            }
        });
        // Sign up Button Click Listener
        signup.setOnClickListener(new OnClickListener() {
 
            public void onClick(View arg0) {
            	
            	Fragment fragment = new SignupFragment();
            	Bundle args = new Bundle();
    			args.putString("selectedcity", city);
    			//Log.d("error", selectedCity);
    			fragment.setArguments(args);
    			
    			FragmentManager fragmentManager = getFragmentManager();
    			FragmentTransaction transaction = getFragmentManager().beginTransaction();
    			transaction.addToBackStack(null);

    			transaction.replace(R.id.frame_container, fragment).commit();
                
                        }
                    });
	}
	
}
