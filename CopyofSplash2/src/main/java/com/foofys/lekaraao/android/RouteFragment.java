package com.foofys.lekaraao.android;

import in.wptrafficanalyzer.locationdistancetimemapv2.DirectionsJSONParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.javapapers.android.maps.path.HttpConnection;

public class RouteFragment extends Fragment {
	private static final LatLng LOWER_MANHATTAN = new LatLng(40.722543,
			-73.998585);
	private static final LatLng BROOKLYN_BRIDGE = new LatLng(40.7057, -73.9964);
	// private static final LatLng WALL_STREET = new LatLng(40.7064, -74.0094);

	ViewGroup root;
	LatLng toPosition, fromPosition;
	GoogleMap googleMap;
	final String TAG = "PathGoogleMapActivity";
	TextView distance, cost, time;
	Button deliver;
	MapView mapView;
	private Bundle bundle;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// root = (ViewGroup) inflater.inflate(R.layout.mapfragment, null);
		super.onCreateView(inflater, container, savedInstanceState);
		root = (ViewGroup) inflater.inflate(R.layout.routefragment,
				container, false);
		MapsInitializer.initialize(getActivity());

		try {
			// Loading map
			initializeMap();

		} catch (Exception e) {
			e.printStackTrace();
		}

		mapView = (MapView) root.findViewById(R.id.map);
		mapView.onCreate(bundle);
		googleMap = ((MapView) root.findViewById(R.id.map)).getMap();

		distance = (TextView) root.findViewById(R.id.tVdist);
		cost = (TextView) root.findViewById(R.id.cost);
		time = (TextView) root.findViewById(R.id.time);
		deliver = (Button) root.findViewById(R.id.bDeliver);
	//	googleMap = fm.getMap();

		deliver.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Intent i = new Intent(PathGoogleMapActivity.this,
				// CustomOrderDetails.class);
			}
		});

		 fromPosition = getArguments().getParcelable("from");
		 toPosition = getArguments().getParcelable("to");

	

		return root;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bundle = savedInstanceState;
	}
	String url;
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		 url = getDirectionsUrl(toPosition, fromPosition);
		mapView.onResume();
		initializeMap();
	}
	
	private void initializeMap() {
	

		if (googleMap == null) {
			googleMap = ((MapView) root.findViewById(R.id.map)).getMap();
		}
		MarkerOptions options = new MarkerOptions();
		options.position(toPosition);
		options.position(fromPosition);
		// options.position(WALL_STREET);
		googleMap.addMarker(options);
		addMarkers();
		// Getting URL to the Google Directions API
		

		DownloadTask downloadTask = new DownloadTask();

		// Start downloading json data from Google Directions API
		downloadTask.execute(url);
		// String url = getMapsApiDirectionsUrl();
		// ReadTask downloadTask = new ReadTask();
		// downloadTask.execute(url);
		//
		googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(toPosition,
				13));
		
		if (googleMap == null) {
			Toast.makeText(getActivity(), "Sorry! unable to create maps",
					Toast.LENGTH_SHORT).show();
		}
	}
	private String getDirectionsUrl(LatLng origin, LatLng dest) {

		// Origin of route
		String str_origin = "origin=" + origin.latitude + ","
				+ origin.longitude;

		// Destination of route
		String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

		// Sensor enabled
		String sensor = "sensor=false";

		// Building the parameters to the web service
		String parameters = str_origin + "&" + str_dest + "&" + sensor;

		// Output format
		String output = "json";

		// Building the url to the web service
		String url = "https://maps.googleapis.com/maps/api/directions/"
				+ output + "?" + parameters;
		//Log.d("URL", url);

		return url;
	}

	/** A method to download json data from url */
	private String downloadUrl(String strUrl) throws IOException {
		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL(strUrl);

			// Creating an http connection to communicate with url
			urlConnection = (HttpURLConnection) url.openConnection();

			// Connecting to url
			urlConnection.connect();

			// Reading data from url
			iStream = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(
					iStream));

			StringBuffer sb = new StringBuffer();

			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			data = sb.toString();

			br.close();

		} catch (Exception e) {
			//Log.d("Exception while downloading url", e.toString());
		} finally {
			iStream.close();
			urlConnection.disconnect();
		}
		return data;
	}

	// Fetches data from url passed
	private class DownloadTask extends AsyncTask<String, Void, String> {

		// Downloading data in non-ui thread
		@Override
		protected String doInBackground(String... url) {

			// For storing data from web service
			//Log.d("DLTAASK", "pre");
			String data = "";

			try {
				// Fetching the data from web service
				data = downloadUrl(url[0]);
			} catch (Exception e) {
				//Log.d("Background Task", e.toString());
			}
			return data;
		}

		// Executes in UI thread, after the execution of
		// doInBackground()
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			//Log.d("DLTAASK", "post");
			ParserTask parserTask = new ParserTask();

			// Invokes the thread for parsing the JSON data
			parserTask.execute(result);

		}
	}

	/** A class to parse the Google Places in JSON format */
	private class ParserTask extends
			AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

		// Parsing the data in non-ui thread
		@Override
		protected List<List<HashMap<String, String>>> doInBackground(
				String... jsonData) {
			//Log.d("PARSERTAASK", "pre");
			JSONObject jObject;
			List<List<HashMap<String, String>>> routes = null;

			try {
				jObject = new JSONObject(jsonData[0]);
				DirectionsJSONParser parser = new DirectionsJSONParser();

				// Starts parsing data
				routes = parser.parse(jObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return routes;
		}

		// Executes in UI thread, after the parsing process
		@Override
		protected void onPostExecute(List<List<HashMap<String, String>>> result) {
			ArrayList<LatLng> points = null;
			PolylineOptions lineOptions = null;
			MarkerOptions markerOptions = new MarkerOptions();
			String distance = "";
			String duration = "";
			//Log.d("PARSERTAASK", "post");

			if (result.size() < 1) {
				Toast.makeText(getActivity(), "No Points", Toast.LENGTH_SHORT)
						.show();
				return;
			}

			// Traversing through all the routes
			for (int i = 0; i < result.size(); i++) {
				points = new ArrayList<LatLng>();
				lineOptions = new PolylineOptions();

				// Fetching i-th route
				List<HashMap<String, String>> path = result.get(i);

				// Fetching all the points in i-th route
				for (int j = 0; j < path.size(); j++) {
					HashMap<String, String> point = path.get(j);

					if (j == 0) { // Get distance from the list
						distance = (String) point.get("distance");
						continue;
					} else if (j == 1) { // Get duration from the list
						duration = (String) point.get("duration");
						continue;
					}

					double lat = Double.parseDouble(point.get("lat"));
					double lng = Double.parseDouble(point.get("lng"));
					LatLng position = new LatLng(lat, lng);

					points.add(position);
				}

				// Adding all the points in the route to LineOptions
				lineOptions.addAll(points);
				lineOptions.width(2);
				lineOptions.color(Color.GREEN);

			}
			String[] split = distance.split("\\s+");

			int dist = Math.round(Float.parseFloat(split[0]));

			int deliveryCost = dist * 5 + 20;
			cost.setText("Rs. " + deliveryCost);
			RouteFragment.this.distance.setText(distance);
			time.setText("60 mins");
			// Drawing polyline in the Google Map for the i-th route
			googleMap.addPolyline(lineOptions);
		}
	}

	private String getMapsApiDirectionsUrl() {
		String waypoints = "waypoints=optimize:true|" + toPosition.latitude
				+ "," + toPosition.longitude + "|" + "|"
				+ fromPosition.latitude + "," + fromPosition.longitude;

		String sensor = "sensor=false";
		String params = waypoints + "&" + sensor;
		String output = "json";
		String url = "https://maps.googleapis.com/maps/api/directions/"
				+ output + "?" + params;
		return url;
	}

	private void addMarkers() {
		if (googleMap != null) {
			googleMap.addMarker(new MarkerOptions().position(toPosition)
					.title("First Point"));
			googleMap.addMarker(new MarkerOptions().position(fromPosition)
					.title("Second Point"));
			// googleMap.addMarker(new MarkerOptions().position(WALL_STREET)
			// .title("Third Point"));
		}
	}

	private class ReadTask extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... url) {
			String data = "";
			try {
				HttpConnection http = new HttpConnection();
				data = http.readUrl(url[0]);
			} catch (Exception e) {
				//Log.d("Background Task", e.toString());
			}
			return data;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			new ParserTask().execute(result);
		}
	}

}
