package com.foofys.lekaraao.android;

import java.util.ArrayList;


import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class StoresAdapter extends BaseAdapter {

	Context c;
	ArrayList<String> list;

	public StoresAdapter(Context baseContext, ArrayList<String> list) {
		
		this.c = baseContext;
		this.list = list;
		//Log.d("asASAs", "DSADASASAS");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		//Log.d("counrt", ""+list.size());
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		//Log.d("getview", "dedeeedeed");
		View row = LayoutInflater.from(c).inflate(R.layout.storerow, null);
		TextView storename = (TextView) row.findViewById(R.id.title);
		storename.setText(list.get(position));

		//Log.d("Store", list.get(position));
		return row;
	}

}
