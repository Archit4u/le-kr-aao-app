package com.foofys.lekaraao.android;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
 
import com.loopj.android.image.SmartImageView;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
 
public class ExpandableAdapter extends BaseExpandableListAdapter {
 
    private Context _context;
    Context c;
    int pos=0, headerpos;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;
	ArrayList<SingleRow> list;
 
    public ExpandableAdapter(Context context, List<String> listDataHeader,
            HashMap<String, List<String>> listChildData, ArrayList<SingleRow> list) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.list = list;
    }
 
    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
        
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, final int childPosition,
            boolean isLastChild, View convertView, ViewGroup parent) {
   
    	//_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition);
    /*	pos = 0;
    		if(groupPosition==0){
    			pos = childPosition;
    		}else{
    			for(int i = 0 ; i<= groupPosition; i++){
    				pos += getChildrenCount(i);
    				Log.d("pos", pos+"..."+getChildrenCount(i));
    			}
    			pos = pos-1;
    			
    		}*/
    		
    	for(int i = 0; i<list.size(); i++){
    		if(list.get(i).getName() ==  _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition)){
    			pos =i ;
    			break;
    		}
    	}
    	View row = LayoutInflater.from(_context).inflate(R.layout.singlerow, null);

		TextView name = (TextView) row.findViewById(R.id.textView1);
		final TextView qty = (TextView) row.findViewById(R.id.qty);
		SmartImageView image = (SmartImageView) row.findViewById(R.id.my_image);
		//TextView desc = (TextView) row.findViewById(R.id.tVdesc);
		//final TextView incart = (TextView) row.findViewById(R.id.incart);
		//incart.setVisibility(View.GONE);
		 TextView desc = (TextView) row.findViewById(R.id.tVdesc);
		TextView price = (TextView) row.findViewById(R.id.textView3);
		// TextView store = (TextView) row.findViewById(R.id.textView4);
		final ImageView add = (ImageView) row.findViewById(R.id.bAdd);
		final ImageView plus = (ImageView) row.findViewById(R.id.bPlus);
		final ImageView minus = (ImageView) row.findViewById(R.id.bMinus);
		// SmartImageView myImage = (SmartImageView)
		// row.findViewById(R.id.my_image);
		final SingleRow temp = list.get(pos);
		//desc.setText(temp.getDescription());
		qty.setText("" + temp.getQty());
		//Log.d("abcimage", "" + temp.getProductId());
		// pos = temp.getProductId();
		// myImage.setImageUrl(temp.getUrl());
		name.setText(temp.getName());
		 desc.setText(temp.getDescription());
		 

		
		 
		price.setText("\u20B9"+temp.getPrice()+" per " + temp.getUnit());
		// store.setText(temp.getStore());
		image.setImageUrl(temp.getImage());
		for (int i = 0; i < MainActivity.getMyList().size(); i++) {
			
			if (temp.getProductId().equals(MainActivity.getMyList().get(i)
					.getProductId()) ){
		//		Log.d("pricelistasc", ""
			//			+ MainActivity.getMyList().get(i).getProductId());
				add.setEnabled(false);
				plus.setEnabled(false);
				minus.setEnabled(false);
				minus.setVisibility(View.INVISIBLE);
				plus.setVisibility(View.INVISIBLE);
				qty.setVisibility(View.INVISIBLE);
				/*
				 * if(incart.getTag()!=null)
				 * incart.setText(""+incart.getTag().toString
				 * ()+" nos. in cart"); incart.setVisibility(View.VISIBLE);
				 */

			}
			
		}

		if (add.isEnabled()) {
			add.setBackgroundResource(R.drawable.cart);
		} else {
			add.setBackgroundResource(R.drawable.cartdisabled);
		}
		add.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(_context, "Item Added to Cart!", Toast.LENGTH_SHORT)
						.show();
				// String str=(String) v.getTag();
				// Log.d("pricesadas", str);

				AddToCart obj = new AddToCart(temp.getProductId(), temp
						.getQty());
				
				MainActivity.getMyList().add(obj);
			//	incart.setText("" + temp.getQty() + "nos. in cart");
				add.setEnabled(false);
				add.setBackgroundResource(R.drawable.cartdisabled);
				add.setTag(temp.getProductId());
				//incart.setTag(temp.getQty());
				//Log.d("price", add.getTag().toString());
				Activity m = (Activity) _context;
				m.invalidateOptionsMenu();
				// ActionBarRefresh.refreshActionBarMenu(c);

				notifyDataSetChanged();
			}
		});
		plus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				temp.setQty(temp.getQty() + 1);
				qty.setText("" + temp.getQty());
				//notifyDataSetChanged();
			}
		});

		minus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (temp.getQty() > 1) {
					temp.setQty(temp.getQty() - 1);
					qty.setText("" + temp.getQty());
					//notifyDataSetChanged();
				}
			}
		});
		//if (add.getTag() != null)
			//Log.d("pricedsaa", add.getTag().toString());
		return row;
        //return convertView;
    }
 
    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
      
    }
 
    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        //Log.d("Numberproduct", ""+getChildrenCount(groupPosition));
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }
 
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
       // lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
 
        return convertView;
    }
 
    @Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}