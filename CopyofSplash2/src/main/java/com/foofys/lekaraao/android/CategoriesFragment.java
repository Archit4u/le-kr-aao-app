package com.foofys.lekaraao.android;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.loopj.android.image.SmartImageView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.app.Activity;
import android.app.ProgressDialog;
//import android.app.ListFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class CategoriesFragment extends Fragment {

	OnCategorySelectedListener mCallback;
	ArrayList<String> array = new ArrayList<String>();
	String city = new String();
	
	public interface OnCategorySelectedListener {
		public void categorySelected(String category, String city);
	}

	ViewGroup root;
	ListView l;
	CategoriesAdapter adapter;
	protected ProgressDialog proDialog;

	List<ParseObject> ob;
	HashSet hs = new HashSet<String>();
	String bestcat = new String("Best Nearby");

	private String pics = new String("http://files.parsetfss.com/e23846f1-ba6c-4a30-a0e5-9bc079383ba2/tfss-f315cfba-1834-42cf-9c2f-1f14823dd7e8-bestnearby.png");
	/*
	 * ,R.drawable.florist, R.drawable.food,R.drawable.groceries,
	 * R.drawable.medicines,R.drawable.stationery };
	 */
	// int j = categories.length;

	ArrayList<String> categories = new ArrayList<String>();
	/*
	 * ArrayList<String> pics = new ArrayList<String>();
	 */
	ArrayList<CategoryRow> category = new ArrayList<CategoryRow>();

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (OnCategorySelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnHeadlineSelectedListener");
		}
	}
	
	protected void startLoading() {
		proDialog = new ProgressDialog(getActivity());
		proDialog.setCancelable(false);
		proDialog.show();
		proDialog.setContentView(R.layout.progressdialog);
		//proDialog.setMessage("loading...");
		//proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		
	}

	protected void stopLoading() {
		proDialog.dismiss();
		proDialog = null;
	}


	/*
	 * public View onCreateView(LayoutInflater inflater, ViewGroup
	 * container,Bundle savedInstanceState) {
	 * 
	 * 
	 * return super.onCreateView(inflater, container, savedInstanceState); }
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		root = (ViewGroup) inflater.inflate(R.layout.categorieslist, null);
		l = (ListView) root.findViewById(R.id.list);
		CategoryRow nearby = new CategoryRow(bestcat, pics);
		View row = LayoutInflater.from(getActivity()).inflate(
				R.layout.categoryrow, null);
		TextView category = (TextView) row.findViewById(R.id.title);
		SmartImageView icon = (SmartImageView) row
				.findViewById(R.id.list_image);
		category.setText(nearby.getCategory());
		// icon.setBackgroundResource(nearby.getPic());
		icon.setImageUrl(nearby.getPic());
		
		//icon.setImageResource(pics);
		//l.addHeaderView(row);
	 	
		return root;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(getArguments()!=null ){
			city = getArguments().getString("selectedcity");
		}
		if(category.size()>0){
			adapter = new CategoriesAdapter(getActivity(), category);
			
			l.setAdapter(adapter);
			l.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					//Log.d("SelectedCategory"+position, ""+category.get(position).getCategory());
					
					mCallback
							.categorySelected(category.get(position).getCategory(), city);
					
					}
			});
		}else{
		populateListView();
		}
	}

	private void populateListView() {
		// TODO Auto-generated method stub
		//startLoading();

		category.clear();
		CategoryRow nearby = new CategoryRow(bestcat, pics);
		category.add(nearby) ;
		//Log.d("HELLO", "WORLD!!!!");
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Category");
		// query.whereEqualTo("Relation", "Store");
		query.whereEqualTo("Cat", "null");
		query.whereEqualTo("Status", 1);
		startLoading();

		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub
				//Log.d("HELLO", "WORLD!!!!2");
				if (e == null) {
					//Log.d("HELLO", "WORLD!!!!3");
					ob = objects;
					for (int i = 0; i < objects.size(); i++) {
						categories.add(objects.get(i).getString("Name"));
						// pics.add(objects.get(i).getString("Image"));
						/*
						 * objects.get(i).getString("Name");
						 * objects.get(i).getString("Image");
						 */
						CategoryRow temp = new CategoryRow(objects.get(i)
								.getString("Name"), objects.get(i).getString(
								"Image"));
						category.add(temp);
					
					}
					//Log.d("HELLO", "WORLD!!!!5");
					
					stopLoading();


				} else {
					//Log.d("HELLO", "WORLD!!!!4");
					//Log.d("score", "Error: " + e.getMessage());
					stopLoading();

				}
				adapter = new CategoriesAdapter(getActivity(), category);
				
				l.setAdapter(adapter);
				//Log.d("HELLO", "WORLD!!!!6");
			}
		});

		l.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				//Log.d("SelectedCategory"+position, ""+category.get(position).getCategory());
				
				mCallback
						.categorySelected(category.get(position).getCategory(), city);
				
				}
		});
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		/*
		 * ParseQuery<ParseObject> query = ParseQuery.getQuery("ProductMaster");
		 * //query.whereEqualTo("Category", "Food"); query.findInBackground(new
		 * FindCallback<ParseObject>() { public void done(List<ParseObject>
		 * objects, ParseException e) { if (e == null) { ob = objects; for(int
		 * i=0;i<objects.size();i++){ // Log.d("adapter", "" +
		 * objects.get(i).getParseFile("image").getUrl());
		 * array.add(objects.get(i).getString("Category")); } } else {
		 * Log.d("score", "Error: " + e.getMessage()); } try { hs.addAll(array);
		 * array.clear(); array.addAll(hs); ArrayAdapter<String> adapter = new
		 * ArrayAdapter<String>(getActivity(),
		 * android.R.layout.simple_list_item_1,array); setListAdapter(adapter);
		 * } catch (Exception e1) { // TODO Auto-generated catch block
		 * e1.printStackTrace(); } } });
		 */

	}

	/*
	 * @Override public void onListItemClick(ListView l, View v, int position,
	 * long id) { // TODO Auto-generated method stub super.onListItemClick(l, v,
	 * position, id); // Toast.makeText(getActivity(), "" +array.get(position),
	 * // Toast.LENGTH_LONG).show(); /* Toast.makeText(getActivity(), ""
	 * +ob.get(position).getString("Category") .toString(),
	 * Toast.LENGTH_LONG).show();
	 */
	// mCallback.categorySelected(array.get(position));
}
