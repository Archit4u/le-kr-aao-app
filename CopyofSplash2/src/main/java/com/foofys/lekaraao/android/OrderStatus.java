package com.foofys.lekaraao.android;

public class OrderStatus {
	
	int OrderID, Status, number;
	String time;
	boolean isPickup;
	
	public OrderStatus(int OrderID, int Status, int number, String time) {
		this.OrderID = OrderID;
		this.Status = Status;
		this.time = time;
		this.number = number;
	}
	public OrderStatus(int OrderID, int Status, int number, String time,
			boolean isPickup) {
		this.OrderID = OrderID;
		this.Status = Status;
		this.time = time;
		this.number = number;
		this.isPickup = isPickup;
		
	}
	public boolean isPickup() {
		return isPickup;
	}
	public void setPickup(boolean isPickup) {
		this.isPickup = isPickup;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public int getOrderID() {
		return OrderID;
	}
	public void setOrderID(int orderID) {
		OrderID = orderID;
	}
	public int getStatus() {
		return Status;
	}
	public void setStatus(int status) {
		Status = status;
	}

}
