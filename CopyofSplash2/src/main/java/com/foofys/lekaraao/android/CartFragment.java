package com.foofys.lekaraao.android;

import in.varunbhalla.jsontest.ServiceHandler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
//import android.app.Fragment;
import android.support.v4.app.Fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CartFragment extends Fragment {

	ArrayList<String> array;
	OnOrderPlacedListener mCallback;

	int total = 0;
	int amount = 0;
	protected ProgressDialog proDialog;
	ProgressDialog progress;

	public interface OnOrderPlacedListener {
		public void orderPlaced();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (OnOrderPlacedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnOrderPlacedListener");
		}
	}

	protected void startLoading() {
		proDialog = new ProgressDialog(getActivity());
		proDialog.setCancelable(false);
		proDialog.show();
		proDialog.setContentView(R.layout.progressdialog);
		// proDialog.setMessage("loading...");
		// proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

	}

	protected void stopLoading() {
		proDialog.dismiss();
		proDialog = null;
	}

	View footer;
	ListView l;
	Button order;
	int quant = 1;
	TextView empty, test;
	ArrayList<CartRow> list;
	List<ParseObject> ob;
	List<AddToCart> myList = new ArrayList<AddToCart>();

	// AddToCart cart = new AddToCart(id,qty);
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		myList = (List<AddToCart>) getArguments().getSerializable("cart");
		// Log.d("carter", ""+myList.get(0).getProductId());
		
//		HashMap<String, List<AddToCart>> itemsInCart = new HashMap<String, List<AddToCart>>();
//		itemsInCart.put(MainActivity.USER_ID, myList);
//		FileOutputStream fos;
//		try {
//			fos = getActivity().openFileOutput("Cart", Context.MODE_PRIVATE);
//			ObjectOutputStream oos = new ObjectOutputStream(fos);
//			oos.writeObject(itemsInCart );
//		    oos.close();
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	    
		/*
		 * for(int i=0;i<myList.size();i++){
		 * 
		 * }
		 */
		// array = getArguments().getIntegerArrayList("cart");
		/*
		 * Log.d("cartfrag", ""+array.size()); for(int i=0; i<array.size();i++)
		 * Log.d("product", ""+array.get(i));
		 */
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup root = (ViewGroup) inflater.inflate(R.layout.cartfragment,
				null);
		l = (ListView) root.findViewById(R.id.listCart);
		order = (Button) root.findViewById(R.id.bOrder);
		// footer = (TextView) root.findViewById(R.id.footer);
		empty = (TextView) root.findViewById(R.id.empty);
		footer = inflater.inflate(R.layout.cartfooter, null);
		test = (TextView) footer.findViewById(R.id.textView1);
		getActivity().invalidateOptionsMenu();
		empty.setVisibility(View.GONE);
		/*
		 * if (myList.size() > 0) { empty.setVisibility(View.GONE); }
		 */

		if (myList.size() == 0) {
			order.setVisibility(View.GONE);
			// footer.setVisibility(View.GONE);
		}

		return root;

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		list = new ArrayList<CartRow>();
		array = new ArrayList<String>();
		for (int i = 0; i < myList.size(); i++) {
			array.add(myList.get(i).getProductId());
			// Log.d("cartarray", ""+array.get(i));
		}
		Collections.sort(myList);
		order.setEnabled(false);
		 progress = ProgressDialog.show(getActivity(), "",
         	    "Please wait", true);

		// startLoading();

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Products");
		// query.whereEqualTo("Category", c);
		query.whereContainedIn("objectId", array);
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
					int p = 0;
					total = 0;
					
					//Log.d("CARTLISTSIZE", ""+objects.size());

					ob = objects;
					for (int i = 0; i < objects.size(); i++) {
						// Log.d("carterpid",
						// ""+objects.get(i).getInt("objectId"));
						// Log.d("abcde",
						// ""+objects.get(i).getString("Description"));
						// Log.d("abcde", ""+objects.get(i).getString("Name"));

						try {
							for (int j = 0; j < myList.size(); j++) {
								if (objects.get(i).getObjectId()
										.equals(myList.get(j).getProductId())) {
									quant = myList.get(j).getQty();
									break;
								}
							}

							CartRow temp = new CartRow(objects.get(i)
									.getString("Name"), quant, objects.get(i)
									.getInt("Price"), objects.get(i)
									.getObjectId(), objects.get(i).getString(
									"Store"), objects.get(i).getString("Unit"),
									objects.get(i).getString("Image"));
							list.add(temp);
							p = objects.get(i).getInt("Price") * temp.getQty();
							total = total + p;
							// Log.d("price",""+ p);
							// Log.d("price",""+ total);
							// Log.d("price",""+ temp.getQty());
							// Log.d("abcd",""+temp.getPrice());
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						// array.add(objects.get(i).getString("Subcategory"));
					}
					// stopLoading();
					LayoutInflater inflater;
					try {
						inflater = (LayoutInflater) getActivity().getSystemService(
								getActivity().LAYOUT_INFLATER_SERVICE);
						View v = inflater.inflate(R.layout.cartfooter, null);
						TextView tot = (TextView) v.findViewById(R.id.textView1);
						tot.setText("The order total is " + total);
						// test.setText("" +
						// MainActivity.getMyList().get(0).getQty());
						// l.addFooterView(footer);
						//Log.d("onresume",
							//	"" + list.size() + "   mylist " + myList.size());
						l.setEmptyView(empty);
						l.setAdapter(new CartAdapter(getActivity(), list, l));
						//Log.d("ONCLICK:ISTENER", "SET");
						order.setEnabled(true);
						progress.dismiss();
						setOnClick();
						// l.addFooterView(footer);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				} else {
					//Log.d("score", "Error: " + e.getMessage());
					// stopLoading();

				}
				

				
				
				/*
				 * order.setOnClickListener(new OnClickListener() {
				 * 
				 * @Override public void onClick(View v) { // TODO
				 * Auto-generated method stub EditText et; for(int
				 * i=0;i<l.getCount();i++){ v = l.getAdapter().getView(i,
				 * null,null);
				 * 
				 * et = (EditText) v.findViewById(R.id.eTQty); Log.d("pls",
				 * ""+et.getText().toString()); et.clearFocus();} for(int
				 * i=0;i<myList.size();i++){ Log.d("orderid",
				 * "p  "+myList.get(i)
				 * .getProductId()+"q  "+myList.get(i).getQty()); } /*EditText
				 * et; for(int i=0;i<l.getCount();i++){ v =
				 * l.getAdapter().getView(i, null,null); et = (EditText)
				 * v.findViewById(R.id.eTQty); Log.d("pls",
				 * ""+et.getText().toString()); } } });
				 */

				// ArrayAdapter<String> adapter = new
				// ArrayAdapter<String>(getActivity(),
				// android.R.layout.simple_list_item_1,array);
				// setListAdapter(adapter);
			}

		
		});

	}
	
	private void setOnClick() {
		// TODO Auto-generated method stub
		order.setOnClickListener(new OnClickListener() {
			
			
			

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//ParseUser currentUser = ParseUser.getCurrentUser();
			//	Log.d("GOOGLE", ""+FacebookLogin.googleUser);
				//Log.d("FAECBOOK",""+ FacebookLogin.facebookUser);
			//	if ((currentUser != null)
				//		&& (currentUser.getBoolean("PasswordEnabled") == true)) {
					
					if (FacebookLogin.googleUser||FacebookLogin.facebookUser){
						//Log.d("total", ""+total);
						
						new placeOrder().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//						Intent i = new Intent(getActivity(),
//								Confirmation.class);
//						Bundle args = new Bundle();
//						args.putSerializable("cart", (Serializable) myList);
//						args.putInt("amount", total);
//						i.putExtras(args);
//						//progress.dismiss();
//						//stopLoading();
//						startActivity(i);
//						amount = 0;
//					for (int j = 0; j < list.size(); j++) {
//						amount += Integer.parseInt(list.get(j)
//								.getPrice()) * list.get(j).getQty();
//					}

//					Intent i = new Intent(getActivity(),
//							Confirmation.class);
//					Bundle args = new Bundle();
//					args.putSerializable("cart", (Serializable) myList);
//					args.putInt("amount", amount);
//					i.putExtras(args);
//					startActivity(i);

				}

				else {
					for (int j = 0; j < list.size(); j++) {
						amount += Integer.parseInt(list.get(j)
								.getPrice()) * list.get(j).getQty();
					}

					Intent i = new Intent(getActivity(),
							ConfirmationWithoutLogin.class);
					Bundle args = new Bundle();
					args.putSerializable("cart", (Serializable) myList);
					args.putInt("amount", amount);
					i.putExtras(args);
					startActivity(i);

					/*
					 * Toast.makeText(getActivity(),
					 * "Please Login to place order",
					 * Toast.LENGTH_SHORT).show();
					 */
					// show the signup or login screen
				}

			}

		});
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

	}
	
	 private class placeOrder extends AsyncTask<Void, Void, Void> {
		 
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	            // Showing progress dialog
	           progress = ProgressDialog.show(getActivity(), "",
	            	    "Please wait", true);
	           amount = 0;
	        	
				for (int j = 0; j < list.size(); j++) {
					amount += Integer.parseInt(list.get(j)
							.getPrice()) * list.get(j).getQty();
				//	Log.d("amount", ""+amount);
				}
	            
				//Log.d("Done", "done");
	          //  startLoading();
	 
	        }
	 
	        @Override
	        protected Void doInBackground(Void... arg0) {
	            // Creating service handler class instance
	        //	Log.d("DOINBACKGROUNDLISTSIZE", "do");
	        	
	 
	            return null;
	        }
	 
	        @Override
	        protected void onPostExecute(Void result) {
	            super.onPostExecute(result);
	            //Log.d("Done", "doneps");
	        	Intent i = new Intent(getActivity(),
						Confirmation.class);
				Bundle args = new Bundle();
				args.putSerializable("cart", (Serializable) myList);
				args.putInt("amount", amount);
				i.putExtras(args);
				progress.dismiss();
				//stopLoading();
				startActivity(i);
	        }
	 
	    }
	
	
}
