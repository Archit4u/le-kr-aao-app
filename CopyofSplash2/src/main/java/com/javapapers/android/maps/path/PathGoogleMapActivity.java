package com.javapapers.android.maps.path;

import in.varunbhalla.jsontest.ServiceHandler;
import in.wptrafficanalyzer.locationdistancetimemapv2.DirectionsJSONParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.foofys.lekaraao.android.CustomOrderDetails;
import com.foofys.lekaraao.android.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class PathGoogleMapActivity extends FragmentActivity {

	private static final LatLng LOWER_MANHATTAN = new LatLng(40.722543,
			-73.998585);
	private static final LatLng BROOKLYN_BRIDGE = new LatLng(40.7057, -73.9964);
	// private static final LatLng WALL_STREET = new LatLng(40.7064, -74.0094);
	LatLng toPosition, fromPosition;
	GoogleMap googleMap;
	final String TAG = "PathGoogleMapActivity";
	TextView distance, cost, time;
	Button deliver;
	int deliveryCost, dist;
	String to, from;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_path_google_map);
		SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		distance = (TextView) findViewById(R.id.tVdist);
		cost = (TextView) findViewById(R.id.cost);
		time = (TextView) findViewById(R.id.time);
		deliver = (Button) findViewById(R.id.bDeliver);
		googleMap = fm.getMap();

		Bundle bundle = getIntent().getParcelableExtra("bundle");
		fromPosition = bundle.getParcelable("from_position");
		toPosition = bundle.getParcelable("to_position");
		to = bundle.getString("to");
		from = bundle.getString("from");
		MarkerOptions options = new MarkerOptions();
		options.position(fromPosition);
		options.position(toPosition);
		//Log.d("dasasas", to + from);
		// options.position(WALL_STREET);
		// if(options!=null)
		googleMap.addMarker(options);
		addMarkers();
		// Getting URL to the Google Directions API
		String url = getDirectionsUrl(fromPosition, toPosition);

		DownloadTask downloadTask = new DownloadTask();

		// Start downloading json data from Google Directions API
		downloadTask.execute(url);
		// String url = getMapsApiDirectionsUrl();
		// ReadTask downloadTask = new ReadTask();
		// downloadTask.execute(url);
		//
		googleMap.moveCamera(CameraUpdateFactory
				.newLatLngZoom(fromPosition, 13));

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		deliver.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (cost.getText().equals("Calculating..")) {
					Toast.makeText(getApplicationContext(),
							"Please wait while the cost is calculated", 1000)
							.show();
				} else {
					Intent i = new Intent(PathGoogleMapActivity.this,
							CustomOrderDetails.class);
					Bundle args = new Bundle();
					args.putInt("cost", dist);

					args.putString("to", to);
					args.putString("from", from);
					i.putExtra("bundle", args);
					startActivity(i);
				}
			}
		});
	}

	private String getDirectionsUrl(LatLng origin, LatLng dest) {

		// Origin of route
		String str_origin = "origin=" + origin.latitude + ","
				+ origin.longitude;

		// Destination of route
		String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

		// Sensor enabled
		String sensor = "sensor=false";

		// Building the parameters to the web service
		String parameters = str_origin + "&" + str_dest + "&" + sensor;

		// Output format
		String output = "json";

		// Building the url to the web service
		String url = "https://maps.googleapis.com/maps/api/directions/"
				+ output + "?" + parameters;

		return url;
	}

	/** A method to download json data from url */
	private String downloadUrl(String strUrl) throws IOException {
		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL(strUrl);

			// Creating an http connection to communicate with url
			urlConnection = (HttpURLConnection) url.openConnection();

			// Connecting to url
			urlConnection.connect();

			// Reading data from url
			iStream = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(
					iStream));

			StringBuffer sb = new StringBuffer();

			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			data = sb.toString();

			br.close();

		} catch (Exception e) {
			//Log.d("Exception while downloading url", e.toString());
		} finally {
			iStream.close();
			urlConnection.disconnect();
		}
		return data;
	}

	// Fetches data from url passed
	private class DownloadTask extends AsyncTask<String, Void, String> {

		// Downloading data in non-ui thread
		@Override
		protected String doInBackground(String... url) {

			// For storing data from web service
			String data = "";

			try {
				// Fetching the data from web service
				data = downloadUrl(url[0]);
			} catch (Exception e) {
				//Log.d("Background Task", e.toString());
			}
			return data;
		}

		// Executes in UI thread, after the execution of
		// doInBackground()
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			ParserTask parserTask = new ParserTask();

			// Invokes the thread for parsing the JSON data
			parserTask.execute(result);

		}
	}

	/** A class to parse the Google Places in JSON format */
	private class ParserTask extends
			AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

		// Parsing the data in non-ui thread
		@Override
		protected List<List<HashMap<String, String>>> doInBackground(
				String... jsonData) {

			JSONObject jObject;
			List<List<HashMap<String, String>>> routes = null;

			try {
				jObject = new JSONObject(jsonData[0]);
				DirectionsJSONParser parser = new DirectionsJSONParser();

				// Starts parsing data
				routes = parser.parse(jObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return routes;
		}

		// Executes in UI thread, after the parsing process
		@Override
		protected void onPostExecute(List<List<HashMap<String, String>>> result) {
			ArrayList<LatLng> points = null;
			PolylineOptions lineOptions = null;
			MarkerOptions markerOptions = new MarkerOptions();
			String distance = "";
			String duration = "";

			if (result.size() < 1) {
				Toast.makeText(getBaseContext(), "No Points",
						Toast.LENGTH_SHORT).show();
				return;
			}

			// Traversing through all the routes
			for (int i = 0; i < result.size(); i++) {
				points = new ArrayList<LatLng>();
				lineOptions = new PolylineOptions();

				// Fetching i-th route
				List<HashMap<String, String>> path = result.get(i);

				// Fetching all the points in i-th route
				for (int j = 0; j < path.size(); j++) {
					HashMap<String, String> point = path.get(j);

					if (j == 0) { // Get distance from the list
						distance = (String) point.get("distance");

						continue;
					} else if (j == 1) { // Get duration from the list
						duration = (String) point.get("duration");
						continue;
					}

					double lat = Double.parseDouble(point.get("lat"));
					double lng = Double.parseDouble(point.get("lng"));
					LatLng position = new LatLng(lat, lng);

					points.add(position);
				}

				// Adding all the points in the route to LineOptions
				lineOptions.addAll(points);
				lineOptions.width(8);
				lineOptions.color(Color.parseColor(("#2D5954")));

			}
			String[] split = distance.split("\\s+");

			dist = Math.round(Float.parseFloat(split[0]));
			if (split[1].equalsIgnoreCase("m")) {
				dist = 0;
			}
			PathGoogleMapActivity.this.distance.setText(distance);
			new GetContacts().execute();

			// Drawing polyline in the Google Map for the i-th route
			googleMap.addPolyline(lineOptions);
		}
	}

	private class GetContacts extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog

		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// Creating service handler class instance
			ServiceHandler sh = new ServiceHandler();
			String url = "http://107.170.68.128:8080/lekaraao/costcalc.jsp?distance="+dist;
			//Log.d("URL", url);
			
			// Making a request to url and getting response
			String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);
			if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    dist = jsonObj.getInt("cod");
                   
               
                }catch (JSONException e) {
                    e.printStackTrace();
                }
			}
			// Toast.makeText(getApplicationContext(), "dsa" +jsonStr,
			// Toast.LENGTH_SHORT).show();
			
			//Log.d("Response: ", "> " + jsonStr);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			deliveryCost = dist * 5 + 20;
			cost.setText("\u20B9" + dist);
			SimpleDateFormat df = new SimpleDateFormat("h:mm a");
			Calendar c = Calendar.getInstance();
			c.add(Calendar.HOUR, 1);
			Date d = c.getTime();
			String date = df.format(d);
			time.setText(date);
		}

	}

	private String getMapsApiDirectionsUrl() {
		String waypoints = "waypoints=optimize:true|" + toPosition.latitude
				+ "," + toPosition.longitude + "|" + "|"
				+ fromPosition.latitude + "," + fromPosition.longitude;

		String sensor = "sensor=false";
		String params = waypoints + "&" + sensor;
		String output = "json";
		String url = "https://maps.googleapis.com/maps/api/directions/"
				+ output + "?" + params;
		return url;
	}

	private void addMarkers() {
		if (googleMap != null) {
			googleMap.addMarker(new MarkerOptions().position(fromPosition)
					.title("From"));
			googleMap
					.addMarker(
							new MarkerOptions().position(toPosition)
									.title("To"))
					.setIcon(
							BitmapDescriptorFactory
									.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
			// googleMap.addMarker(new MarkerOptions().position(WALL_STREET)
			// .title("Third Point"));
		}
	}

	private class ReadTask extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... url) {
			String data = "";
			try {
				HttpConnection http = new HttpConnection();
				data = http.readUrl(url[0]);
			} catch (Exception e) {
				////Log.d("Background Task", e.toString());
			}
			return data;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			new ParserTask().execute(result);
		}
	}

	// private class ParserTask extends
	// AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
	//
	// @Override
	// protected List<List<HashMap<String, String>>> doInBackground(
	// String... jsonData) {
	//
	// JSONObject jObject;
	// List<List<HashMap<String, String>>> routes = null;
	//
	// try {
	// jObject = new JSONObject(jsonData[0]);
	// PathJSONParser parser = new PathJSONParser();
	// routes = parser.parse(jObject);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// return routes;
	// }
	//
	// @Override
	// protected void onPostExecute(List<List<HashMap<String, String>>> routes)
	// {
	// ArrayList<LatLng> points = null;
	// PolylineOptions polyLineOptions = null;
	//
	// // traversing through routes
	// for (int i = 0; i < routes.size(); i++) {
	// points = new ArrayList<LatLng>();
	// polyLineOptions = new PolylineOptions();
	// List<HashMap<String, String>> path = routes.get(i);
	//
	// for (int j = 0; j < path.size(); j++) {
	// HashMap<String, String> point = path.get(j);
	//
	// double lat = Double.parseDouble(point.get("lat"));
	// double lng = Double.parseDouble(point.get("lng"));
	// LatLng position = new LatLng(lat, lng);
	//
	// points.add(position);
	// }
	//
	// polyLineOptions.addAll(points);
	// polyLineOptions.width(2);
	// polyLineOptions.color(Color.BLUE);
	// }
	//
	// googleMap.addPolyline(polyLineOptions);
	// }
	// }
}
